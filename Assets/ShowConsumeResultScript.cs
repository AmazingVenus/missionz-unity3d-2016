﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShowConsumeResultScript : MonoBehaviour {
    public int beforeValue;
    public int afterValue;
    public int iconIndex;
    public string title;

    public Slider slider;
    public Image icon;
    public Text titleObject;
    public Text valueObject;
    // Use this for initialization
    public bool isShow;
    public Vector2 currentProgress;
    public Color sliderColor;
    public Image fillArea;

    public Sprite[] iconPool;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        fillArea.color = sliderColor;
        icon.sprite = iconPool[iconIndex];
        titleObject.text = title;
        gameObject.SetActive(isShow);
        if (isShow) {
            currentProgress = Vector2.Lerp(currentProgress, new Vector2(afterValue, 0), 4 * Time.deltaTime);
            slider.value = currentProgress.x;
            StartCoroutine(delay());
        }
	}

    public IEnumerator delay() {
        yield return new WaitForSeconds(2);
        isShow = false;
        
    }
}
