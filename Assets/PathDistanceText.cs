﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PathDistanceText : MonoBehaviour {
	public Node node;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<TextMesh>().text = node.nextDistance.ToString()+" m.";
	}
}
