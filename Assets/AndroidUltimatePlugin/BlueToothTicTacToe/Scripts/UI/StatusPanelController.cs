﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class StatusPanelController : MonoBehaviour {

	public Text blueToothStatusText;
	public Text deviceNameText;	

	private const string TAG="[StatusPanelController] ";
	
	private TicTacToeEventManager ticTacToeEventManager;
	private TicTacToeDataManager ticTacToeDataManager;
	
	private void Awake(){
		ticTacToeDataManager = TicTacToeDataManager.GetInstance();		
		ticTacToeEventManager = TicTacToeEventManager.GetInstance();
	}
	
	// Use this for initialization
	void Start (){
		AddEventListener();
	}
	
	private void OnDestroy(){
		RemoveEventListener();
	}
	
	private void AddEventListener(){
		if(ticTacToeEventManager!=null){
			ticTacToeEventManager.OnSelectDevice+=OnSelectDevice;	
			ticTacToeEventManager.OnConnecting+=OnConnecting;
			ticTacToeEventManager.OnConnected+=OnConnected;
			ticTacToeEventManager.OnDisConnected+=OnDisConnected;
			ticTacToeEventManager.OnConnectToDevice+=OnConnectToDevice;
		}
	}
	
	private void RemoveEventListener(){
		if(ticTacToeEventManager!=null){
			ticTacToeEventManager.OnSelectDevice-=OnSelectDevice;
			ticTacToeEventManager.OnConnecting-=OnConnecting;
			ticTacToeEventManager.OnConnected-=OnConnected;
			ticTacToeEventManager.OnDisConnected-=OnDisConnected;
			ticTacToeEventManager.OnConnectToDevice-=OnConnectToDevice;
		}
	}
	
	private void UpdateDevice(DeviceData device){
		deviceNameText.text = String.Format("{0} : {1}", device.name,device.macAddress);
	}

	private void UpdateBlueToothTextStatus(string status){
		if(blueToothStatusText!=null){
			if(ticTacToeDataManager.connectionState == ConnectionState.CONNECTED){
				if(ticTacToeDataManager.isServer){
					blueToothStatusText.text = string.Format("Status: {0} Role: {1}",status,"Server");
				}else{
					blueToothStatusText.text = string.Format("Status: {0} Role: {1}",status,"Client");
				}
			}else{
				blueToothStatusText.text = string.Format("Status: {0} to",status);
			}
		}
	}


	
	private void OnSelectDevice(DeviceData device){
		if(ticTacToeDataManager.connectionState == ConnectionState.NOT_CONNECTED){
			//ticTacToeDataManager.currentDevice = device;
			ticTacToeDataManager.currentSelected = device;
			UpdateDevice(device);
			Debug.Log(TAG + "OnSelectDevice device: " + device.name);
		}
	}

	private void OnConnecting(String data){
		UpdateBlueToothTextStatus(data);
	}
	
	private void OnConnected(string data){
		UpdateBlueToothTextStatus(data);
	}
	
	private void OnDisConnected(string data){
		UpdateBlueToothTextStatus(data);
	}

	private void OnConnectToDevice(string data){
		UpdateBlueToothTextStatus(data);
		UpdateDevice(ticTacToeDataManager.currentDevice);
	}
}
