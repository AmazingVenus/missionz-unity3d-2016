﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DeviceButtonController : MonoBehaviour {

	public DeviceData deviceData;
	public Text deviceNameText;
	public Text deviceMacAddressText;

	private TicTacToeEventManager ticTacToeEventManager;

	private void Start(){
		ticTacToeEventManager = TicTacToeEventManager.GetInstance();
	}

	public void OnSelectDevice(){
		ticTacToeEventManager.ClickDeviceOnList(deviceData);
		Debug.Log(" OnSelectMacAddress DeviceName: " + deviceData.name + " deviceMacAddress " + deviceData.macAddress);
		//Debug.Log(" OnSelectMacAddress DeviceName: ");
	}

	public void UpdateButton(){
		deviceNameText.text = deviceData.name;
		deviceMacAddressText.text = deviceData.macAddress;
	}
}
