﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class DeviceListController : MonoBehaviour {

	public GameObject deviceButton;
	public Transform contentPanel;

	private BlueToothPlugin blueToothPlugin;
	private const string TAG="[DeviceListController] ";

	private List<GameObject> deviceButtonCollection = new List<GameObject>();
	private TicTacToeEventManager ticTacToeEventManager;

	private void Awake(){

	}

	// Use this for initialization
	void Start (){
		blueToothPlugin = BlueToothPlugin.GetInstance();
		ticTacToeEventManager = TicTacToeEventManager.GetInstance();
		AddEventListener();
		EnableButton(true);
	}

	private void OnDestroy(){
		RemoveEventListener();
	}
	
	private void AddEventListener(){
		if(ticTacToeEventManager!=null){
			ticTacToeEventManager.OnConnecting+=OnConnecting;
			ticTacToeEventManager.OnConnected+=OnConnected;
			ticTacToeEventManager.OnDisConnected+=OnDisConnected;
			ticTacToeEventManager.OnConnectToDevice+=OnConnectToDevice;
		}
	}
	
	private void RemoveEventListener(){
		if(ticTacToeEventManager!=null){
			ticTacToeEventManager.OnConnecting-=OnConnecting;
			ticTacToeEventManager.OnConnected-=OnConnected;
			ticTacToeEventManager.OnDisConnected-=OnDisConnected;
			ticTacToeEventManager.OnConnectToDevice-=OnConnectToDevice;
		}
	}

	/*public void Populate(){			
		for(int index=0; index<10; index++ ){
			GameObject button = Instantiate(deviceButton) as GameObject;
			DeviceButtonController deviceButtonController = button.GetComponent<DeviceButtonController>();					
			button.transform.SetParent (contentPanel,false);
			//deviceButtonController.UpdateButton();
		}
	}*/
	
	public void Populate(){
		string pairedDevices = blueToothPlugin.GetPairedDevices();
		
		if(!pairedDevices.Equals("",StringComparison.Ordinal)){
			string[] pairedDeviceSet = pairedDevices.Split(',');
			int len = pairedDeviceSet.Length;
			Debug.Log("got mac address!!!!!!!!!!!");

			if(len > 0){
				for(int index=0; index<len; index++ ){
					string device =  pairedDeviceSet.GetValue(index).ToString();
					string[] deviceInfo = device.Split('_');
					string deviceName = deviceInfo.GetValue(0).ToString();
					string deviceMacAddress = deviceInfo.GetValue(1).ToString();

					//check if exist if not create new entry else do nothing
					if(!SearchByMacAddress(deviceMacAddress)){
						GameObject button = Instantiate(deviceButton) as GameObject;
						DeviceButtonController deviceButtonController = button.GetComponent<DeviceButtonController>();
						deviceButtonController.deviceData.name = deviceName;
						deviceButtonController.deviceData.macAddress = deviceMacAddress;					
						button.transform.SetParent (contentPanel,false);
						deviceButtonController.UpdateButton();
						deviceButtonCollection.Add(button);
					}
				}
			}
		}
	}

	private bool SearchByMacAddress(string macAddress){
		bool found = false;
		int len = deviceButtonCollection.Count;
		for(int index=0; index<len;index++){
			DeviceButtonController deviceButtonController = deviceButtonCollection[index].GetComponent<DeviceButtonController>();
			if(deviceButtonController.deviceData.macAddress.Equals(macAddress,StringComparison.Ordinal)){
				found =  true;
				break;
			}
		}

		return found;
	}

	private void EnableButton(bool val){
		int len = deviceButtonCollection.Count;
		for(int index=0; index<len;index++){
			Button button = deviceButtonCollection[index].GetComponent<Button>();
			button.interactable = val;
		}		

	}

	private void OnConnecting(String data){
		EnableButton(false);
	}
	
	private void OnConnected(string data){
		EnableButton(false);
	}
	
	private void OnDisConnected(string data){
		EnableButton(true);
	}
	
	private void OnConnectToDevice(string data){
		EnableButton(false);
	}
}
