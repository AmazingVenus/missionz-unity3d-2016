﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BoardButtonController : MonoBehaviour {

	public BoardButton buttonType;
	public string buttonIndex;
	public string buttonMark;
	public int row;
	public int col;
	private TicTacToeEventManager ticTacToeEventManager;
	private Text buttonText;
	public bool isLock = false;


	private TicTacToeDataManager ticTacToeDataManager;
	// Use this for initialization
	void Start () {
		ticTacToeDataManager = TicTacToeDataManager.GetInstance();
		ticTacToeEventManager = TicTacToeEventManager.GetInstance();
		buttonText = this.gameObject.GetComponentInChildren<Text>();
	}	

	public void OnClickBoardButton(){
		if(!isLock){
			isLock = true;

			if(ticTacToeDataManager.isServer){
				buttonMark = BoardButtonMark.X_MARK;
			}else{
				buttonMark = BoardButtonMark.O_MARK;
			}
			
			buttonText.text = buttonMark;
			
			ticTacToeEventManager.DispatchClickBoardButton( this);
			Debug.Log("[BoardButtonController] OnClickBoardButton buttonIndex: " + buttonIndex);
			Debug.Log("[BoardButtonController] OnClickBoardButton buttonType: " + buttonType);
		}
	}
}
