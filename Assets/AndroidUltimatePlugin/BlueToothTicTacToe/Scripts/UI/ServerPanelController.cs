﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class ServerPanelController : MonoBehaviour {

	public Text connectButtonText;
	public DeviceListController deviceListController;
	
	private BlueToothPlugin blueToothPlugin;
	private const string TAG="[ServerPanelController] ";

	private GameManager gameManager;
	private TicTacToeEventManager ticTacToeEventManager;
	private TicTacToeDataManager ticTacToeDataManager;
	
	private void Awake(){
		ticTacToeDataManager = TicTacToeDataManager.GetInstance();		
		ticTacToeEventManager = TicTacToeEventManager.GetInstance();

		AddEventListener();
		
		blueToothPlugin = BlueToothPlugin.GetInstance();
		blueToothPlugin.SetDebug(1);
		blueToothPlugin.Init();

		ticTacToeDataManager.myDevice.name = blueToothPlugin.GetDeviceName();
		ticTacToeDataManager.myDevice.macAddress = blueToothPlugin.GetDeviceAddress();

		Debug.Log(" my device name " + ticTacToeDataManager.myDevice.name);
		Debug.Log(" my device address " + ticTacToeDataManager.myDevice.macAddress);

		blueToothPlugin.SetConnectionCallbackListener(
			OnBTConnected
			,OnConnecting
			,OnNotConnected
			,OnConnectionFailed
			,OnConnectionLost
			,OnConnectToDevice
		);

		blueToothPlugin.SetDataCallbackListener(OnRecievedData,OnSentData,OnLogMessage);


		//checks if blue tooth is available
		bool isEnable = blueToothPlugin.CheckBlueTooth();
		Debug.Log("[ServerPanelController] isBlueToothEnable: " + isEnable);

		if(!isEnable){
			blueToothPlugin.NotifyUserToEnableBlueTooth();
			//blueToothPlugin.EnsureDiscoverable();
		}else{
			blueToothPlugin.InitServer();
		}
	}
	
	// Use this for initialization
	void Start (){
		gameManager = GameManager.GetInstance();
		gameManager.Init();
	}
	
	private void OnDestroy(){
		RemoveEventListener();
	}
	
	private void AddEventListener(){
		if(ticTacToeEventManager!=null){
			ticTacToeEventManager.OnConnected+=OnConnected;
			ticTacToeEventManager.OnDisConnected+=OnDisConnected;
		}
	}
	
	private void RemoveEventListener(){
		if(ticTacToeEventManager!=null){
			ticTacToeEventManager.OnConnected-=OnConnected;
			ticTacToeEventManager.OnDisConnected-=OnDisConnected;
		}
	}
	
	public void Connect(){
		if(ticTacToeDataManager.currentSelected!=null){
			ticTacToeDataManager.isServer = false;
			
			blueToothPlugin.Connect(ticTacToeDataManager.currentSelected.macAddress,false);
			Debug.Log(TAG + "Connect to " + ticTacToeDataManager.currentSelected.name + " mac address " + ticTacToeDataManager.currentSelected.macAddress);
		}else{
			Debug.Log(TAG + "can't Connect no device selected!");
		}
	}

	private void OnBTConnected(string data){
		ticTacToeDataManager.connectionState = ConnectionState.CONNECTED;
		ticTacToeEventManager.DispatchConnected(data.ToString());

		Debug.Log("[ServerPanelController] OnConnected============> " + data);
	}

	private void OnConnecting(string data){
		ticTacToeDataManager.connectionState = ConnectionState.CONNECTING;
		ticTacToeEventManager.DispatchConnecting(data.ToString());

		Debug.Log("[ServerPanelController] OnConnecting============> " + data);
	}

	private void OnNotConnected(string data){
		ticTacToeDataManager.connectionState = ConnectionState.NOT_CONNECTED;
		ticTacToeEventManager.DispatchDisConnected(data.ToString());

		Debug.Log("[ServerPanelController] OnNotConnected============> " + data);
	}

	private void OnConnectionFailed(string data){
		ticTacToeDataManager.connectionState = ConnectionState.NOT_CONNECTED;
		ticTacToeEventManager.DispatchDisConnected(data.ToString());

		Debug.Log("[ServerPanelController] OnConnectionFailed============> " + data);
	}

	private void OnConnectionLost(string data){
		ticTacToeDataManager.connectionState = ConnectionState.NOT_CONNECTED;
		ticTacToeEventManager.DispatchDisConnected(data.ToString());

		Debug.Log("[ServerPanelController] OnConnectionLost============> " + data);
	}
	private void OnConnectToDevice(string data){
		string deviceConnectedTo = data.ToString();
		string[] deviceInfo = deviceConnectedTo.Split('_');
		string deviceName = deviceInfo.GetValue(0).ToString();
		string macAddress = deviceInfo.GetValue(1).ToString();
		
		DeviceData device = new DeviceData();
		device.name = deviceName;
		device.macAddress = macAddress;
		
		ticTacToeDataManager.currentDevice = device;
		
		ticTacToeEventManager.DispatchConnectedToDevice("OnConnectToDevice");
		Debug.Log("[BlueToothTicTacToeListener] OnConnectToDevice " + data);

		Debug.Log("[ServerPanelController] OnConnectToDevice============> deviceName " + deviceName + " macAddress " + macAddress );
	}


	public void UpdatePairedDevices(){
		deviceListController.Populate();
	}
	
	private void OnConnected(string data){
		connectButtonText.text = "Disconnect";
	}
	
	private void OnDisConnected(String data){
		ticTacToeDataManager.isServer = true;
		connectButtonText.text = "Connect";
	}

	private void OnRecievedData(string data){
		Debug.Log("[BlueToothTicTacToeListener] OnRecievedMessage " + data);
		
		string recievedData =  data.ToString();
		string[] dataInfo = recievedData.Split('_');
		
		switch(dataInfo.GetValue(0).ToString()){
		case MessageCode.BEGIN_TURN:
			ticTacToeEventManager.DispatchBeginTurn(dataInfo.GetValue(1).ToString(),dataInfo.GetValue(2).ToString());
			break;
			
		case MessageCode.END_TURN:
			ticTacToeEventManager.DispatchEndTurn(dataInfo.GetValue(1).ToString(),dataInfo.GetValue(2).ToString());
			break;
			
		case MessageCode.MOVE_TURN:
			ticTacToeEventManager.DispatchMoveTurn(dataInfo.GetValue(1).ToString(),dataInfo.GetValue(2).ToString(),dataInfo.GetValue(3).ToString());
			break;
			
		case MessageCode.CHAT:
			ticTacToeEventManager.DispatchRecievedChat(dataInfo.GetValue(1).ToString());
			break;
		}

		Debug.Log("[ServerPanelController] OnRecievedData============> " + data);
	}

	private void OnSentData(string data){

		ticTacToeEventManager.DispatchSentData(data.ToString());
		Debug.Log("[BlueToothEventListener] OnSendMessage " + data);
		
		string recievedData =  data.ToString();
		string[] dataInfo = recievedData.Split('_');
		
		switch(dataInfo.GetValue(0).ToString()){
		case MessageCode.BEGIN_TURN:
			break;
			
		case MessageCode.END_TURN:
			break;
			
		case MessageCode.MOVE_TURN:
			ticTacToeEventManager.DispatchSentMove(dataInfo.GetValue(1).ToString());
			break;
			
		case MessageCode.CHAT:
			ticTacToeEventManager.DispatchSentChat(dataInfo.GetValue(1).ToString());
			break;
		}

		Debug.Log("[ServerPanelController] OnSentData============> " + data);
	}

	private void OnLogMessage(string data){
		Debug.Log("[ServerPanelController] OnLogMessage============> " + data);
	}
}
