﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class TurnStatusController : MonoBehaviour {

	public Text turnStatusText;
	private TicTacToeDataManager ticTacToeDataManager;
	private TicTacToeEventManager ticTacToeEventManager;

	//private BlueToothPlugin blueToothPlugin;

	// Use this for initialization
	void Start () {
		ticTacToeDataManager = TicTacToeDataManager.GetInstance();
		ticTacToeEventManager = TicTacToeEventManager.GetInstance();
		//blueToothPlugin = BlueToothPlugin.GetInstance();
		AddEventListener();
	}

	private void OnDestroy(){
		RemoveEventListener();
	}

	private void AddEventListener(){
		if(ticTacToeEventManager!=null){
			ticTacToeEventManager.OnBeginTurn+=OnBeginTurn;
			ticTacToeEventManager.OnEndTurn+=OnEndTurn;
			ticTacToeEventManager.OnClickBoardButton+=OnClickBoardButton;
		}
	}
	
	private void RemoveEventListener(){
		if(ticTacToeEventManager!=null){
			ticTacToeEventManager.OnBeginTurn-=OnBeginTurn;
			ticTacToeEventManager.OnEndTurn-=OnEndTurn;
			ticTacToeEventManager.OnClickBoardButton-=OnClickBoardButton;
		}
	}
	
	// Update is called once per frame
	private void UpdateBlueToothTextStatusTurn(string val,string turnCount){
		if(turnStatusText!=null){
			turnStatusText.text = string.Format("{0} turncount: {1}",val,turnCount);
		}
	}

	private void OnBeginTurn(string macAddress, string turnCount){
		if(ticTacToeDataManager.isServer){
			UpdateBlueToothTextStatusTurn("Your Turn",turnCount);
		}else{
			UpdateBlueToothTextStatusTurn("Enemy Turn",turnCount);
		}
	}

	private void OnEndTurn(string macAddress, string turnCount){
		if(!ticTacToeDataManager.myDevice.macAddress.Equals(macAddress,StringComparison.Ordinal)){
			UpdateBlueToothTextStatusTurn("Your Turn",turnCount);
		}
	}

	private void OnClickBoardButton(BoardButtonController button){
		ticTacToeDataManager.turnCount++;
		string count = ticTacToeDataManager.turnCount.ToString();
		UpdateBlueToothTextStatusTurn("Enemy Turn",count);
	}
}
