using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIMessagePanelController : MonoBehaviour {

	public InputField inputMessageField;
	private BlueToothPlugin blueToothPlugin;

	public GameObject messagePrefab;
	public Transform contentPanel;

	private TicTacToeDataManager ticTacToeDataManager;
	private TicTacToeEventManager ticTacToeEventManager;

	private void Awake(){
		ticTacToeDataManager = TicTacToeDataManager.GetInstance();
		ticTacToeEventManager = TicTacToeEventManager.GetInstance();
	}

	// Use this for initialization
	void Start (){
		blueToothPlugin = BlueToothPlugin.GetInstance();
		AddEventListener();
	}

	private void OnDestroy(){
		RemoveEventListener();
	}
	
	private void AddEventListener(){
		if(ticTacToeEventManager!=null){
			ticTacToeEventManager.OnSentChat+=OnSentChat;
			ticTacToeEventManager.OnRecievedChat+=OnRecievedChat;
		}
	}
	
	private void RemoveEventListener(){
		if(ticTacToeEventManager!=null){
			ticTacToeEventManager.OnSentChat-=OnSentChat;
			ticTacToeEventManager.OnRecievedChat-=OnRecievedChat;
		}
	}
	
	public void SendMessage(){
		if(inputMessageField!=null){
			string chatToSend = MessageCode.CHAT + "_" + inputMessageField.text;
			blueToothPlugin.SendData(chatToSend);
		}
	}

	private void AddMessage(string message){
		GameObject button = Instantiate(messagePrefab) as GameObject;
		Text text = button.GetComponent<Text>();
		text.text = message;
		button.transform.SetParent (contentPanel,false);
	}

	private void OnSentChat(string data){
		string messageSent = string.Format("Me: {0}", data);
		AddMessage( messageSent);
	}

	private void OnRecievedChat(string data){
		string messageRecieved = string.Format("{0}: {1}",ticTacToeDataManager.currentDevice.name,data);
		AddMessage( messageRecieved);
	}
}
