﻿using UnityEngine;
using System.Collections;

public enum ConnectionState{
	NOT_CONNECTED = 0,
	CONNECTING = 1,
	CONNECTED = 2
}
