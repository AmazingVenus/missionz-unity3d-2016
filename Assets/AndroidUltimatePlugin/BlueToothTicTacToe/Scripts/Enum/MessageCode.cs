﻿using UnityEngine;
using System.Collections;

public class MessageCode{
	public const string CHAT="chat";
	public const string BEGIN_TURN="beginTurn";
	public const string END_TURN="endTurn";
	public const string MOVE_TURN="moveTurn";
}
