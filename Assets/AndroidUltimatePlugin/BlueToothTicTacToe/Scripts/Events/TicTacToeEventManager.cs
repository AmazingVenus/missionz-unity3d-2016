﻿using UnityEngine;
using System.Collections;
using System;

public class TicTacToeEventManager : MonoBehaviour {

	private static TicTacToeEventManager instance;
	private static GameObject container;

	private Action<DeviceData> SelectDevice;
	public event Action<DeviceData> OnSelectDevice{
		add{SelectDevice+=value;}
		remove{SelectDevice-=value;}
	}

	private Action <String>Connecting;
	public event Action <String>OnConnecting{
		add{Connecting+=value;}
		remove{Connecting-=value;}
	}


	private Action <String>Connected;
	public event Action <String>OnConnected{
		add{Connected+=value;}
		remove{Connected-=value;}
	}

	private Action <String>DisConnected;
	public event Action <String>OnDisConnected{
		add{DisConnected+=value;}
		remove{DisConnected-=value;}
	}

	private Action <String>ConnectToDevice;
	public event Action <String>OnConnectToDevice{
		add{ConnectToDevice+=value;}
		remove{ConnectToDevice-=value;}
	}

	private Action <String>SentData;
	public event Action <String>OnSentData{
		add{SentData+=value;}
		remove{SentData-=value;}
	}

	private Action <String>RecievedData;
	public event Action <String>OnRecievedData{
		add{RecievedData+=value;}
		remove{RecievedData-=value;}
	}

	private Action <String>SentChat;
	public event Action <String>OnSentChat{
		add{SentChat+=value;}
		remove{SentChat-=value;}
	}

	private Action <String>RecievedChat;
	public event Action <String>OnRecievedChat{
		add{RecievedChat+=value;}
		remove{RecievedChat-=value;}
	}

	private Action <String,String>BeginTurn;
	public event Action <String,String>OnBeginTurn{
		add{BeginTurn+=value;}
		remove{BeginTurn-=value;}
	}

	private Action <String,String>EndTurn;
	public event Action <String,String>OnEndTurn{
		add{EndTurn+=value;}
		remove{EndTurn-=value;}
	}

	private Action <String,String,String>MoveTurn;
	public event Action <String,String,String>OnMoveTurn{
		add{MoveTurn+=value;}
		remove{MoveTurn-=value;}
	}

	private Action <String>SentMove;
	public event Action <String>OnSentMove{
		add{SentMove+=value;}
		remove{SentMove-=value;}
	}

	private Action <BoardButtonController>ClickBoardButton;
	public event Action <BoardButtonController>OnClickBoardButton{
		add{ClickBoardButton+=value;}
		remove{ClickBoardButton-=value;}
	}



	public static TicTacToeEventManager GetInstance(){
		if(instance == null){
			container = new GameObject();
			container.name = "TicTacToeEventManager";
			instance = container.AddComponent(typeof(TicTacToeEventManager)) as TicTacToeEventManager;
			DontDestroyOnLoad(instance.gameObject);
		}

		return instance;
	}

	public void ClickDeviceOnList(DeviceData device ){
		Debug.Log("[TicTacToeEventManager] ClickDeviceOnList ");
		if(null!=SelectDevice){
			SelectDevice(device);
		}
	}

	public void DispatchConnected(string data){
		if(null!=Connected){
			Connected(data);
		}
	}

	public void DispatchDisConnected(string data){
		if(null!=DisConnected){
			DisConnected(data);
		}
	}

	public void DispatchConnecting(string data){
		if(null!=Connecting){
			Connecting(data);
		}
	}

	public void DispatchConnectedToDevice(string data){
		if(null!=ConnectToDevice){
			ConnectToDevice(data);
		}
	}

	public void DispatchRecievedData(string data){
		if(null!=RecievedData){
			RecievedData(data);
		}
	}

	public void DispatchSentData(string data){
		if(null!=SentData){
			SentData(data);
		}
	}

	public void DispatchBeginTurn(string macAddress, string turnCount){
		if(null!=BeginTurn){
			BeginTurn(macAddress,turnCount);
		}
	}

	public void DispatchEndTurn(string macAddress, string turnCount){
		if(null!=EndTurn){
			EndTurn(macAddress,turnCount);
		}
	}

	public void DispatchMoveTurn(string col, string row,string mark ){
		if(null!=MoveTurn){
			MoveTurn(col,row,mark);
		}
	}

	public void DispatchClickBoardButton(BoardButtonController data){
		if(null!=ClickBoardButton){
			ClickBoardButton(data);
		}
	}

	public void DispatchRecievedChat(string data){
		if(null!=RecievedChat){
			RecievedChat(data);
		}
	}

	public void DispatchSentChat(string data){
		if(null!=SentChat){
			SentChat(data);
		}
	}

	public void DispatchSentMove(string data){
		if(null!=SentMove){
			SentMove(data);
		}
	}


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
