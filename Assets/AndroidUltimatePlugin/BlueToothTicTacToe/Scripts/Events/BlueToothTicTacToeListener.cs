﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BlueToothTicTacToeListener : MonoBehaviour{

	private TicTacToeDataManager ticTacToeDataManager;
	private TicTacToeEventManager ticTacToeEventManager;

	// Use this for initialization
	void Start () {
		ticTacToeDataManager = TicTacToeDataManager.GetInstance();
		ticTacToeEventManager = TicTacToeEventManager.GetInstance();
	}
	
	public void OnConnected(string data){
		ticTacToeDataManager.connectionState = ConnectionState.CONNECTED;
		ticTacToeEventManager.DispatchConnected(data.ToString());
	}
	
	public void OnConnecting(string data){
		ticTacToeDataManager.connectionState = ConnectionState.CONNECTING;
		ticTacToeEventManager.DispatchConnecting(data.ToString());
	}
	
	public void OnNotConnected(string data){
		ticTacToeDataManager.connectionState = ConnectionState.NOT_CONNECTED;
		ticTacToeEventManager.DispatchDisConnected(data.ToString());
	}
	
	public void OnConnectionFailed(string data){
		ticTacToeDataManager.connectionState = ConnectionState.NOT_CONNECTED;
		ticTacToeEventManager.DispatchDisConnected(data.ToString());
	}
	
	public void OnConnectionLost(string data){
		ticTacToeDataManager.connectionState = ConnectionState.NOT_CONNECTED;
		ticTacToeEventManager.DispatchDisConnected(data.ToString());
	}
	
	public void OnSendMessage(string data){
		ticTacToeEventManager.DispatchSentData(data.ToString());
		Debug.Log("[BlueToothEventListener] OnSendMessage " + data);

		string recievedData =  data.ToString();
		string[] dataInfo = recievedData.Split('_');
		
		switch(dataInfo.GetValue(0).ToString()){
		case MessageCode.BEGIN_TURN:
			break;
			
		case MessageCode.END_TURN:
			break;
			
		case MessageCode.MOVE_TURN:
			ticTacToeEventManager.DispatchSentMove(dataInfo.GetValue(1).ToString());
			break;
			
		case MessageCode.CHAT:
			ticTacToeEventManager.DispatchSentChat(dataInfo.GetValue(1).ToString());
			break;
		}
	}
	
	public void OnRecievedMessage(string data){
		Debug.Log("[BlueToothTicTacToeListener] OnRecievedMessage " + data);

		string recievedData =  data.ToString();
		string[] dataInfo = recievedData.Split('_');

		switch(dataInfo.GetValue(0).ToString()){
			case MessageCode.BEGIN_TURN:
				ticTacToeEventManager.DispatchBeginTurn(dataInfo.GetValue(1).ToString(),dataInfo.GetValue(2).ToString());
			break;

			case MessageCode.END_TURN:
				ticTacToeEventManager.DispatchEndTurn(dataInfo.GetValue(1).ToString(),dataInfo.GetValue(2).ToString());
			break;

			case MessageCode.MOVE_TURN:
			ticTacToeEventManager.DispatchMoveTurn(dataInfo.GetValue(1).ToString(),dataInfo.GetValue(2).ToString(),dataInfo.GetValue(3).ToString());
			break;

			case MessageCode.CHAT:
				ticTacToeEventManager.DispatchRecievedChat(dataInfo.GetValue(1).ToString());
			break;
		}
	}
	
	public void OnMessage(string data){
		Debug.Log("[BlueToothTicTacToeListener] OnMessage " + data);
	}
	
	public void OnConnectToDevice(string data){
		string deviceConnectedTo = data.ToString();
		string[] deviceInfo = deviceConnectedTo.Split('_');
		string deviceName = deviceInfo.GetValue(0).ToString();
		string macAddress = deviceInfo.GetValue(1).ToString();
		
		DeviceData device = new DeviceData();
		device.name = deviceName;
		device.macAddress = macAddress;
		
		ticTacToeDataManager.currentDevice = device;

		ticTacToeEventManager.DispatchConnectedToDevice("OnConnectToDevice");
		Debug.Log("[BlueToothTicTacToeListener] OnConnectToDevice " + data);
	}	
}
