﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class GameBoardController : MonoBehaviour {

	public Button[] buttonCollection;
	private TicTacToeEventManager ticTacToeEventManager;
	private TicTacToeDataManager ticTacToeDataManager;
	//private BlueToothPlugin blueToothPlugin;

	private List<List<GameObject>> boardButtonCollection = new List<List<GameObject>>();
	public GameObject boardButton;
	public Transform boardButtonHolder;
	private GridLayoutGroup gridLayoutGroup;

	public int gridCount;

	// Use this for initialization
	void Start () {
		//gameManager = GameManager.GetInstance();
		ticTacToeDataManager = TicTacToeDataManager.GetInstance();
		ticTacToeEventManager = TicTacToeEventManager.GetInstance();
		//blueToothPlugin = BlueToothPlugin.GetInstance();

		gridLayoutGroup = boardButtonHolder.GetComponent<GridLayoutGroup>();

		AddEventListener();

		CreateBoard();
	}

	private void OnDestroy(){
		RemoveEventListener();
	}

	private void AddEventListener(){
		ticTacToeEventManager.OnBeginTurn+=OnBeginTurn;
		ticTacToeEventManager.OnMoveTurn+=OnMoveTurn;
		ticTacToeEventManager.OnEndTurn+=OnEndTurn;
		ticTacToeEventManager.OnClickBoardButton+=OnClickBoardButton;
		ticTacToeEventManager.OnDisConnected+=OnDisConnected;
		ticTacToeEventManager.OnConnected+=OnConnected;
	}

	private void RemoveEventListener(){
		ticTacToeEventManager.OnMoveTurn-=OnMoveTurn;
		ticTacToeEventManager.OnBeginTurn-=OnBeginTurn;
		ticTacToeEventManager.OnEndTurn-=OnEndTurn;
		ticTacToeEventManager.OnClickBoardButton-=OnClickBoardButton;
		ticTacToeEventManager.OnDisConnected-=OnDisConnected;
		ticTacToeEventManager.OnConnected-=OnConnected;
	}

	private void CreateBoard(){
		if(boardButtonCollection==null){
			boardButtonCollection = new List<List<GameObject>>();
		}else{
			boardButtonCollection.Clear();
		}

		gridLayoutGroup.constraintCount = gridCount;
		int row = gridCount;
		int col = gridCount;

		for(int i = 0; i < row; i++ ){
			List<GameObject> colObject = new List<GameObject>();
			for(int j=0; j < col;j++){
				GameObject newBoardButton = Instantiate(boardButton) as GameObject;
				newBoardButton.transform.SetParent(boardButtonHolder,false);

				BoardButtonController boardButtonController = newBoardButton.GetComponent<BoardButtonController>();
				boardButtonController.row = i;
				boardButtonController.col = j;
				boardButtonController.buttonIndex =   i + "_" + j;

				colObject.Add(newBoardButton);
				Debug.Log( " check tile" + i + "_" + j  );
			}

			boardButtonCollection.Add(colObject);
		}

		BoardButtonController bbc = boardButtonCollection[1][1].GetComponent<BoardButtonController>();
		Debug.Log("check what i caught " + bbc.buttonIndex);
	}

	private void OnMoveTurn(string row, string col, string mark){
		int moveRow = int.Parse(row);
		int moveCol = int.Parse(col);

		GameObject moveButton = (GameObject)boardButtonCollection[moveRow][moveCol];
		Button button = moveButton.GetComponent<Button>();
		button.GetComponentInChildren<Text>().text = mark;


		CheckBoard(moveRow,moveCol,mark);
	}

	private void EnableGameBoardButton(bool val){
		int row = gridCount;
		int col = gridCount;
		
		for(int i = 0; i < row; i++ ){
			for(int j=0; j < col;j++){
				Button button = (Button)boardButtonCollection[i][j].GetComponentInChildren<Button>();
				button.interactable = val;
			}
		}
	}

	private void ResetGameBoardButton(){
		int row = gridCount;
		int col = gridCount;
		
		for(int i = 0; i < row; i++ ){
			for(int j=0; j < col;j++){
				Button button = (Button)boardButtonCollection[i][j].GetComponentInChildren<Button>();
				button.GetComponentInChildren<Text>().text = "";

				BoardButtonController bbc = boardButtonCollection[i][j].GetComponent<BoardButtonController>();
				bbc.isLock = false;
			}
		}
	}

	private void OnBeginTurn(string macAddress, string turnCount){
		ticTacToeDataManager.turnCount = int.Parse(turnCount);
		ticTacToeDataManager.currentPlayerTurn = macAddress;

		if(ticTacToeDataManager.myDevice.macAddress.Equals(ticTacToeDataManager.currentPlayerTurn,StringComparison.Ordinal)){
			EnableGameBoardButton(true);
		}else{
			EnableGameBoardButton(false);
		}
	}

	private void OnClickBoardButton(BoardButtonController button){
		EnableGameBoardButton(false);
		CheckBoard(button.row,button.col,button.buttonMark);
	}

	private void OnEndTurn(string macAddress, string turnCount){
		ticTacToeDataManager.turnCount = int.Parse(turnCount);
		ticTacToeDataManager.previousPlayerTurn = macAddress;

		if(ticTacToeDataManager.myDevice.macAddress.Equals(ticTacToeDataManager.previousPlayerTurn,StringComparison.Ordinal)){
			EnableGameBoardButton(false);
		}else{
			ticTacToeDataManager.currentPlayerTurn = ticTacToeDataManager.myDevice.macAddress;
			EnableGameBoardButton(true);
		}
	}

	private void Reset(){
		ticTacToeDataManager.currentPlayerTurn = "";
		ticTacToeDataManager.previousPlayerTurn = "";
		ticTacToeDataManager.turnCount = 0;
		ResetGameBoardButton();
		EnableGameBoardButton(true);
	}

	//private void CheckBoard(BoardButtonController button){
	private void CheckBoard(int row,int col, string mark){
		Debug.Log("CheckBoard...");

		//string mark = button.buttonMark;
		//int row = int.Parse(moveRow);
		//int col = int.Parse(moveCol);

		int horizontalCounter = 0;
		int verticalCounter = 0;

		BoardButtonController bbc;

		//horizontal check
		if(col  ==  0){
			bbc = boardButtonCollection[row][col+1].GetComponent<BoardButtonController>();
			if(mark.Equals(bbc.buttonMark,StringComparison.Ordinal)){
				horizontalCounter++;
			}

			bbc = boardButtonCollection[row][col+2].GetComponent<BoardButtonController>();
			if(mark.Equals(bbc.buttonMark,StringComparison.Ordinal)){
				horizontalCounter++;
			}
		}


		if(col  >  0 && col == (gridCount - 1)){
			bbc = boardButtonCollection[row][col-1].GetComponent<BoardButtonController>();
			if(mark.Equals(bbc.buttonMark,StringComparison.Ordinal)){
				horizontalCounter++;
			}
			
			bbc = boardButtonCollection[row][col-2].GetComponent<BoardButtonController>();
			if(mark.Equals(bbc.buttonMark,StringComparison.Ordinal)){
				horizontalCounter++;
			}
		}

		if(col  >  0 && (col + 1) <= (gridCount - 1)){
			Debug.Log("checking center!!!!!!!!!");

			bbc = boardButtonCollection[row][col-1].GetComponent<BoardButtonController>();
			if(mark.Equals(bbc.buttonMark,StringComparison.Ordinal)){
				horizontalCounter++;
			}
			
			bbc = boardButtonCollection[row][col+1].GetComponent<BoardButtonController>();
			if(mark.Equals(bbc.buttonMark,StringComparison.Ordinal)){
				horizontalCounter++;
			}
		}
		//horizontal check

		//vertical check
		if(row == 0){
			bbc = boardButtonCollection[row+1][col].GetComponent<BoardButtonController>();
			if(mark.Equals(bbc.buttonMark,StringComparison.Ordinal)){
				verticalCounter++;
			}
			
			bbc = boardButtonCollection[row+2][col].GetComponent<BoardButtonController>();
			if(mark.Equals(bbc.buttonMark,StringComparison.Ordinal)){
				verticalCounter++;
			}
		}

		if(row > 0 && row  == (gridCount - 1) ){
			bbc = boardButtonCollection[row-1][col].GetComponent<BoardButtonController>();
			if(mark.Equals(bbc.buttonMark,StringComparison.Ordinal)){
				verticalCounter++;
			}
			
			bbc = boardButtonCollection[row-2][col].GetComponent<BoardButtonController>();
			if(mark.Equals(bbc.buttonMark,StringComparison.Ordinal)){
				verticalCounter++;
			}
		}

		if(row > 0 && ( row + 1 ) <= (gridCount - 1) ){
			bbc = boardButtonCollection[row-1][col].GetComponent<BoardButtonController>();
			if(mark.Equals(bbc.buttonMark,StringComparison.Ordinal)){
				verticalCounter++;
			}
			
			bbc = boardButtonCollection[row+1][col].GetComponent<BoardButtonController>();
			if(mark.Equals(bbc.buttonMark,StringComparison.Ordinal)){
				verticalCounter++;
			}
		}

		if(horizontalCounter == 2){
			Debug.Log("<color=blue>got horizontal winner!</color>");
		}

		if(verticalCounter == 2){
			Debug.Log("<color=red>got vertical winner!</color>");
		}

		if(horizontalCounter!=2 && verticalCounter != 2 && CheckIfBoardisAllFilled()){
			Debug.Log("<color=green>there's no winner draw!!!</color>");
		}

		Debug.Log("end Check Board...");
	}

	private bool CheckIfBoardisAllFilled(){
		int row = gridCount;
		int col = gridCount;

		int count=0;
		int total=gridCount*gridCount;
		
		for(int i = 0; i < row; i++ ){
			for(int j=0; j < col;j++){
				Button button = (Button)boardButtonCollection[i][j].GetComponentInChildren<Button>();
				Text buttonText = button.GetComponentInChildren<Text>();
				if(!buttonText.text.Equals("",StringComparison.Ordinal)){
					count++;
				}
			}
		}

		if(count==total){
			return true;
		}else{
			return false;
		}
	}

	private void OnDisConnected(string data){
		Reset();
	}

	private void OnConnected(string data){
		Reset();
	}
}
