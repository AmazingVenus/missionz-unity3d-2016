﻿using UnityEngine;
using System.Collections;

public class TicTacToeDataManager : MonoBehaviour {

	private static TicTacToeDataManager instance;
	private static GameObject container;

	public DeviceData currentSelected=null;
	public DeviceData currentDevice=null;

	public ConnectionState connectionState = ConnectionState.NOT_CONNECTED;
	public bool isServer = true;

	public int turnCount = 0;

	//mac address
	public string currentPlayerTurn = "";
	public string previousPlayerTurn = "";

	public DeviceData myDevice;
	
	public static TicTacToeDataManager GetInstance(){
		if(instance == null){
			container = new GameObject();
			container.name = "TicTacToeDataManager";
			instance = container.AddComponent(typeof(TicTacToeDataManager)) as TicTacToeDataManager;
			DontDestroyOnLoad(instance.gameObject);
		}
		
		return instance;
	}

	private void Awake(){
		myDevice = new DeviceData();
	}

	// Use this for initialization
	void Start () {
		
	}
}
