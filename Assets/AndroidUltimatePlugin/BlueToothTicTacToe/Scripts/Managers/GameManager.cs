﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	private static GameManager instance;
	private static GameObject container;

	private TicTacToeEventManager ticTacToeEventManager;
	private TicTacToeDataManager  ticTacToeDataManager;

	private BlueToothPlugin blueToothPlugin;

	public static GameManager GetInstance(){
		if(instance == null){
			container = new GameObject();
			container.name = "GameManager";
			instance = container.AddComponent(typeof(GameManager)) as GameManager;
			DontDestroyOnLoad(instance.gameObject);
		}
		
		return instance;
	}

	// Use this for initialization
	void Start () {

	}

	private void OnDestroy(){
		RemoveEventListener();
	}

	public void Init(){
		ticTacToeDataManager =TicTacToeDataManager.GetInstance();
		blueToothPlugin = BlueToothPlugin.GetInstance();
		
		ticTacToeEventManager = TicTacToeEventManager.GetInstance();
		AddEventListener();
	}


	private void AddEventListener(){
		ticTacToeEventManager.OnConnected+=OnConnected;
		ticTacToeEventManager.OnClickBoardButton+=OnClickBoardButton;
		ticTacToeEventManager.OnSentMove+=OnSentMove;

	}

	private void RemoveEventListener(){
		ticTacToeEventManager.OnConnected-=OnConnected;
		ticTacToeEventManager.OnClickBoardButton-=OnClickBoardButton;
		ticTacToeEventManager.OnSentMove-=OnSentMove;
	}

	private void OnConnected(string data){
		BeginTurn();
	}

	private void OnClickBoardButton(BoardButtonController bbc){
		Debug.Log("[GameManager] OnClickBoardButton button: " );
		MoveTurn(bbc);
	}

	public void BeginTurn(){
		if(ticTacToeDataManager.turnCount==0 && ticTacToeDataManager.isServer){
			blueToothPlugin.SendData(MessageCode.BEGIN_TURN + "_" + ticTacToeDataManager.myDevice.macAddress + "_" + ticTacToeDataManager.turnCount);
		}

		if(ticTacToeDataManager.turnCount > 0){
			blueToothPlugin.SendData(MessageCode.BEGIN_TURN + "_" + ticTacToeDataManager.myDevice.macAddress + "_" + ticTacToeDataManager.turnCount);
		}
	}


	public void EndTurn(){
		blueToothPlugin.SendData(MessageCode.END_TURN + "_" + ticTacToeDataManager.myDevice.macAddress + "_" + ticTacToeDataManager.turnCount);
	}

	public void MoveTurn(BoardButtonController bbc){
		string dataToSend = MessageCode.MOVE_TURN + "_" + bbc.row + "_" + bbc.col +  "_" + bbc.buttonMark;
		Debug.Log("[GameManager] dataToSend: " + dataToSend);
		blueToothPlugin.SendData(dataToSend);
	}

	private void OnSentMove(string data){
		Invoke("EndTurn",0.1f);
	}
}
