﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class DeviceData{
	public string name;
	public string macAddress;
}
