﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class AndroidAdvanceInfoDemo : MonoBehaviour {

	public Text androidIdText;
	public Text telePhoneNumberText;
	public Text telephonyDeviceIdText;
	public Text telephonySimSerialNumberText;
	public Text advertisingIdText;
	public Text uniqueIdText;
	public Text simStatusText;

	private DeviceInfoPlugin deviceInfoPlugin;

	private bool hasSim = false;

	// Use this for initialization
	void Start () {
		deviceInfoPlugin = DeviceInfoPlugin.GetInstance();
		deviceInfoPlugin.SetDebug(0);
		deviceInfoPlugin.Init();
		deviceInfoPlugin.SetDeviceInfoCallbackListener(onGetAdvertisingIdComplete,onGetAdvertisingIdFail);

		hasSim = deviceInfoPlugin.CheckSim();
	}
	
	public void GetAndroidId(){
		string androidId = deviceInfoPlugin.GetAndroidId();
		if(androidIdText!=null){
			androidIdText.text =  string.Format("Android ID: {0}",androidId);
			Debug.Log(" Android ID: " + androidId);
		}
	}

	public void GetTelephoneNumber(){
		string telephoneNumber = deviceInfoPlugin.GetPhoneNumber();
		if(telePhoneNumberText!=null){
			telePhoneNumberText.text =  string.Format("Telephone Number: {0}",telephoneNumber);
			Debug.Log("Telephone Number: " + telephoneNumber);
		}
	}

	public void GetTelephonyDeviceId(){
		if(hasSim){
			string telephonyDeviceId = deviceInfoPlugin.GetTelephonyDeviceId();
			if(telephonyDeviceIdText!=null){
				telephonyDeviceIdText.text =  string.Format("telephony Device ID: {0}",telephonyDeviceId);
				Debug.Log(" telephony Device ID: " + telephonyDeviceId);
			}
		}else{
			telephonyDeviceIdText.text =  string.Format("telephony Device ID: {0}","no sim");
			Debug.Log("no sim");
		}
	}

	public void GetTelephonySimSerialNumber(){
		if(hasSim){
			string telephonySimSerialNumber = deviceInfoPlugin.GetTelephonySimSerialNumber();
			if(telephonySimSerialNumberText!=null){
				telephonySimSerialNumberText.text =  string.Format("Telephony SimSerial Number: {0}",telephonySimSerialNumber);
				Debug.Log("Telephony SimSerial Number: " + telephonySimSerialNumber);
			}
		}else{
			telephonySimSerialNumberText.text =  string.Format("Telephony SimSerial Number: {0}","no sim");
			Debug.Log("no sim");
		}
	}

	public void GetAdvertisingId(){
		deviceInfoPlugin.GetAdvertisingId();
	}

	public void GenerateUniqueId(){
		if(hasSim){
			string uniqueId = deviceInfoPlugin.GenerateUniqueId();
			if(uniqueIdText!=null){
				uniqueIdText.text =  string.Format("Unique ID: {0}",uniqueId);
				Debug.Log("Unique ID: " + uniqueId);
			}
		}else{
			uniqueIdText.text =  string.Format("Unique ID: {0}","no sim");
			Debug.Log("no sim");
		}
	}

	public void CheckSim(){
		bool hasSim = deviceInfoPlugin.CheckSim();
		if(simStatusText!=null){
			simStatusText.text =  string.Format("Has Sim: {0}",hasSim);
			Debug.Log("Has Sim: " + hasSim);
		}
	}

	private void onGetAdvertisingIdComplete(string advertisingId){
		if(advertisingIdText!=null){
			advertisingIdText.text =  string.Format("Advertising ID: {0}",advertisingId);
			Debug.Log("onGetAdvertisingIdComplete Advertising ID: " + advertisingId);
		}
	}

	private void onGetAdvertisingIdFail(string errorMessage){
		Debug.Log("onGetAdvertisingIdFail errorMessage: " + errorMessage);
	}
}