﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InternetInfoDemo : MonoBehaviour {

	private InternetPlugin internetPlugin;

	public Text mobileConnectionText;
	public Text wifiConnectionText;
	public Text wifiSignalStrengthText;
	public Text wifiIPText;
	public Text wifiSSIDText;
	public Text wifiBSSIDText;
	public Text wifiRssiText;

	public Text wifiSpeedText;

	// Use this for initialization
	void Start () {
		internetPlugin = InternetPlugin.GetInstance();
		internetPlugin.SetDebug(0);
		internetPlugin.setInternetCallbackListener(OnWifiConnect,OnWifiDisconnect,OnWifiSignalStrengthChange);
		internetPlugin.RegisterEvent();
	}

	private void OnApplicationPause(bool val){
		if(internetPlugin!=null){
			if(val){
				internetPlugin.UnRegisterEvent();
			}else{
				internetPlugin.RegisterEvent();
			}
		}
	}

	public void ScanWifi(){
		internetPlugin.ScanWifi();
	}

	public void CheckMobileConnection(){
		bool status = internetPlugin.IsMobileConnected();
		if(mobileConnectionText!=null){
			mobileConnectionText.text = string.Format("Is Mobile Connected: {0}", status);
		}
	}

	public void CheckWifiConnection(){
		bool status = internetPlugin.IsWifiConnected();
		if(wifiConnectionText!=null){
			wifiConnectionText.text = string.Format("Is Wifi Connected: {0}", status);
		}
	}

	public void GetWifiIP(){
		string wifiIP = internetPlugin.GetWifiIP();
		if(wifiIPText!=null){
			wifiIPText.text = string.Format("Wifi IP: {0}", wifiIP);
		}
	}

	public void GetWifiSSID(){
		string wifiSSID = internetPlugin.GetWifiSSID();
		if(wifiSSIDText!=null){
			wifiSSIDText.text = string.Format("wifi SSID: {0}", wifiSSID);
		}
	}

	public void GetWifiBSSID(){
		string wifiBSSID = internetPlugin.GetWifiBSSID();
		if(wifiBSSIDText!=null){
			wifiBSSIDText.text = string.Format("wifi BSSID: {0}", wifiBSSID);
		}
	}

	public void GetWifiRssi(){
		string wifiRssi = internetPlugin.GetWifiRssi();
		if(wifiRssiText!=null){
			wifiRssiText.text = string.Format("wifi Rssi: {0}", wifiRssi);
		}
	}

	public void GetWifiSpeed(){
		string wifiSpeed = internetPlugin.GetWifiSpeed();
		if(wifiSpeedText!=null){
			wifiSpeedText.text = string.Format("wifi speed: {0}", wifiSpeed);
		}
	}
	
	void OnWifiConnect(){
		Debug.Log("[InternetInfoDemo] OnWifiConnect");
	}
	
	void OnWifiDisconnect(){
		Debug.Log("[InternetInfoDemo] OnWifiDisconnect");
	}
	
	void OnWifiSignalStrengthChange(int signalStrength, int signalDifference){
		if(wifiSignalStrengthText!=null){
			wifiSignalStrengthText.text = string.Format("wifi Signal Strength: {0}", signalStrength);
		}
		Debug.Log("[InternetInfoDemo] OnWifiSignalStrengthChange signalStrength " + signalStrength + " signalDifference " + signalDifference);
	}
}
