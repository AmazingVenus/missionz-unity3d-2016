﻿using UnityEngine;
using System.Collections;

public class AnimalBehaviour : MonoBehaviour {
	public Vector3 boundaryPoint1;
	public Vector3 boundaryPoint2;
	//public Transform CrosshairPosition;
	// Use this for initialization

	public bool isMoving;
	public bool AtTarget;
	public bool isAttacking;
    public bool isDead;

	public float delayTime;
	public float delay;
	public float attackTime = 1.5f;
	public Vector3 targetPos;

    public ParticleSystem AnimalSleepFallParticle;
    public GameObject stageController;
    public int damage;
    public Animator AC;
	void Start () {
        isDead = true;
        AC = GetComponent<Animator> ();
        //CrosshairPosition = GameObject.Find ("Crosshair").transform;
        stageController = GameObject.Find("StageController");
        damage = stageController.GetComponent<StageController>().AnimalDamage;
    }
	
	// Update is called once per frame
	void Update () {
		boundaryPoint1.x = transform.position.x - 1f;
		boundaryPoint1.y = transform.position.y + 1f;

		boundaryPoint2.x = transform.position.x + 1f;
		boundaryPoint2.y = transform.position.y - 1f;

		//this.transform.position = RandomNewPosition (this.transform.position);
		//this.transform.position = Vector3.MoveTowards (this.transform.position, RandomNewPosition (this.transform.position), 5f);


		if (!isMoving) {

			//while animal stop walking
			if(AtTarget){
				delay -= Time.deltaTime;
				AC.SetBool("Stop",true);

				if(isAttacking){
					AttackPlayer();
					isAttacking = false;
				}
			}

			//when animal start walking
			if(delay <= 0f){
				DetectDirection(targetPos);
				this.transform.position = Vector3.MoveTowards (this.transform.position, targetPos , 0.03f);
				AtTarget = false;

				AC.SetBool("Walking",true);

				if(Vector3.Distance(this.transform.position,targetPos) <= 0.01f){
					isMoving = true;
					AtTarget = true;
					AC.SetBool("Walking",false);
				}
			}
		} else {
			delay = delayTime;
			isMoving = false;
			targetPos = RandomNewPosition (this.transform.position);
			isAttacking = true;
		}
	}

	Vector3 RandomNewPosition(Vector3 currentPosition){
		//Screen size
		float H = 5.3f;
		float W = 4f;

		float px = (currentPosition.x + 2f) / W;
		float py = (currentPosition.y + 2.3f) / H;



		if (px <= 0.1f) {
			currentPosition.x = Random.Range(currentPosition.x, boundaryPoint2.x);
		} else if(px >= 0.85f){
			currentPosition.x = Random.Range(currentPosition.x, boundaryPoint1.x);
		} else {
			currentPosition.x = Random.Range(boundaryPoint1.x, boundaryPoint2.x);
		}

		if (py <= 0.1f) {
			currentPosition.y = Random.Range(currentPosition.y, boundaryPoint1.y);
		} else if(py >= 0.85f){
			currentPosition.y = Random.Range(currentPosition.y, boundaryPoint2.y);
		} else {
			currentPosition.y = Random.Range(boundaryPoint1.y, boundaryPoint2.y);
		}

		/*
		if (Vector2.Distance (currentPosition, CrosshairPosition.position) <= 0.5f) {
			if (px <= 0.5f) {
				currentPosition.x = Random.Range(0f, 1.5f);
			} else {
				currentPosition.x = Random.Range(0f, 1.5f);
			}
			
			if (py <= 0.5f) {
				currentPosition.y += Random.Range(0f, 1.5f);
			} else {
				currentPosition.y -= Random.Range(0f, 1.5f);
			}
		
		}
		*/
		return currentPosition;
	}

	public void AttackPlayer(){
        float chance;
		float r = Random.value;
        int level = GameplayController.instance.stageController.GetComponent<StageController>().AnimalLevel;

        if (level >= 80)
        {
            chance = 1;
        }
        else if (level >= 60)
        {
            chance = 0.8f;
        }
        else if (level >= 40)
        {
            chance = 0.6f;
        }
        else if (level >= 20)
        {
            chance = 0.5f;
        }
        else {
            chance = 0.4f;
        }

		if (r <= chance) {// Chance to attack player
			AC.SetTrigger ("Attack");
			delay += attackTime;
			StartCoroutine(ApplyDamage(damage));
		}

	}

	public void DetectDirection(Vector3 destPos){

		if (transform.position.x - destPos.x > 0) {
			if(transform.localScale.x < 0f){
				//Vector3 n = new Vector3(-1,1,1);
				Vector3 temp = transform.localScale;
				temp.x = temp.x * - 1;
				transform.localScale = temp;
			}
		} else {
			if(transform.localScale.x > 0f){
				//Vector3 n = new Vector3(-1,1,1);
				Vector3 temp = transform.localScale;
				temp.x = temp.x * - 1;
				transform.localScale = temp;
			}
		}
	}

	public IEnumerator ApplyDamage(float damage){
		yield return new WaitForSeconds(1.7f);
		GameplayController.instance.stageController.GetComponent<StageController>().PlayerCurrentHP -= damage/2f;
        GameplayController.instance.heartFall.Play();
		yield return new WaitForSeconds(0.4f);
		GameplayController.instance.stageController.GetComponent<StageController>().PlayerCurrentHP -= damage/2f;
        GameplayController.instance.heartFall.Play();
    }

    public void getHitByPlayer() {
        AnimalSleepFallParticle.Play();
    }

    public void Dead() {
        if (isDead)
        {
            AC.SetTrigger("Dead");
            isDead = false;
        }
    }
}
