﻿using UnityEngine;
using System.Collections;

public class PlayerPinController : MonoBehaviour {
    //public TextMesh DistanceText;
    public GameObject ProfileImageHolder;
	// Use this for initialization
	void Start () {
        GameObject facebookObject = GameObject.FindGameObjectWithTag("Facebook");
        ProfileImageHolder.GetComponent<SpriteRenderer>().sprite = facebookObject.GetComponent<FBHolder>().ProfileImage;
	}
	
	// Update is called once per frame
	void Update () {
		//DistanceText.text = DataCenterModel.instance.playerMng.CurrentDistance.ToString ()+" m.";
	}
}
