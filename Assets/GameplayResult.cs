﻿using UnityEngine;
using System.Collections;

public class GameplayResult : MonoBehaviour {
	public bool isLevelUp;
	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		isLevelUp = GamePlayFinishScript.instance.isLevelUp;
	}
}
