﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameplayBackground : MonoBehaviour {
	public string deviceName;
	public WebCamTexture webCamTexture;

    IEnumerator Start()
    {
        yield return Application.RequestUserAuthorization(UserAuthorization.WebCam | UserAuthorization.Microphone);
        if (Application.HasUserAuthorization(UserAuthorization.WebCam | UserAuthorization.Microphone))
        {
            deviceName = WebCamTexture.devices[0].name;
            webCamTexture = new WebCamTexture(deviceName, 400, 225);
            webCamTexture.Play();
        }
        else
        {

        }
    }
    // Update is called once per frame
    void Update () {

		
	
		GetComponent<Renderer>().material.mainTexture = webCamTexture;
	
	}

}
