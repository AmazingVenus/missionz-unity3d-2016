﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameplayController : MonoBehaviour {
    public GameObject CameraBackground;
	public GameObject stageController;
	public static GameplayController instance;
	public Slider PlayerHealthBar;
	public Slider AnimalHealthBar;
	public Text sleepDartText;
    

	public GameObject crossHair;
	public GameObject SelectedAnimal;
	public Transform SpawnAnimalPoint;
	public GameObject[] AnimalPool;

	private float reloadTime = 1;
	public float reload;
	public bool isLoading;
	public int expReceive;

	public float totalTime;
	public int totalFireDart = 1;
	public int totalHitDart = 1;
    public float acc;
    public ParticleSystem heartFall;

    public AudioClip ShootSound;
    public AudioClip hitSound;
    void Awake(){
		instance = this;
	}
	// Use this for initialization
	void Start () {
        


        heartFall = this.GetComponent<EffectHandler>().PlayerHeartFallParticle.GetComponent<ParticleSystem>();

        stageController = GameObject.Find("StageController");
        SelectedAnimal = (GameObject)Instantiate(SelectAnimal(stageController.GetComponent<StageController>().AnimalName), SpawnAnimalPoint.position, Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update () {
		totalTime += Time.deltaTime;
		sleepDartText.text = stageController.GetComponent<StageController> ().sleepDart.ToString ();

		PlayerHealthBar.maxValue = stageController.GetComponent<StageController> ().PlayerMaxHP;
		PlayerHealthBar.value = stageController.GetComponent<StageController> ().PlayerCurrentHP;
		PlayerHealthBar.transform.FindChild ("value").GetComponent<Text> ().text = ((int)(100f*(PlayerHealthBar.value/PlayerHealthBar.maxValue))).ToString()+" %";

		AnimalHealthBar.maxValue = 100;
		AnimalHealthBar.value = stageController.GetComponent<StageController> ().AnimalHP;
		AnimalHealthBar.transform.FindChild ("value").GetComponent<Text> ().text = ((int)(100f*(AnimalHealthBar.value/AnimalHealthBar.maxValue))).ToString()+" %";

		if (reload >= 0) {
			reload -= Time.deltaTime;
			isLoading = true;
		} else {
			isLoading = false;
		}


		expReceive = GetComponent<GamePlayFinishScript> ().receiveExp;
	}

	public void AnimalTakingDamage(float damage){
		stageController.GetComponent<StageController> ().AnimalHP -= damage;
	}

	public void ShootAnimal(){
		Debug.Log("Shooting");
        
		totalFireDart++;
		if (reload <= 0) {
			reload = reloadTime;
			stageController.GetComponent<StageController> ().sleepDart--;
            GetComponent<AudioSource>().PlayOneShot(ShootSound);
            if (crossHair.GetComponent<CrossfireScript> ().onTarget) {
				AnimalTakingDamage (Random.Range (8, 15));
                GetComponent<AudioSource>().PlayOneShot(hitSound);
                totalHitDart++;
                crossHair.GetComponent<CrossfireScript>().animalTransform.GetComponent<AnimalBehaviour>().getHitByPlayer();
            }
		}
        acc = (totalHitDart / (float)totalFireDart) * 100f;
    }

    public GameObject SelectAnimal(string AnimalName)
    {
        if (AnimalName.Equals("Lion"))
        {
            return AnimalPool[0];

        }
        else if (AnimalName.Equals("Rabbit"))
        {
            return AnimalPool[1];

        }
        else if (AnimalName.Equals("Elephen"))
        {
            return AnimalPool[2];
        }
        else if (AnimalName.Equals("Monkey"))
        {
            return AnimalPool[3];
        }
        else if (AnimalName.Equals("Bear"))
        {
            return AnimalPool[4];
        }
        else if (AnimalName.Equals("Raccoon"))
        {
            return AnimalPool[5];
        }
        else {
            return AnimalPool[0];
        }

    }
}
