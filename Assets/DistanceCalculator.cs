﻿using UnityEngine;
using System.Collections;

public class DistanceCalculator : MonoBehaviour {
    public Location L1;
    public Location L2;
    public double L1_lat;
    public double L1_long;
    public double L2_lat;
    public double L2_long;
    public double Distance;
	// Use this for initialization
	void Start () {
        L1 = new Location();
        L2 = new Location();
	}
	
	// Update is called once per frame
	void Update () {
        L1.Latitude = L1_lat;
        L1.Longitude = L1_long;

        L2.Latitude = L2_lat;
        L2.Longitude = L2_long;

        Distance = DistanceCalculate.GreatCircleDistance(L1, L2)*1000;
	}
}
