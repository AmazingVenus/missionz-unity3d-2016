﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//using System;

public class GamePlayFinishScript : MonoBehaviour {
	public static GamePlayFinishScript instance;

	private float animalHP;
	private float playerHP;
	private int numberSleepDart;
	// Use this for initialization

	public bool isSuccess;
	public bool isFinshed = false;
	public bool isLevelUp;
    private bool isReceivedReward;

	public GameObject successDialog;
	public GameObject failDialog;


	public int receiveExp;
	public int receiveScore;
	public int receiveCion;

    public AudioClip PlayerWin;
    public AudioClip PlayerLose;
    public AudioClip PlayerNearDead;
    public AudioClip BGM;

    public GameObject LowHPText;
    int i = 1; 
	void Awake(){
		instance = this;
	
	}

	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		animalHP = this.GetComponent<GameplayController>().stageController.GetComponent<StageController> ().AnimalHP;
		playerHP = this.GetComponent<GameplayController>().stageController.GetComponent<StageController> ().PlayerCurrentHP;
		numberSleepDart = this.GetComponent<GameplayController>().stageController.GetComponent<StageController> ().sleepDart;

        if ((playerHP / this.GetComponent<GameplayController>().stageController.GetComponent<StageController>().PlayerMaxHP) <= 0.25f) {
            
            if (i > 0) {
                LowHPText.SetActive(true);
                GetComponent<AudioSource>().clip = PlayerNearDead;
                GetComponent<AudioSource>().Play();
                i = 0;
            }
            
        }
		if (animalHP <= 0) {
			isSuccess = true;
			isFinshed = true;
        }

		if (numberSleepDart <= 0 || playerHP <= 0) {
			isSuccess = false;
			isFinshed = true;
            
        }

		if (isFinshed && !isReceivedReward) {
            Debug.Log("isFinshed");
            LowHPText.SetActive(false);
            GetComponent<AudioSource>().clip = BGM;
            GetComponent<AudioSource>().Play();

            try
            {
                //Destroy(this.GetComponent<GameplayController>().CameraBackground.gameObject);
                GetComponent<GameplayController>().CameraBackground.GetComponent<GameplayBackground>().webCamTexture.Stop();
                GetComponent<GameplayController>().CameraBackground.GetComponent<GameplayBackground>().webCamTexture = null;
            }
            catch
            {
                if (GetComponent<GameplayController>().CameraBackground.GetComponent<GameplayBackground>().webCamTexture != null)
                {
                    GetComponent<GameplayController>().CameraBackground.GetComponent<GameplayBackground>().webCamTexture = null;
                }
            }
            finally
            {
                Destroy(GetComponent<GameplayController>().CameraBackground.gameObject);
            }

            //Calculate exp
            receiveExp = receiveExpCalculator(GetComponent<GameplayController>().stageController.GetComponent<StageController> ().AnimalLevel);
            
            //Save Score Data
            receiveScore = CalulateRecieveScore(GetComponent<GameplayController>().stageController.GetComponent<StageController>().PlayerLevel,GetComponent<GameplayController>().stageController.GetComponent<StageController> ().AnimalLevel,GetComponent<GameplayController>().totalTime, GetComponent<GameplayController>().acc);
            //Destroy(GameObject.FindGameObjectWithTag("Animal"));

            //Change BMG
           

            if (!isSuccess)
            {
                receiveCion = 80;
                receiveScore = 0;
                receiveExp = 0;
                failDialog.SetActive(true);
                GetComponent<AudioSource>().PlayOneShot(PlayerLose);

            }
            else
            {
                receiveCion = 80 + Mathf.CeilToInt(receiveScore * 0.5f);
                GetComponent<GameplayController>().SelectedAnimal.GetComponent<AnimalBehaviour>().Dead();
                GetComponent<GameplayController>().SelectedAnimal.GetComponent<AnimalBehaviour>().damage = 0;
                StartCoroutine(Delay(2f));
            }
            int OldScore = PlayerPrefs.GetInt(MePlayerPrefs.SCORE_KEY, 0);
            int OldScoreRecord = PlayerPrefs.GetInt(MePlayerPrefs.SCORE_RECORD_KEY, 0);
            int newScore = OldScore + receiveScore;
            int newScoreRecord = OldScoreRecord + receiveScore;

                PlayerPrefs.SetInt(MePlayerPrefs.SCORE_KEY, newScore);
                PlayerPrefs.SetInt(MePlayerPrefs.SCORE_RECORD_KEY, newScoreRecord);

                //เพิ่ม coin ที่ได้ลงใน PlayerPref
                //int newCoin = PlayerPrefs.GetInt(BackpackPlayerPrefs.MONEY_KEY) + receiveCion;
                //PlayerPrefs.SetInt(BackpackPlayerPrefs.MONEY_KEY, newCoin);

                //ChallengeManager.IncreaseCurrentMoneyChallenge(receiveCion);
                isReceivedReward = true;
                Debug.Log("isRecieved");

            
            PlayerPrefs.SetInt(BackpackPlayerPrefs.SLEEP_DART_KEY, GetComponent<GameplayController>().stageController.GetComponent<StageController>().sleepDart);
            
           // this.GetComponent<GameplayController>().CameraBackground.SetActive(false);
			if (isSuccess) {
                
				
			} else {
				
			}
		} else {
			//successDialog.SetActive (false);
            //failDialog.SetActive (false);
		}
	}

	public void backToMap(){
        if (isSuccess) {
            AddAnimalToCollection(this.GetComponent<GameplayController>().stageController.GetComponent<StageController>().AnimalName);
        }
	

		int Level = PlayerPrefs.GetInt (MePlayerPrefs.LEVEL_KEY, 1);
		int MaxExp = getExpForNextLevel (Level);
		int oldExp = PlayerPrefs.GetInt (MePlayerPrefs.EXP_CURRENT_KEY,0);
		int newExp = oldExp + receiveExp;

		float MaxHP = MaxHPCalculation (Level);
        float CurrentHP = playerHP;
        
        //when player get level up
        if (newExp >= MaxExp) {
			Level++;
			MaxHP = MaxHPCalculation (Level);
			CurrentHP = MaxHP;
			isLevelUp = true;
            PlayerPrefs.SetInt(MePlayerPrefs.ENERGY_CURRENT_KEY, 100);
            PlayerPrefs.SetInt(MePlayerPrefs.LEVEL_KEY, Level);
        }


        //เพิ่ม coin ที่ได้ลงใน PlayerPref
        int newCoin = PlayerPrefs.GetInt(BackpackPlayerPrefs.MONEY_KEY) + receiveCion;
        PlayerPrefs.SetInt(BackpackPlayerPrefs.MONEY_KEY, newCoin);
        ChallengeManager.IncreaseCurrentMoneyChallenge(receiveCion);


        PlayerPrefs.SetInt(MePlayerPrefs.EXP_CURRENT_KEY,newExp);
		PlayerPrefs.SetInt(MePlayerPrefs.LEVEL_KEY,Level);
		PlayerPrefs.SetInt(MePlayerPrefs.HEALTH_CURRENT_KEY,(int)CurrentHP);
        ///
        Debug.Log("HEALTH_CURRENT_KEY " + PlayerPrefs.GetInt(MePlayerPrefs.HEALTH_CURRENT_KEY));

        DestroyObject (GameplayController.instance.stageController);
		DestroyObject (this.gameObject);
        try
        {
            Debug.Log("LoadScene");
            Application.LoadLevel("MainLoadingScreen");
        }
        catch{
         
        }
	}

	public int receiveExpCalculator(int animalLevel){
		float a = 6.5f * Mathf.Pow(1.5f,0.1f * animalLevel);
		float b = Mathf.Pow (0.991f, animalLevel);

		return Mathf.CeilToInt(a/b);

	}

	public int getExpForNextLevel(int currentLevel){
		return Mathf.CeilToInt (10 * currentLevel * Mathf.Pow (1.5f, 0.18f * currentLevel));
	}

	public float MaxHPCalculation(int level){
		return Mathf.Ceil( 100*Mathf.Pow(1.1f,0.67f*level));
	}

	public int CalulateRecieveScore(int playerLevel,int animalLevel,float time,float acc){
		
		float a = 0.1f * acc * Mathf.Pow (2, animalLevel - playerLevel);
		float b = 0.35f * time;
		return Mathf.CeilToInt((a / b) * 100)+128;

	}

    public void AddAnimalToCollection(string AnimalName)
    {
        ChallengeManager.IncreaseCurrentHunterChallenge(1);
        Debug.Log("Animal Name : " + AnimalName);
        if (AnimalName.Equals("Lion"))
        {
            int old = PlayerPrefs.GetInt(CollectionPlayerPrefs.LION_KEY, 0);
            old++;
            PlayerPrefs.SetInt(CollectionPlayerPrefs.LION_KEY, old);
        }
        else if (AnimalName.Equals("Rabbit"))
        {
            int old = PlayerPrefs.GetInt(CollectionPlayerPrefs.RABBIT_KEY, 0);
            old++;
            PlayerPrefs.SetInt(CollectionPlayerPrefs.RABBIT_KEY, old);
        }
        else if (AnimalName.Equals("Elephen"))
        {
            int old = PlayerPrefs.GetInt(CollectionPlayerPrefs.ELEPHANT_KEY, 0);
            old++;
            PlayerPrefs.SetInt(CollectionPlayerPrefs.ELEPHANT_KEY, old);
        }
        else if (AnimalName.Equals("Monkey"))
        {
            
            int old = PlayerPrefs.GetInt(CollectionPlayerPrefs.MONKEY_KEY, 0);
            Debug.Log("Monkey : " + old);
            old++;
            Debug.Log("Monkey2 : " + old);
            PlayerPrefs.SetInt(CollectionPlayerPrefs.MONKEY_KEY, old);
            PlayerPrefs.Save();
            Debug.Log("Monkey3 : " + PlayerPrefs.GetInt(CollectionPlayerPrefs.MONKEY_KEY, 0));
        }
        else if (AnimalName.Equals("Bear"))
        {
            int old = PlayerPrefs.GetInt(CollectionPlayerPrefs.BEAR_KEY, 0);
            old++;
            PlayerPrefs.SetInt(CollectionPlayerPrefs.BEAR_KEY, old);
        }
        else if (AnimalName.Equals("Raccoon"))
        {
            int old = PlayerPrefs.GetInt(CollectionPlayerPrefs.RACCOON_KEY, 0);
            old++;
            PlayerPrefs.SetInt(CollectionPlayerPrefs.RACCOON_KEY, old);
        }

    }

    public IEnumerator Delay(float Second) {
        yield return new WaitForSeconds(Second);
        GetComponent<AudioSource>().PlayOneShot(PlayerWin);
        successDialog.SetActive(true);
    }

    public void increaseCoin(int coin) {
        receiveCion += coin;

    }
}
