﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SuccessDialog : MonoBehaviour {

	public Text ExpText;
	public Text ScoreText;
    public Text CoinText;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		ExpText.text = GameplayController.instance.expReceive.ToString ();
		ScoreText.text = GamePlayFinishScript.instance.receiveScore.ToString ();
        CoinText.text = GamePlayFinishScript.instance.receiveCion.ToString();
	}
}
