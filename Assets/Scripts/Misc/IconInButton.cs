﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IconInButton : MonoBehaviour {

	public Button ButtonLayer;
	private Color c;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		c = GetComponent<Image> ().color;
		c.a = ButtonLayer.GetComponent<Image> ().color.a;
		GetComponent<Image> ().color = c;
	}
}
