﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextInButton : MonoBehaviour {
	public Button ButtonLayer;
	private Color c;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		c = GetComponent<Text> ().color;
		c.a = ButtonLayer.GetComponent<Image> ().color.a;
		GetComponent<Text> ().color = c;
	}
}
