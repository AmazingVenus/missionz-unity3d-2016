﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GetStringValue : MonoBehaviour {
	public int ID;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		switch (ID) {
		
		case 1 :
			GetComponent<Text> ().text = DataCenterModel.instance.playerMng.Weight.ToString ()+"kg.";
			break;
		case 2 :
			GetComponent<Text> ().text = DataCenterModel.instance.playerMng.Height.ToString ()+"cm.";
			break;
		}
	}
}
