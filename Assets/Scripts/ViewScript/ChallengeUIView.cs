﻿using UnityEngine;
using System.Collections;

public class ChallengeUIView : MonoBehaviour {
	public GameObject HighLightTab;
	public GameObject Tab1;
	public GameObject Tab2;
	public int CurrentTabIndex;
	
	public Vector3 TabPosition1;
	public Vector3 TabPosition2;
	// Use this for initialization
	void Start () {
		TabPosition1 = new Vector3 (-66.157f, 150f,0f);
		TabPosition2 = new Vector3 (69.2f, 150f,0f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void SwitchTab(int TabIndex){
		if (CurrentTabIndex != TabIndex) {
			
			if(TabIndex == 1){
				HighLightTab.GetComponent<RectTransform>().localPosition = TabPosition1;
				Tab1.SetActive(true);
				Tab2.SetActive(false);
			}else{
				HighLightTab.GetComponent<RectTransform>().localPosition = TabPosition2;
				Tab2.SetActive(true);
				Tab1.SetActive(false);
			}
			
			CurrentTabIndex = TabIndex;
		} else {
			
			
		}
	}
}
