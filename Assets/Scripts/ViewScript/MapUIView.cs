﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MapUIView : MonoBehaviour {
	public Slider HealthBar;
	public Slider EnergyBar;
	public Slider ExpBar;

	public Text LevelText;
    public Text ScoreText;
    //public Text DistanceText;

	public GameObject BackpackBtn;
	public GameObject ShopBtn;
	public GameObject RankBtn;
	public GameObject CollectionBtn;
	public GameObject ChallengeBtn;
    public GameObject SettingBtn;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
