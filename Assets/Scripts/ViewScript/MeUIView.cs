﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MeUIView : MonoBehaviour {
	public GameObject HighLightTab;
	public GameObject Tab1;
	public GameObject Tab2;
	public int CurrentTabIndex;

	public Text HpValue;
	public Text ExpValue;
	public Text EnergyValue;
    public Text PlayerName;
    public Text CalBurnedText;
    public Text DistanceTravelled;
    public Text ScoreText;
    public Text LevelText;
    public Image ProfileImage;

    public Text BurnedRecordText;
    public Text DistanceRecordText;
    public Text ScoreRecordText;

	public Vector3 TabPosition1;
	public Vector3 TabPosition2;
	// Use this for initialization
	void Start () {

		TabPosition1 = new Vector3 (-66.157f, -3f,0f);
		TabPosition2 = new Vector3 (67.518f, -3f,0f);
	}
	
	// Update is called once per frame
	void Update () {
        PlayerName.text = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_NAME_KEY, "...");
		HpValue.text = ((int)DataCenterModel.instance.playerMng.CurrentHP).ToString () + "/" + ((int)DataCenterModel.instance.playerMng.MaxHP).ToString ();
		ExpValue.text = DataCenterModel.instance.playerMng.CurrentExp.ToString () + "/" + DataCenterModel.instance.playerMng.MaxExp.ToString ();
		EnergyValue.text = DataCenterModel.instance.playerMng.CurrentEnergy.ToString () + "/100";
        ProfileImage.sprite = GameObject.FindGameObjectWithTag("Facebook").GetComponent<FBHolder>().ProfileImage;
        CalBurnedText.text = Mathf.FloorToInt(DataCenterModel.instance.playerMng.Burned)+" kcal";
        DistanceTravelled.text = ((int)DataCenterModel.instance.playerMng.CurrentDistance) + "m";
        ScoreText.text = DataCenterModel.instance.playerMng.Score.ToString();

        BurnedRecordText.text = (int)DataCenterModel.instance.playerMng.BurnedRecord + " kcal";
        DistanceRecordText.text = (int)DataCenterModel.instance.playerMng.DistanceRecord + "m";
        ScoreRecordText.text = DataCenterModel.instance.playerMng.ScoreRecord.ToString();
        LevelText.text = DataCenterModel.instance.playerMng.Level.ToString();
    }

	public void SwitchTab(int TabIndex){
		if (CurrentTabIndex != TabIndex) {

			if(TabIndex == 1){
				HighLightTab.GetComponent<RectTransform>().localPosition = TabPosition1;
				Tab1.SetActive(true);
				Tab2.SetActive(false);
			}else{
				HighLightTab.GetComponent<RectTransform>().localPosition = TabPosition2;
				Tab2.SetActive(true);
				Tab1.SetActive(false);
			}

			CurrentTabIndex = TabIndex;
		} else {


		}
	}
}
