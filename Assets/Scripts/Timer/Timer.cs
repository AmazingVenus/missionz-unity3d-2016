﻿using UnityEngine;
using System;
using System.Collections;

public class Timer : MonoBehaviour {



	// Use this for initialization
	void Start () {
        checkTime();
	}
	
	// Update is called once per frame
	void Update () {
        checkTime();
       // Debug.Log("Playerpref Challenge " + PlayerPrefs.GetInt(ChallengePlayerPrefs.CHALLENGE_ACTIVE_KEY,-1));
	}

    public void checkTime() {
        //9:51 PM
        if (DateTime.Now.ToShortTimeString().Equals("11:59 AM"))
        {
            PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_BURNED_CHALLENGE_KEY, 0);
            PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_MONEY_CHALLENGE_KEY, 0);
            PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_HUNTER_CHALLENGE_KEY, 0);
            PlayerPrefs.SetInt(ChallengePlayerPrefs.CHALLENGE_ACTIVE_KEY, -1);
            PlayerPrefs.Save();
            
        }

        if (DateTime.Now.ToShortTimeString().Equals("12:00 AM")) {
            DataCenterModel.instance.playerMng.CurrentEnergy = 100;
        }
    }
        
    }

