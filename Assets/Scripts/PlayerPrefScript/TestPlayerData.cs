﻿using UnityEngine;
using System.Collections;

public class TestPlayerData : MonoBehaviour {
    public string facebookId;
   
    public int MeBurn = 0, MeBurnRecord = 0, MeDistance = 0, MeDISTANCE_RECORD_KEY = 0, currentEnergy = 0, energyFull = 0, expCurrent = 0,
expMax = 11, healthFull = 107, MeLevel = 1, MeScore = 0, MeSCORE_RECORD_KEY = 0, money = 300, currentHP = 107;

    public int lion = 0, elephant = 0, monkey = 0, raccoon = 0, bear = 0, rabbit = 0;

    public int sleepingDart = 15, healthPotion = 2, energyPotion = 2;

    public bool isSaveComplete;
    public bool ResetWhenStart;
    // Use this for initialization
    void Awake() {
        if (ResetWhenStart) {
            PlayerPrefs.DeleteAll();
        }

    }

    void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void InitializePlayerData()
    {
        //ME
        PlayerPrefs.SetString(MePlayerPrefs.FACEBOOK_ID_KEY, facebookId);
        PlayerPrefs.SetInt(MePlayerPrefs.HEALTH_CURRENT_KEY, currentHP);
        PlayerPrefs.SetInt(MePlayerPrefs.BURNED_RECORD_KEY, MeBurnRecord);
        PlayerPrefs.SetInt(MePlayerPrefs.BURNED_KEY, MeBurn);
        PlayerPrefs.SetInt(MePlayerPrefs.DISTANCE_RECORD_KEY, MeDISTANCE_RECORD_KEY);
        PlayerPrefs.SetInt(MePlayerPrefs.DISTANCE_TRAVELLED_KEY, MeDistance);
        PlayerPrefs.SetInt(MePlayerPrefs.ENERGY_CURRENT_KEY, currentEnergy);
        PlayerPrefs.SetInt(MePlayerPrefs.ENERGY_MAX_KEY, 100);
        PlayerPrefs.SetInt(MePlayerPrefs.EXP_CURRENT_KEY, expCurrent);
        PlayerPrefs.SetInt(MePlayerPrefs.EXP_MAX_KEY, expMax);
        PlayerPrefs.SetInt(MePlayerPrefs.HEALTH_MAX_KEY, healthFull);
        PlayerPrefs.SetInt(MePlayerPrefs.LEVEL_KEY, MeLevel);
        PlayerPrefs.SetInt(MePlayerPrefs.SCORE_KEY, MeScore);
        PlayerPrefs.SetInt(MePlayerPrefs.SCORE_RECORD_KEY, MeScore);


        //COLLECTION
        PlayerPrefs.SetInt(CollectionPlayerPrefs.BEAR_KEY, bear);
        PlayerPrefs.SetInt(CollectionPlayerPrefs.LION_KEY, lion);
        PlayerPrefs.SetInt(CollectionPlayerPrefs.RACCOON_KEY, raccoon);
        PlayerPrefs.SetInt(CollectionPlayerPrefs.RABBIT_KEY, rabbit);
        PlayerPrefs.SetInt(CollectionPlayerPrefs.MONKEY_KEY, monkey);
        PlayerPrefs.SetInt(CollectionPlayerPrefs.ELEPHANT_KEY, elephant);

        //BACKPACK
        PlayerPrefs.SetInt(BackpackPlayerPrefs.HEALTH_POTION_KEY, healthPotion);
        PlayerPrefs.SetInt(BackpackPlayerPrefs.ENERGY_POTION_KEY, energyPotion);
        PlayerPrefs.SetInt(BackpackPlayerPrefs.SLEEP_DART_KEY, sleepingDart);
        PlayerPrefs.SetInt(BackpackPlayerPrefs.MONEY_KEY, money);

        PlayerPrefs.Save();
        isSaveComplete = true;
    }
}
