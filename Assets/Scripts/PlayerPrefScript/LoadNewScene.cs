﻿using UnityEngine;
using System.Collections;
using Facebook;
public class LoadNewScene : MonoBehaviour {

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void goTOScene(string sceneName){
		Application.LoadLevel (sceneName);
	}

    public void deleteData() {
        ChallengeManager.ResetJsonFile();
        ActivityManager.ResetJsonFile();
        Destroy(GameObject.FindGameObjectWithTag("Facebook"));
        PlayerPrefs.DeleteAll();
    }

   
}
