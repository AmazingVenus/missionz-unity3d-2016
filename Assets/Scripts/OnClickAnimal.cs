﻿using UnityEngine;
using System.Collections;

public class OnClickAnimal : MonoBehaviour {
	public GameObject dialog;
	public GameObject menu;
    public GameObject mapUI;

    public bool isClickable;
	// Use this for initialization
	void Start () {

	}

	void OnMouseDown() {
		Debug.Log("Drag ended!222");
        //play ();
        if (!dialog.GetComponent<AnimalInfoDialog>().isShow && isClickable)
            showInfo();
    }

	// Update is called once per frame
	void Update () {
        isClickable = mapUI.active;
    }

	public void showInfo(){
		dialog.SetActive (true);
		dialog.GetComponent<AnimalInfoDialog>().animal = this.GetComponent<Animal> ();
        dialog.GetComponent<AnimalInfoDialog>().node = GetComponent<Node>();
		dialog.GetComponent<AnimalInfoDialog> ().fadeIn ();
	}

	public void showMenu(){
		menu.GetComponent<MenuPopup>().clicked_animal = this.gameObject;
		//menu.GetComponent<MenuPopup>().isClicked = false;

		if (!menu.GetComponent<MenuPopup> ().isClicked) {
			menu.GetComponent<ScalingEffect> ().currentScale = Vector2.zero;
			menu.GetComponent<ScalingEffect> ().Speed = 10;

			menu.transform.position = Input.mousePosition;
			menu.GetComponent<ScalingEffect> ().EndScale = Vector2.one;
			menu.GetComponent<ScalingEffect> ().play ();
		}
	}

	public void play(){
		StartCoroutine (Animate ());
	}
	
	public IEnumerator Animate(){
		yield return new WaitForSeconds(0.20f);
		showMenu ();
	}
}
