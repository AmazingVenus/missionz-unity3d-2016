﻿using UnityEngine;
using System.Collections;

public class DataCenterModel : MonoBehaviour {
	public static DataCenterModel instance;

	public LocationManagerModel locationMng;
	public PlayerManagerModel playerMng;
	// Use this for initialization
	void Start () {
		instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SaveDataToPlayerPref(string key, int value){
		PlayerPrefs.SetInt (key,value);
	}

	public void SaveDataToPlayerPref(string key, float value){
		PlayerPrefs.SetFloat(key,value);
	}

	public void SaveDataToPlayerPref(string key, string value){
		PlayerPrefs.SetString(key,value);
	}

    public void SaveAllData() {
        Debug.Log("SaveAllData()");
        PlayerPrefs.SetInt(MePlayerPrefs.DISTANCE_TRAVELLED_KEY, (int)playerMng.CurrentDistance);

        PlayerPrefs.SetInt(MePlayerPrefs.BURNED_KEY, (int)playerMng.Burned);
        PlayerPrefs.SetInt(MePlayerPrefs.WEIGHT_KEY, (int)playerMng.Weight);

        PlayerPrefs.SetInt(MePlayerPrefs.HEALTH_CURRENT_KEY, (int)playerMng.CurrentHP);//(int)playerMng.CurrentHP
        PlayerPrefs.SetInt(MePlayerPrefs.EXP_CURRENT_KEY, playerMng.CurrentExp);
        PlayerPrefs.SetInt(MePlayerPrefs.ENERGY_CURRENT_KEY, playerMng.CurrentEnergy);

        PlayerPrefs.SetInt(MePlayerPrefs.BURNED_RECORD_KEY, (int)playerMng.BurnedRecord);
        PlayerPrefs.SetInt(MePlayerPrefs.DISTANCE_RECORD_KEY, (int)playerMng.DistanceRecord);
        PlayerPrefs.SetInt(MePlayerPrefs.EXP_MAX_KEY, playerMng.MaxExp);
        PlayerPrefs.SetInt(MePlayerPrefs.HEALTH_MAX_KEY, (int)playerMng.MaxHP);
        PlayerPrefs.SetInt(MePlayerPrefs.LEVEL_KEY, playerMng.Level);
        PlayerPrefs.SetInt(MePlayerPrefs.SCORE_KEY, playerMng.Score);
        PlayerPrefs.SetInt(MePlayerPrefs.SCORE_RECORD_KEY, playerMng.ScoreRecord);


        /*
public float Height;
public float Weight;
public float CurrentHP;
public int Energy;
public int CurrentEnergy;
public float MaxHP;
public int Level;
public int CurrentExp;
public int MaxExp;
public float CurrentDistance;
public float TotalDistance;
public int Money;
public int Score;
public int SleepDart;
*/
    }
}
