﻿using UnityEngine;
using System.Collections;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System;
using Parse;

public static class ActivityManager
{
    public static string facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY);
   

    public static string jsonPath = Application.persistentDataPath +"/activity.json";

  

    public static void SaveActivityToJson(Activity activity)
    {
        List<Activity> tempActivities = new List<Activity>();

        //pull old data
        tempActivities = JsonConvert.DeserializeObject<List<Activity>>(File.ReadAllText(jsonPath));

        //push new one
        tempActivities.Add(activity);

        string jsonOutput = JsonConvert.SerializeObject(tempActivities, Formatting.Indented);
        File.WriteAllText(jsonPath, jsonOutput);

        //clear list
        tempActivities.Clear();
    }

    public static void SaveActivity(Activity activity)
    {
        //Date Operation
        DateTime nowDateTime = DateTime.Now; //วันนี้วันที่เท่าไหร่กี่โมง
        int countDay = PlayerPrefs.GetInt(ActivityPlayerPrefs.COUNT_DAY_KEY,1); //วันที่ต่างกันนับได้เท่าไหร่แล้ว

        string stringDateFromPP = PlayerPrefs.GetString(ActivityPlayerPrefs.DATE_LATEST_KEY, DateTime.Now.ToString()); //วันล่าสุดที่บันทึกไว้ใน PP

        DateTime tmpDateFromPP = Convert.ToDateTime(stringDateFromPP); //แปลงเป็น DateTime

        if (DateTime.Compare(nowDateTime.Date, tmpDateFromPP.Date) != 0) //ถ้าวันนี้ไม่ตรงกับวันล่าสุดที่เคยบันทึก นับเพิ่มวัน
        {
            countDay++;
        }



        //Other Operation -> PP
        int sumDistance = PlayerPrefs.GetInt(ActivityPlayerPrefs.SUM_DISTANCE_KEY,0);
        int sumBurned = PlayerPrefs.GetInt(ActivityPlayerPrefs.SUM_BURNED_KEY,0);

        sumBurned += activity.burned;
        sumDistance += activity.distance;

        SaveActivityToPlayerPrefs(sumBurned, sumDistance, countDay, nowDateTime);

        SaveActivityToJson(activity);
    }

    public static void ResetAllActivityPlayerPrefs()
    {
        PlayerPrefs.SetInt(ActivityPlayerPrefs.SUM_BURNED_KEY, 0);
        PlayerPrefs.SetInt(ActivityPlayerPrefs.SUM_DISTANCE_KEY, 0);
        PlayerPrefs.SetInt(ActivityPlayerPrefs.COUNT_DAY_KEY, 0);
        PlayerPrefs.SetString(ActivityPlayerPrefs.DATE_LATEST_KEY, "");
        PlayerPrefs.Save();
    }



    public static void ResetJsonFile()
    {
        string initialJson = "[]";
        File.WriteAllText(jsonPath, initialJson);
    }

    public static void SaveActivityToPlayerPrefs(int sumBurned, int sumDistance, int countDay, DateTime dateLatest)
    {
        PlayerPrefs.SetInt(ActivityPlayerPrefs.SUM_BURNED_KEY, sumBurned);
        PlayerPrefs.SetInt(ActivityPlayerPrefs.SUM_DISTANCE_KEY, sumDistance);
        PlayerPrefs.SetInt(ActivityPlayerPrefs.COUNT_DAY_KEY, countDay);
        PlayerPrefs.SetString(ActivityPlayerPrefs.DATE_LATEST_KEY, dateLatest.ToString());
        PlayerPrefs.Save();
    }

     public static void SaveActivityToParse()
    {
        List<Activity> tempActivities = new List<Activity>();
            tempActivities = JsonConvert.DeserializeObject<List<Activity>>(File.ReadAllText(ActivityManager.jsonPath));
        //ตรวจสอบว่ามีเคยมีข้อมูลของ facebookId นี้อยู่บ้างไหมใน parse.com
        var query = ParseObject.GetQuery("Activity")
           .WhereEqualTo("facebookId", facebookId);

        query.CountAsync().ContinueWith(t =>
        {
            int count = t.Result;
            if (count > 0)
            {
                Debug.Log("activity data exist");
                SendOnlyNewDataToParse(tempActivities); //save เฉพาะข้อมูลที่ใหม่กว่า
            }
            else
            {
                Debug.Log("activity data doesn't exist");
                SendAllDataToParse(tempActivities); //save ทั้งหมด
            }
        });
    }

    private static void SendAllDataToParse(List<Activity> tempActivities)
    {
        foreach (Activity activity in tempActivities)
        {

                ParseObject tmpParseObject = new ParseObject("Activity");
                tmpParseObject["facebookId"] = activity.facebookId; 
                tmpParseObject["minute"] = activity.minute;
                tmpParseObject["burned"] = activity.burned;
                tmpParseObject["distance"] = activity.distance;
                tmpParseObject["weight"] = activity.weight;
                tmpParseObject["saveDate"] = activity.saveDate;
                tmpParseObject.SaveAsync();
                Debug.Log("Activity saved");
            
        }
    }

    public static void SendOnlyNewDataToParse(List<Activity> tempActivities)
    {
        var query2 = ParseObject.GetQuery("Activity").WhereEqualTo("facebookId", facebookId);
        query2.OrderByDescending("saveDate").FirstAsync().ContinueWith(t2 =>
        {
            ParseObject lastestActivityFromParse = t2.Result;

            string dateTimeStyle = "MM/dd/yyyy hh:mm:ss tt";

            DateTime lastestDateFromParse = lastestActivityFromParse.Get<DateTime>("saveDate");
            string stringDateParse = lastestDateFromParse.ToString(dateTimeStyle);
            foreach (Activity activity in tempActivities)
            {
                
                //send data to parse if local data latest
                if (DateTime.Compare(activity.saveDate, lastestDateFromParse) > 0)
                {
                    if (stringDateParse.Equals(activity.saveDate.ToString(dateTimeStyle)))
                    {
                        ParseObject tmpParseObject = new ParseObject("Activity");
                        // tmpParseObject["facebookId"] = activity.facebookId;
                        tmpParseObject["minute"] = activity.minute;
                        tmpParseObject["burned"] = activity.burned;
                        tmpParseObject["distance"] = activity.distance;
                        tmpParseObject["weight"] = activity.weight;
                        tmpParseObject["saveDate"] = activity.saveDate;
                        tmpParseObject.SaveAsync();
                        Debug.Log("Activity saved");
                    }
                }
            }

        });
    }


public static void ShowActivityPlayerPrefs()
{
    Debug.Log(ActivityPlayerPrefs.SUM_BURNED_KEY + " : " + PlayerPrefs.GetInt(ActivityPlayerPrefs.SUM_BURNED_KEY));
    Debug.Log(ActivityPlayerPrefs.SUM_DISTANCE_KEY + " : " + PlayerPrefs.GetInt(ActivityPlayerPrefs.SUM_DISTANCE_KEY));
    Debug.Log(ActivityPlayerPrefs.COUNT_DAY_KEY + " : " + PlayerPrefs.GetInt(ActivityPlayerPrefs.COUNT_DAY_KEY));
    Debug.Log(ActivityPlayerPrefs.DATE_LATEST_KEY + " : " + PlayerPrefs.GetString(ActivityPlayerPrefs.DATE_LATEST_KEY));
}

public static void ShowAcitivitiesFromJson()
{
    List<Activity> tempActivities = new List<Activity>();
    tempActivities = JsonConvert.DeserializeObject<List<Activity>>(File.ReadAllText(ActivityManager.jsonPath));

    foreach (Activity activity in tempActivities)
    {
        Debug.Log(activity.burned + ", " + activity.distance
            + ", " + activity.facebookId + ", " + activity.minute + ", " + activity.saveDate
            + ", " + activity.weight);
    }
}

}
