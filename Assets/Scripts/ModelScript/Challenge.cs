﻿using UnityEngine;
using System.Collections;
using System;

public class Challenge
{
   // public int challengeId;
    public string facebookId;
    public string challengeName;
   
    public int challengeLevel;
    public int challengeTarget; //+"xxxx" for coin, m, KCal
    public int scoreReward;
    public DateTime saveDate;



    

    public Challenge(string facebookId, string challengeName, int challengeLevel, int challengeTarget, int scoreReward,DateTime saveDate)
    {
     //   challengeId = GetNewChallengeId();
        this.facebookId = facebookId;
        this.challengeName = challengeName;
        this.challengeLevel = challengeLevel;
        this.challengeTarget = challengeTarget;
        this.scoreReward = scoreReward;
        this.saveDate = saveDate;
    }

  /*  private int GetNewChallengeId()
    {
        return PlayerPrefs.GetInt(ChallengePlayerPrefs.challengeIdKey, 0) + 1;
    }*/


}
