﻿using UnityEngine;
using System.Collections;

public class PlayerManagerModel : MonoBehaviour {

	public float Height;
	public float Weight;
	public float CurrentHP;
	public int Energy;
	public int CurrentEnergy;
	public float MaxHP;
	public int Level;
	public int CurrentExp;
	public int MaxExp;
	public float CurrentDistance;
	public float DistanceRecord;
	public int Money;
	public int Score;
    public int ScoreRecord;
	public int SleepDart;
    public float Burned;
    public float BurnedRecord;
	public GameObject levelUpDialog;
	private bool trigger;
    // Use this for initialization
    void Awake() {

        Level = PlayerPrefs.GetInt(MePlayerPrefs.LEVEL_KEY, MePlayerPrefs.DEFAULT_LEVEL_KEY);
        // Debug.Log("Level : " + Level);
        // MaxHP = MaxHPCalculation(Level);
        // Debug.Log("MaxHP : " + MaxHP);
        // MaxHP = PlayerPrefs.GetInt(MePlayerPrefs.HEALTH_MAX_KEY, MePlayerPrefs.DEFAULT_HEALTH_MAX_KEY);
        Height = PlayerPrefs.GetInt(MePlayerPrefs.HEIGHT_KEY, MePlayerPrefs.DEFAULT_HEIGHT_KEY);
        Weight = PlayerPrefs.GetInt(MePlayerPrefs.WEIGHT_KEY, MePlayerPrefs.DEFAULT_WEIGHT_KEY);
        CurrentHP = PlayerPrefs.GetInt(MePlayerPrefs.HEALTH_CURRENT_KEY, MePlayerPrefs.DEFAULT_HEALTH_CURRENT_KEY);
        CurrentEnergy = PlayerPrefs.GetInt(MePlayerPrefs.ENERGY_CURRENT_KEY, MePlayerPrefs.DEFAULT_ENERGY_CURRENT_KEY);
        CurrentExp = PlayerPrefs.GetInt(MePlayerPrefs.EXP_CURRENT_KEY, MePlayerPrefs.DEFAULT_EXP_CURRENT_KEY);
        CurrentDistance = PlayerPrefs.GetInt(MePlayerPrefs.DISTANCE_TRAVELLED_KEY, MePlayerPrefs.DEFAULT_DISTANCE_TRAVELLED_KEY);
        Score = PlayerPrefs.GetInt(MePlayerPrefs.SCORE_KEY, MePlayerPrefs.DEFAULT_SCORE_KEY);
        Burned = PlayerPrefs.GetInt(MePlayerPrefs.BURNED_KEY, MePlayerPrefs.DEFAULT_BURNED_KEY);
        Money = PlayerPrefs.GetInt(BackpackPlayerPrefs.MONEY_KEY, BackpackPlayerPrefs.DEFAULT_MONEY_KEY);

        ScoreRecord = PlayerPrefs.GetInt(MePlayerPrefs.SCORE_RECORD_KEY, MePlayerPrefs.DEFAULT_SCORE_KEY);
        DistanceRecord = PlayerPrefs.GetInt(MePlayerPrefs.DISTANCE_RECORD_KEY, MePlayerPrefs.DEFAULT_DISTANCE_TRAVELLED_KEY);
        BurnedRecord = PlayerPrefs.GetInt(MePlayerPrefs.BURNED_RECORD_KEY, MePlayerPrefs.DEFAULT_BURNED_KEY);
    }

	void Start () {
		
        
        Debug.Log("PlayerMng " + PlayerPrefs.GetFloat(MePlayerPrefs.HEALTH_CURRENT_KEY));
    }
	
	// Update is called once per frame
	void Update () {
        //SleepDart = PlayerPrefs.GetInt(BackpackPlayerPrefs.SLEEP_DART_KEY,999);

        MaxHP = MaxHPCalculation(Level);
        MaxExp = getExpForNextLevel (Level);
		if (GameObject.Find ("Result") != null) {
			if(GameObject.Find ("Result").GetComponent<GameplayResult>().isLevelUp){
				levelUpDialog.SetActive (true);
			}
			Destroy(GameObject.Find ("Result").gameObject);
		}
        if (CurrentHP < 0) {
            CurrentHP = 0;
        }

        if (CurrentHP > MaxHP)
        {
            CurrentHP = MaxHP;
        }

        if (CurrentEnergy < 0)
        {
            CurrentEnergy = 0;
        }

        if (CurrentEnergy > 100)
        {
            CurrentEnergy = 100;
        }    
    }



	public float MaxHPCalculation(int level){
		return Mathf.Ceil( 100*Mathf.Pow(1.1f,0.67f*level));
	}

	public int getExpForNextLevel(int currentLevel){
		return Mathf.CeilToInt (10 * currentLevel * Mathf.Pow (1.5f, 0.18f * currentLevel));
	}


	public void LevelUP(){
		levelUpDialog.SetActive (true);
	}
}
