﻿using System;
using System.Collections;
using UnityEngine;

public class Activity {
  //  public int activityId { get; set; }
    public string facebookId { get; set; }
    public int minute { get; set; }
    public int burned { get; set; }
    public int distance { get; set;}
    public int weight { get; set; }
    public DateTime saveDate { get; set; }




    public Activity(string facebookId,int minute, int distance,int weight, int burned, DateTime saveDate) {
        
        this.facebookId = facebookId;
        this.minute = minute;
        this.distance = distance;
        this.weight = weight;
        
        this.saveDate = saveDate;

      //  saveDate = DateTime.Now;
      //  saveDate.ToLocalTime();

        this.burned = burned;
   
    }


 

}
