﻿using UnityEngine;
using Parse;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using System;

public class SaveChallengeData : MonoBehaviour {

    string facebookId;
    public int Done;
    public string jsonPath;
    // Use this for initialization
    void Start()
    {
        Done = 0;
        facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY);
    }

    // Update is called once per frame
    void Update () {
        jsonPath = Application.persistentDataPath + "/complete_challenge.json";
    }

    public void SaveToParse()
    {
        List<Challenge> tempChallenge = new List<Challenge>();
        tempChallenge = JsonConvert.DeserializeObject<List<Challenge>>(File.ReadAllText(jsonPath));

        if (tempChallenge.Count > 0)
        {

            //ตรวจสอบว่ามีเคยมีข้อมูลของ facebookId นี้อยู่บ้างไหมใน parse.com
            var query = ParseObject.GetQuery("ChallengeComplete")
               .WhereEqualTo("facebookId", facebookId);

            query.CountAsync().ContinueWith(t =>
            {
                t.Wait();
                int count = t.Result;
                if (count > 0)
                {
                    Debug.Log("challenge c data exist");
                    SendOnlyNewDataToParse(tempChallenge);
                }
                else
                {
                    Debug.Log("challenge c data doesn't exist");
                    SendAllDataToParse(tempChallenge);
                }
            });
        }
        else {
            Done = 1;
        }
    }

    private void SendAllDataToParse(List<Challenge> tempChallenge)
    {
        foreach (Challenge challenge in tempChallenge)
        {
            ParseObject tmpParseObject = new ParseObject("ChallengeComplete");
            tmpParseObject["facebookId"] = challenge.facebookId;
            tmpParseObject["challengeName"] = challenge.challengeName;
            tmpParseObject["challengeLevel"] = challenge.challengeLevel;
            tmpParseObject["target"] = challenge.challengeTarget;
            tmpParseObject["scoreReward"] = challenge.scoreReward;
            tmpParseObject["saveDate"] = challenge.saveDate;
            tmpParseObject.SaveAsync();
            Debug.Log("Challenge Complete Saved");
            Done = 1;
        }
    }

    public void SendOnlyNewDataToParse(List<Challenge> tempChallenge)
    {
        var query2 = ParseObject.GetQuery("ChallengeComplete").WhereEqualTo("facebookId", facebookId);
        query2.OrderByDescending("saveDate").FirstAsync().ContinueWith(t2 =>
        {//ต้องแก้
            t2.Wait();
            ParseObject lastestChallengeFromParse = t2.Result;
            string dateTimeStyle = "MM/dd/yyyy hh:mm:ss tt";

            DateTime lastestDateFromParse = lastestChallengeFromParse.Get<DateTime>("saveDate");
            string lastDateParseNewStyle = lastestDateFromParse.ToString(dateTimeStyle);

            foreach (Challenge challenge in tempChallenge)
            {

                //send data to parse if local data latest
                if (DateTime.Compare(challenge.saveDate, lastestDateFromParse) > 0)
                {
                    string saveDateTemp = challenge.saveDate.ToString(dateTimeStyle);
                    if (!lastDateParseNewStyle.Equals(saveDateTemp))
                    {
                        ParseObject tmpParseObject = new ParseObject("ChallengeComplete");
                        tmpParseObject["facebookId"] = challenge.facebookId;
                        tmpParseObject["challengeName"] = challenge.challengeName;
                        tmpParseObject["challengeLevel"] = challenge.challengeLevel;
                        tmpParseObject["target"] = challenge.challengeTarget;
                        tmpParseObject["scoreReward"] = challenge.scoreReward;
                        tmpParseObject["saveDate"] = challenge.saveDate;
                        tmpParseObject.SaveAsync();
                        Debug.Log("Challenge Complete Saved");
                    }
                    else
                    {
                        Debug.Log("same date");
                    }

                }
            }
            Done = 1;
        });
    }


}
