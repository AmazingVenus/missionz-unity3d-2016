﻿using UnityEngine;
using System.Collections;

public class SaveDataToParseController : MonoBehaviour {
    public SaveMeData MeDataObject;
    public SaveBackpackData BackpackDataObject;
    public SaveCollectionData CollectionDataObject;
    public SaveChallengeData ChallengeDataObject;
    public SaveActivityData ActivityDataObject;
    public SaveDataChallenge2 ChalengeDataObject2;
    public SaveRankData rankDataObject;

    public GameObject WaitingDialogObject;
    // Use this for initialization
    public int ProgressValue;

    private Vector2 currrentProgress;
	void Start () {

}
	
	// Update is called once per frame
	void Update () {
        ProgressValue = MeDataObject.Done + BackpackDataObject.Done + CollectionDataObject.Done + ChallengeDataObject.Done + ActivityDataObject.Done + ChalengeDataObject2.Done + rankDataObject.Done;
        currrentProgress = Vector2.Lerp(currrentProgress, new Vector2(ProgressValue,0), 4 * Time.deltaTime);
        WaitingDialogObject.GetComponent<WaitingDialog>().ProgressBar.maxValue = 7;

        WaitingDialogObject.GetComponent<WaitingDialog>().ProgressBar.value = currrentProgress.x;

        if (ProgressValue == 7)
        {
            StartCoroutine(HideDialog(1));
            
        }
    }

    public void SaveToParse() {
        WaitingDialogObject.SetActive(true);
        MeDataObject.SaveToParse();
        BackpackDataObject.SaveToParse();
        CollectionDataObject.SaveToParse();
        ChallengeDataObject.SaveToParse();
        ActivityDataObject.SaveActivityToParse();
        ChalengeDataObject2.SaveToParse();
        rankDataObject.SaveToParse();


    }

    public IEnumerator HideDialog(int delay) {
        yield return new WaitForSeconds(delay);
        WaitingDialogObject.SetActive(false);
        MeDataObject.Done = 0;
        BackpackDataObject.Done = 0;
        CollectionDataObject.Done = 0;
        ChallengeDataObject.Done = 0;
        ActivityDataObject.Done = 0;
        ChalengeDataObject2.Done = 0;
        rankDataObject.Done = 0;
    }
}
