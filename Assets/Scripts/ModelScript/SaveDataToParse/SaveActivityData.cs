﻿using UnityEngine;
using Parse;
using Newtonsoft.Json;
using System.IO;
using System.Collections.Generic;
using System;

public class SaveActivityData : MonoBehaviour {

    string facebookId;
    public int Done;
    public string jsonPath;
    // Use this for initialization
    void Start()
    {
        Done = 0;
        facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY);
    }

    // Update is called once per frame
    void Update () {
        jsonPath = Application.persistentDataPath + "/activity.json";
    }

    public void SaveActivityToParse()
    {
        List<Activity> tempActivities = new List<Activity>();
        tempActivities = JsonConvert.DeserializeObject<List<Activity>>(File.ReadAllText(ActivityManager.jsonPath));

        if (tempActivities.Count > 0) //prung add
        {
            //ตรวจสอบว่ามีเคยมีข้อมูลของ facebookId นี้อยู่บ้างไหมใน parse.com
            var query = ParseObject.GetQuery("Activity")
               .WhereEqualTo("facebookId", facebookId);

            query.CountAsync().ContinueWith(t =>
            {
                t.Wait();
                int count = t.Result;
                if (count > 0)
                {
                    Debug.Log("activity data exist");
                    SendOnlyNewDataToParse(tempActivities); //save เฉพาะข้อมูลที่ใหม่กว่า
                }
                else
                {
                    Debug.Log("activity data doesn't exist");
                    SendAllDataToParse(tempActivities); //save ทั้งหมด
                }
            });
        }
        else
        { //prung add
            Done = 1;
        }
    }

    private void SendAllDataToParse(List<Activity> tempActivities)
    {
        foreach (Activity activity in tempActivities)
        {

            ParseObject tmpParseObject = new ParseObject("Activity");
            tmpParseObject["facebookId"] = activity.facebookId;
            tmpParseObject["minute"] = activity.minute;
            tmpParseObject["burned"] = activity.burned;
            tmpParseObject["distance"] = activity.distance;
            tmpParseObject["weight"] = activity.weight;
            tmpParseObject["saveDate"] = activity.saveDate;
            tmpParseObject.SaveAsync();
            Debug.Log("Activity saved");
            Done = 1;
        }
    }

    public void SendOnlyNewDataToParse(List<Activity> tempActivities)
    {
        var query2 = ParseObject.GetQuery("Activity").WhereEqualTo("facebookId", facebookId);
        query2.OrderByDescending("saveDate").FirstAsync().ContinueWith(t2 =>
        {
            t2.Wait();
            ParseObject lastestActivityFromParse = t2.Result;

            string dateTimeStyle = "MM/dd/yyyy hh:mm:ss tt";

            DateTime lastestDateFromParse = lastestActivityFromParse.Get<DateTime>("saveDate");
            string stringDateParse = lastestDateFromParse.ToString(dateTimeStyle);
            foreach (Activity activity in tempActivities)
            {

                //send data to parse if local data latest
                if (DateTime.Compare(activity.saveDate, lastestDateFromParse) > 0)
                {
                    if (stringDateParse.Equals(activity.saveDate.ToString(dateTimeStyle)))
                    {
                        ParseObject tmpParseObject = new ParseObject("Activity");
                        // tmpParseObject["facebookId"] = activity.facebookId;
                        tmpParseObject["minute"] = activity.minute;
                        tmpParseObject["burned"] = activity.burned;
                        tmpParseObject["distance"] = activity.distance;
                        tmpParseObject["weight"] = activity.weight;
                        tmpParseObject["saveDate"] = activity.saveDate;
                        tmpParseObject.SaveAsync();
                        Debug.Log("Activity saved");
                        
                    }
                }
            }
            Done = 1;
        });
    }
}
