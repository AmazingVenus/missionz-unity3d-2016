﻿using UnityEngine;
using System;
using Parse;

public class SaveMeData : MonoBehaviour
{
    string FacebookName;
    int score;
    int scoreRecord;
    int healthFull;
    int energyFull;
    int level;
    int distance;
    int DistanceRecord;
    int burn;
    int burnRecord;
    int expCurrent;
    int expMax;
    string facebookId;
    public int Done;
    void Start() {
        Done = 0;
        facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY);
    }

    void Update() {
        
    }

    public void SaveToParse() {
        FacebookName = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_NAME_KEY, "");
        score = DataCenterModel.instance.playerMng.Score;
        scoreRecord = DataCenterModel.instance.playerMng.ScoreRecord;
        healthFull = (int)DataCenterModel.instance.playerMng.MaxHP;
        energyFull = MePlayerPrefs.DEFAULT_ENERGY_MAX_KEY;
        level = DataCenterModel.instance.playerMng.Level;
        distance = (int)DataCenterModel.instance.playerMng.CurrentDistance;
        DistanceRecord = (int)DataCenterModel.instance.playerMng.DistanceRecord;
        burn = (int)DataCenterModel.instance.playerMng.Burned;
        burnRecord = (int)DataCenterModel.instance.playerMng.BurnedRecord;
        expCurrent = DataCenterModel.instance.playerMng.CurrentExp;
        expMax = (int)DataCenterModel.instance.playerMng.MaxExp;
        Debug.Log("Me : initial finished");

        var query = ParseObject.GetQuery("Me")
       .WhereEqualTo("facebookId", facebookId);
        query.CountAsync().ContinueWith(t =>
        {
            t.Wait();
            int count = t.Result;
            if (count == 1)
            {
                Debug.Log("Me data exist");
                //ถ้ามีข้อมูลอยู่ ให้ update
                var query2 = ParseObject.GetQuery("Me").WhereEqualTo("facebookId", facebookId);
                query2.FirstAsync().ContinueWith(t2 =>
                {
                    t2.Wait();
                    ParseObject MeParse = t2.Result;
                    MeParse["facebookName"] = FacebookName;
                    MeParse["score"] = score;
                    MeParse["scoreRecord"] = scoreRecord;
                    MeParse["healthFull"] = healthFull;
                    MeParse["energyFull"] = energyFull;
                    MeParse["level"] = level;
                    MeParse["distance"] = distance;
                    MeParse["distanceRecord"] = DistanceRecord;
                    MeParse["burn"] = burn;
                    MeParse["burnRecord"] = burnRecord;
                    MeParse["expCurrent"] = expCurrent;
                    MeParse["expMax"] = expMax;

                    MeParse.SaveAsync();

                    Debug.Log("Me Saved");
                    Done = 1;
                });
            }
            else
            {
                Debug.Log("Me data doesn't exist");

                ParseObject MeParse = new ParseObject("Me");
                MeParse["facebookName"] = FacebookName;
                MeParse["score"] = score;
                MeParse["scoreRecord"] = scoreRecord;
                MeParse["healthFull"] = healthFull;
                MeParse["energyFull"] = energyFull;
                MeParse["level"] = level;
                MeParse["distance"] = distance;
                MeParse["distanceRecord"] = DistanceRecord;
                MeParse["burn"] = burn;
                MeParse["burnRecord"] = burnRecord;
                MeParse["expCurrent"] = expCurrent;
                MeParse["expMax"] = expMax;
                MeParse["facebookId"] = facebookId;
                MeParse.SaveAsync();
                Debug.Log("Me Saved");
                Done = 1;
            }
        });
    }
}

