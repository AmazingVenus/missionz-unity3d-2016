﻿using UnityEngine;
using System.Collections;
using Parse;

public class SaveBackpackData : MonoBehaviour {
    string facebookId;
    public int Done;
    // Use this for initialization
    void Start () {
        Done = 0;
        facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY);
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SaveToParse()
    {
        int amountHealthPotion = PlayerPrefs.GetInt(BackpackPlayerPrefs.HEALTH_POTION_KEY, 2);
        int amountEnergyPotion = PlayerPrefs.GetInt(BackpackPlayerPrefs.ENERGY_POTION_KEY, 2);
        int amountSleepDart = PlayerPrefs.GetInt(BackpackPlayerPrefs.SLEEP_DART_KEY, 15);
        int money = PlayerPrefs.GetInt(BackpackPlayerPrefs.MONEY_KEY, 200);

        //find data exist
        var query = ParseObject.GetQuery("Backpack")
          .WhereEqualTo("facebookId", facebookId);
        query.CountAsync().ContinueWith(t =>
        {
            t.Wait();
            int count = t.Result;
            if (count == 1)
            {
                Debug.Log("Backpack data exist");
                //ถ้ามีข้อมูลอยู่ ให้ update
                var query2 = ParseObject.GetQuery("Backpack").WhereEqualTo("facebookId", facebookId);
                query2.FirstAsync().ContinueWith(t2 =>
                {
                    t2.Wait();
                    ParseObject backpackParse = t2.Result;
                    backpackParse["healthPotion"] = amountHealthPotion;
                    backpackParse["energyPotion"] = amountEnergyPotion;
                    backpackParse["sleepDart"] = amountSleepDart;
                    backpackParse["money"] = money;
                    backpackParse.SaveAsync();
                    Done = 1;
                });
            }
            else
            {
                Debug.Log("Backpack data doesn't exist");
                ParseObject backpackObject = new ParseObject("Backpack");
                backpackObject["facebookId"] = facebookId;
                backpackObject["healthPotion"] = amountHealthPotion;
                backpackObject["energyPotion"] = amountEnergyPotion;
                backpackObject["sleepDart"] = amountSleepDart;
                backpackObject["money"] = money;
                backpackObject.SaveAsync();
                Done = 1;
            }
        });


    }
}
