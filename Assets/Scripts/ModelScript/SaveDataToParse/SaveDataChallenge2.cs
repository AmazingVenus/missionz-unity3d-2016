﻿using UnityEngine;
using Parse;
using System.Collections.Generic;
using System.IO;
using System;

public class SaveDataChallenge2 : MonoBehaviour
{

    string facebookId;
    public int Done;
    public string jsonPath;

    public int currentDistance;
    public int currentBurned;
    public int currentMoney;
    public int currentHunter;

    public int targetDistance;
    public int targetBurned;
    public int targetMoney;
    public int targetHunter;

    public int levelDistance;
    public int levelBurned;
    public int levelMoney;
    public int levelHunter;

    // Use this for initialization
    void Start()
    {
        Done = 0;
        facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY);
        InitialPlayerPrefs();
        //SaveToParse();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void InitialPlayerPrefs()
    {
        
        currentBurned = PlayerPrefs.GetInt(ChallengePlayerPrefs.CURRENT_BURNED_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_CURRENT_CHALLENGE_KEY);
        currentMoney = PlayerPrefs.GetInt(ChallengePlayerPrefs.CURRENT_MONEY_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_CURRENT_CHALLENGE_KEY);
        currentHunter = PlayerPrefs.GetInt(ChallengePlayerPrefs.CURRENT_HUNTER_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_CURRENT_CHALLENGE_KEY);

        
        targetBurned = PlayerPrefs.GetInt(ChallengePlayerPrefs.TARGET_BURNED_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_TARGET_BURNED_CHALLENGE_KEY);
        targetMoney = PlayerPrefs.GetInt(ChallengePlayerPrefs.TARGET_MONEY_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_TARGET_MONEY_CHALLENGE_KEY);
        targetHunter = PlayerPrefs.GetInt(ChallengePlayerPrefs.TARGET_HUNTER_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_TARGET_HUNTER_CHALLENGE_KEY);

        
        levelBurned = PlayerPrefs.GetInt(ChallengePlayerPrefs.LEVEL_BURNED_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_LEVEL_CHALLENGE_KEY);
        levelMoney = PlayerPrefs.GetInt(ChallengePlayerPrefs.LEVEL_MONEY_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_LEVEL_CHALLENGE_KEY);
        levelHunter = PlayerPrefs.GetInt(ChallengePlayerPrefs.LEVEL_HUNTER_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_LEVEL_CHALLENGE_KEY);
    }

    public void SaveToParse()
    {


        //ตรวจสอบว่ามีเคยมีข้อมูลของ facebookId นี้อยู่บ้างไหมใน parse.com
        var query = ParseObject.GetQuery("Challenge")
           .WhereEqualTo("facebookId", facebookId);

        query.CountAsync().ContinueWith(t =>
        {
            t.Wait();
            int count = t.Result;
            if (count == 1)
            {
                Debug.Log("challenge data exist");
                UpdateDataParse();



                Done = 1;
            }
            else
            {
                Debug.Log("challenge data doesn't exist");
                SaveNewDataParse();
            }
        });
    }

    private void SaveNewDataParse()
    {

        ParseObject tmpParseObject = new ParseObject("Challenge");
        tmpParseObject["facebookId"] = facebookId;
        tmpParseObject["currentDistance"] = currentDistance;
        tmpParseObject["currentBurned"] = currentBurned;
        tmpParseObject["currentMoney"] = currentMoney;
        tmpParseObject["currentHunter"] = currentHunter;

        tmpParseObject["targetDistance"] = targetDistance;
        tmpParseObject["targetBurned"] = targetBurned;
        tmpParseObject["targetMoney"] = targetMoney;
        tmpParseObject["targetHunter"] = targetHunter;

        tmpParseObject["levelDistance"] = targetDistance;
        tmpParseObject["levelBurned"] = levelBurned;
        tmpParseObject["levelMoney"] = levelMoney;
        tmpParseObject["levelHunter"] = levelHunter;


        tmpParseObject.SaveAsync();
        Debug.Log("Challenge Saved");
        Done = 1;


    }

    public void UpdateDataParse()
    {
        var query2 = ParseObject.GetQuery("Challenge").WhereEqualTo("facebookId", facebookId);
        query2.FirstAsync().ContinueWith(t2 =>
        {//ต้องแก้
            t2.Wait();
            ParseObject tmpParseObject = t2.Result;
            tmpParseObject["facebookId"] = facebookId;
            tmpParseObject["currentDistance"] = currentDistance;
            tmpParseObject["currentBurned"] = currentBurned;
            tmpParseObject["currentMoney"] = currentMoney;
            tmpParseObject["currentHunter"] = currentHunter;

            tmpParseObject["targetDistance"] = targetDistance;
            tmpParseObject["targetBurned"] = targetBurned;
            tmpParseObject["targetMoney"] = targetMoney;
            tmpParseObject["targetHunter"] = targetHunter;

            tmpParseObject["levelDistance"] = targetDistance;
            tmpParseObject["levelBurned"] = levelBurned;
            tmpParseObject["levelMoney"] = levelMoney;
            tmpParseObject["levelHunter"] = levelHunter;


            tmpParseObject.SaveAsync();
            Debug.Log("Challenge Saved99");



            Done = 1;
        });
    }


}