﻿using UnityEngine;
using Parse;

public class SaveRankData : MonoBehaviour {
    string facebookId;
    string facebookName;
    int score;
    public int Done;
    // Use this for initialization
    void Start()
    {
        Done = 0;
        facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY);
        facebookName = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_NAME_KEY);

    }

    // Update is called once per frame
    void Update()
    {
        score = DataCenterModel.instance.playerMng.Score;
    }

    public void SaveToParse()
    {

        //find data exist
        var query = ParseObject.GetQuery("Rank")
          .WhereEqualTo("facebookId", facebookId);
        query.CountAsync().ContinueWith(t =>
        {
            t.Wait();
            int count = t.Result;
            if (count == 1)
            {
                Debug.Log("Rank data exist");
                //ถ้ามีข้อมูลอยู่ ให้ update
                var query2 = ParseObject.GetQuery("Rank").WhereEqualTo("facebookId", facebookId);
                query2.FirstAsync().ContinueWith(t2 =>
                {
                    t2.Wait();
                    ParseObject RankParse = t2.Result;
                    RankParse["score"] = score;
                    RankParse.SaveAsync();
                    Done = 1;
                });
            }
            else
            {
                Debug.Log("Rank data doesn't exist");
                ParseObject RankObject = new ParseObject("Rank");
                RankObject["facebookId"] = facebookId;
                RankObject["facebookName"] = facebookName;
                RankObject["score"] = score;
                RankObject.SaveAsync();
                Done = 1;
            }
        });


    }
}
