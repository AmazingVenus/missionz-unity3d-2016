﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Parse;
using System;
using System.IO;
using Newtonsoft.Json;

public class LoadChallengData : MonoBehaviour {
    public string facebookId;
    public bool isChallengeCompleteFinished;
    public List<Challenge> challengeFromParse;
    public bool isDone;
    // Use this for initialization
    void Start () {
        facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY, "1234567890");
        challengeFromParse = new List<Challenge>();
        LoadingData();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void LoadingData()
    {
        var query = ParseObject.GetQuery("ChallengeComplete")
         .WhereEqualTo("facebookId", facebookId);

        query.CountAsync().ContinueWith(t =>
        {
            t.Wait();
            int count = t.Result;
            if (count > 0)
            {
                Debug.Log("challenge c data exist");


                var query2 = ParseObject.GetQuery("ChallengeComplete")
    .WhereEqualTo("facebookId", facebookId);
                query2.FindAsync().ContinueWith(t2 =>
                {
                    t2.Wait();
                    IEnumerable<ParseObject> results = t2.Result;
                    foreach (ParseObject OKResults in results)
                    {
                        Challenge c = new Challenge(facebookId,
                           OKResults.Get<string>("challengeName"),
                           OKResults.Get<int>("challengeLevel"),
                           OKResults.Get<int>("target"),
                           OKResults.Get<int>("scoreReward"),
                           OKResults.Get<DateTime>("saveDate"));

                        challengeFromParse.Add(c);
                    }

                    isChallengeCompleteFinished = true;
                    isDone = true;
                    Debug.Log("Challenge Loaded");

                });

            }
            else
            {
                Debug.Log("challenge c data doesn't exist");
                isChallengeCompleteFinished = true;
                isDone = true;

            }
        });
    }
}
