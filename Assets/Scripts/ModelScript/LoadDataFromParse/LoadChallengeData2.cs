﻿using UnityEngine;
using Parse;

public class LoadChallengeData2 : MonoBehaviour
{

    public string facebookId;
    public bool isChallenge2Finished;
    public bool isDone;

   // public int currentDistance;
    public int currentBurned;
    public int currentMoney;
    public int currentHunter;

  //  public int targetDistance;
    public int targetBurned;
    public int targetMoney;
    public int targetHunter;

   // public int levelDistance;
    public int levelBurned;
    public int levelMoney;
    public int levelHunter;

    void Start()
    {
        facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY,"1234567890");

        InitialVariable();

        LoadingData();
    }


    void Update()
    {

    }

    public void InitialVariable()
    {
     //   currentDistance = 0;
        currentBurned = 0;
        currentMoney = 0;
        currentHunter = 0;

     //   targetDistance = ChallengePlayerPrefs.DEFAULT_TARGET_DISTANCE_CHALLENGE_KEY;
        targetBurned = ChallengePlayerPrefs.DEFAULT_TARGET_BURNED_CHALLENGE_KEY;
        targetMoney = ChallengePlayerPrefs.DEFAULT_TARGET_MONEY_CHALLENGE_KEY;
        targetHunter = ChallengePlayerPrefs.DEFAULT_TARGET_HUNTER_CHALLENGE_KEY;

     //   levelDistance = 1;
        levelBurned = 1;
        levelMoney = 1;
        levelHunter = 1;
    }

    public void LoadingData()
    {
        var query = ParseObject.GetQuery("Challenge")
         .WhereEqualTo("facebookId", facebookId);

        query.CountAsync().ContinueWith(t =>
        {
            t.Wait();
            int count = t.Result;
            if (count == 1)
            {
                Debug.Log("challenge data exist");


                var query2 = ParseObject.GetQuery("Challenge")
    .WhereEqualTo("facebookId", facebookId);
                query2.FirstAsync().ContinueWith(t2 =>
                {
                    t2.Wait();
                    ParseObject results = t2.Result;

                //    currentDistance = results.Get<int>("currentDistance");
                    currentBurned = results.Get<int>("currentBurned");
                    currentMoney = results.Get<int>("currentMoney");
                    currentHunter = results.Get<int>("currentHunter");

                 //   targetDistance = results.Get<int>("targetDistance");
                    targetBurned = results.Get<int>("targetBurned");
                    targetMoney = results.Get<int>("targetMoney");
                    targetHunter = results.Get<int>("targetHunter");

                //    levelDistance = results.Get<int>("levelDistance");
                    levelBurned = results.Get<int>("levelBurned");
                    levelMoney = results.Get<int>("levelMoney");
                    levelHunter = results.Get<int>("levelHunter");



                    isChallenge2Finished = true;
                    isDone = true;
                    Debug.Log("Challenge 2 Loaded");

                });

            }
            else
            {
                Debug.Log("challenge 2 data doesn't exist");
                isChallenge2Finished = true;
                isDone = true;

            }
        });
    }
}
