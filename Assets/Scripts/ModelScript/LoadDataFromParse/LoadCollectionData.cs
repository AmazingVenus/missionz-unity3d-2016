﻿using UnityEngine;
using System.Collections;
using Parse;

public class LoadCollectionData : MonoBehaviour {
    public string facebookId;
    public int lion = 0, elephant = 0, monkey = 0, raccoon = 0, bear = 0, rabbit = 0;
    public bool isCollectionFinished;
    public bool isDone;
    // Use this for initialization
    void Start () {
        facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY, "1234567890");
        LoadingData();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    public void LoadingData()
    {
        var query = ParseObject.GetQuery("Collection")
          .WhereEqualTo("facebookId", facebookId);

        query.CountAsync().ContinueWith(t =>
        {
            t.Wait();
            int count = t.Result;
            if (count > 0)
            {
                Debug.Log("collection data exist");

                var query2 = ParseObject.GetQuery("Collection")
    .WhereEqualTo("facebookId", facebookId);
                query2.FirstAsync().ContinueWith(t2 =>
                {
                    t2.Wait();
                    ParseObject results = t2.Result;
                    lion = results.Get<int>("lion");
                    elephant = results.Get<int>("elephant");
                    monkey = results.Get<int>("monkey");
                    raccoon = results.Get<int>("raccoon");
                    bear = results.Get<int>("bear");
                    rabbit = results.Get<int>("rabbit");

                    Debug.Log("Collection Loaded");

                    isCollectionFinished = true;
                    isDone = true;
                });

            }
            else
            {
                Debug.Log("collection  data doesn't exist");

                isCollectionFinished = true;
                isDone = true;
            }
        });
    }
}
