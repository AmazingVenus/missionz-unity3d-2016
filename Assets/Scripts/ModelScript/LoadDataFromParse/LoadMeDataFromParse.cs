﻿using UnityEngine;
using System.Collections;
using Parse;

public class LoadMeDataFromParse : MonoBehaviour {
    public string facebookId;
    public bool isMeFinished;
    public int MeBurn = 0, MeBurnRecord = 0, MeDistance = 0, MeDISTANCE_RECORD_KEY = 0, energyFull = 0, expCurrent = 0,
        expMax = 11, healthFull = 107, MeLevel = 1, MeScore = 0, MeSCORE_RECORD_KEY = 0, money = 300, currentHP = 107;
    public string FACEBOOK_NAME_KEY;
    public bool isDone;
    // Use this for initialization
    void Start () {
        facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY, "1234567890");
        LoadingData();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    private void LoadingData()
    {
        var query = ParseObject.GetQuery("Me")
       .WhereEqualTo("facebookId", facebookId);
        query.CountAsync().ContinueWith(t =>
        {
            isMeFinished = false;
            t.Wait();
            int count = t.Result;
            if (count == 1)
            {
                Debug.Log("Me data exist");
                //ถ้ามีข้อมูลอยู่ ให้ update
                var query2 = ParseObject.GetQuery("Me").WhereEqualTo("facebookId", facebookId);
                query2.FirstAsync().ContinueWith(t2 =>
                {
                    t2.Wait();
                    ParseObject MeParse = t2.Result;

                    MeBurn = MeParse.Get<int>("burn");
                    MeBurnRecord = MeParse.Get<int>("burnRecord");
                    MeDistance = MeParse.Get<int>("distance");
                    MeDISTANCE_RECORD_KEY = MeParse.Get<int>("distanceRecord");
                    energyFull = 100;
                    expCurrent = MeParse.Get<int>("expCurrent");
                    expMax = MeParse.Get<int>("expMax");
                    healthFull = MeParse.Get<int>("healthFull");
                    MeLevel = MeParse.Get<int>("level");
                    MeScore = MeParse.Get<int>("score");
                    MeSCORE_RECORD_KEY = MeParse.Get<int>("scoreRecord");
                    currentHP = healthFull;
                    isMeFinished = true;
                    Debug.Log("Me Loaded");
                    isDone = true;

                });
            }
            else
            {
                Debug.Log("Me data doesn't exist");
                isMeFinished = true;
                isDone = true;
            }
            

        });

    }
}
