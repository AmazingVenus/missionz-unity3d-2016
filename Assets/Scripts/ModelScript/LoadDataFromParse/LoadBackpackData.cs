﻿using UnityEngine;
using System.Collections;
using Parse;

public class LoadBackpackData : MonoBehaviour {
    public string facebookId;
    public bool isBackpackFinished;
    public int sleepingDart = 15, healthPotion = 2, energyPotion = 2;
    public int money = 300;
    public bool isDone;
    // Use this for initialization
    void Start () {
        facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY, "1234567890");
        LoadingData();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    private void LoadingData()
    {
        var query = ParseObject.GetQuery("Backpack")
           .WhereEqualTo("facebookId", facebookId);

        query.CountAsync().ContinueWith(t =>
        {
            t.Wait();
            int count = t.Result;
            if (count > 0)
            {
                Debug.Log("backpack data exist");

                var query2 = ParseObject.GetQuery("Backpack")
    .WhereEqualTo("facebookId", facebookId);
                query2.FirstAsync().ContinueWith(t2 =>
                {
                    t2.Wait();
                    ParseObject results = t2.Result;
                    sleepingDart = results.Get<int>("sleepDart");
                    healthPotion = results.Get<int>("healthPotion");
                    energyPotion = results.Get<int>("energyPotion");
                    money = results.Get<int>("money");

                    isBackpackFinished = true;
                    isDone = true;
                });
            }
            else
            {
                Debug.Log("backpack  data doesn't exist");
                isBackpackFinished = true;
                isDone = true;
            }


        });


    }
}
