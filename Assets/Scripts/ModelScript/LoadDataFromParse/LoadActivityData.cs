﻿using UnityEngine;
using System.Collections;
using Parse;
using System.Collections.Generic;
using System;
using Newtonsoft.Json;
using System.IO;

public class LoadActivityData : MonoBehaviour {
    public string facebookId;
    public bool isActivityFinished;
    public List<Activity> activityFromParse;
    public bool isDone;
    // Use this for initialization
    void Start () {
        facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY, "1234567890");
        activityFromParse = new List<Activity>();
        LoadingData();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void LoadingData()
    {
        var query = ParseObject.GetQuery("Activity")
         .WhereEqualTo("facebookId", facebookId);

        query.CountAsync().ContinueWith(t =>
        {
            t.Wait();
            int count = t.Result;
            if (count > 0)
            {
                Debug.Log("activity c data exist");

                var query2 = ParseObject.GetQuery("Activity")
    .WhereEqualTo("facebookId", facebookId);
                query2.FindAsync().ContinueWith(t2 =>
                {
                    Debug.Log("Activity Loading1");
                    t2.Wait();
                    Debug.Log("Activity Loading2");
                    IEnumerable<ParseObject> results = t2.Result;
                    Debug.Log("Activity Loading3");
                    foreach (ParseObject OKResults in results)
                    {
                        Debug.Log("Activity Loading4");
                        Activity aa = new Activity(facebookId,
                             OKResults.Get<int>("minute"),
                             OKResults.Get<int>("distance"),
                             OKResults.Get<int>("weight"),
                             OKResults.Get<int>("burned"),
                             OKResults.Get<DateTime>("saveDate"));
                        Debug.Log("Activity Loading5");
                        activityFromParse.Add(aa);
                        Debug.Log("Activity Loading6");
                    }

                    isActivityFinished = true;
                    isDone = true;
                    Debug.Log("Activity Loaded");
                });
            }
            else
            {
                Debug.Log("activity data doesn't exist");

                isActivityFinished = true;
                isDone = true;
            }
            

        });

        
    }
}
