﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SceneManagerModel : MonoBehaviour {
	public static SceneManagerModel instance;
	public GameObject initScene;
	public GameObject currentScene;
	public GameObject previousScene;
	public GameObject panelBar;
    public GameObject backButton;
	public int count;
	public List<GameObject> scenes = new List<GameObject>();

	public List<GameObject> AllScenes = new List<GameObject>();
    int isLoggedIn;
    // Use this for initialization
    void Awake() {
		instance = this;
        isLoggedIn = PlayerPrefs.GetInt(MissionZPlayerPref.GAME_LOGIN_STATUS_KEY, 0);
    }
    void Start () {
        
        if (isLoggedIn == 0)
        {
            initScene = AllScenes[1];
            PlayerPrefs.SetInt(MissionZPlayerPref.GAME_LOGIN_STATUS_KEY, 1);
            //PlayerPrefs.SetInt(ChallengePlayerPrefs.LEVEL_DISTANCE_CHALLENGE_KEY, 1);
            //PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_BURNED_CHALLENGE_KEY, 70);
            //PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_DISTANCE_CHALLENGE_KEY, 70);
            
            //PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_MONEY_CHALLENGE_KEY, 700);
            //PlayerPrefs.SetInt(BackpackPlayerPrefs.MONEY_KEY, 700);
            //PlayerPrefs.SetInt(BackpackPlayerPrefs.SLEEP_DART_KEY, 300);
            //PlayerPrefs.SetInt(BackpackPlayerPrefs.ENERGY_POTION_KEY, 50);
            //PlayerPrefs.SetInt(BackpackPlayerPrefs.HEALTH_POTION_KEY, 50);


            PlayerPrefs.Save();
        }
        else
        {
            initScene = AllScenes[2];
        }
        scenes.Add (initScene);
		initScene.SetActive (true);
	}
	
	// Update is called once per frame
	void Update () {
		count = scenes.Count;
		currentScene = scenes[scenes.Count-1];

		if (count >= 2) {
			previousScene = scenes [scenes.Count - 2];
			//panelBar.SetActive(true);
		} else {
			//panelBar.SetActive(false);
		}

        
	}

	public void clearScene(){
		scenes.Clear ();
	}
}
