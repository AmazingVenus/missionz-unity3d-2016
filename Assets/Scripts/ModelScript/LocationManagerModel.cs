﻿using UnityEngine;
using System.Collections;

public class LocationManagerModel : MonoBehaviour {

	public float lat;
	public float lng;
	// Use this for initialization
	IEnumerator Start () {

		Input.location.Start ();

		int maxWait = 20;

		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
		{
			yield return new WaitForSeconds(1);
			maxWait--;
		}
		// Service didn't initialize in 20 seconds
		if (maxWait < 1)
		{
			print("Timed out");
			yield break;
		}
		
		// Connection has failed
		if (Input.location.status == LocationServiceStatus.Failed)
		{
			print("Unable to determine device location");
			yield break;
		}
		else
		{
			// Access granted and location value could be retrieved
			print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
		}
		
		// Stop service if there is no need to query location updates continuously
		Input.location.Stop();
	}
	
	// Update is called once per frame
	void Update () {
		lat = Input.location.lastData.latitude;
		lng = Input.location.lastData.longitude;
	}
}
