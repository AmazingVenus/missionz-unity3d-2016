﻿using UnityEngine;
using Parse;
using System.Collections.Generic;

public class BackpackManager{
    public static string facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY);

    //money ก็ใช้ได้นะ
    public static void DecreaseAmountItem(string itemKey, int amount) {
        PlayerPrefs.SetInt(itemKey, PlayerPrefs.GetInt(itemKey)-amount);
        PlayerPrefs.Save();
    }

    //money ก็ใช้ได้นะ
    public static void IncreaseAmountItem(string itemKey, int amount) {
        PlayerPrefs.SetInt(itemKey, PlayerPrefs.GetInt(itemKey) + amount);
        PlayerPrefs.Save();
    }
}
