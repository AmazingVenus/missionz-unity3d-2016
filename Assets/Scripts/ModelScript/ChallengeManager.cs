﻿using UnityEngine;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using Parse;
using System;

public static class ChallengeManager
{
    //ขาด save Challenge to parse.com
    public static string facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY);

    public static string jsonPath = Application.persistentDataPath + "/complete_challenge.json";
    public static string ChallengeDistanceName = "Walk Man";
    public static string ChallengeBurnName = "Burn Calories";
    public static string ChallengeMoneyName = "Rich Man";
    public static string ChallengeHunterName = "Hunter";

    public static void IncreaseCurrentHunterChallenge(int amountAnimal) {
        if (PlayerPrefs.GetInt(ChallengePlayerPrefs.CHALLENGE_ACTIVE_KEY, ChallengePlayerPrefs.DEFAULT_CHALLENGE_ACTIVE_KEY) == 2)
        {
            int tempAmountAnimal = PlayerPrefs.GetInt(ChallengePlayerPrefs.CURRENT_HUNTER_CHALLENGE_KEY, 0);
            PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_HUNTER_CHALLENGE_KEY, tempAmountAnimal + amountAnimal);
            PlayerPrefs.Save();
        }
    }

    public static void IncreaseCurrentBurnedChallenge(int burned) {
        if (PlayerPrefs.GetInt(ChallengePlayerPrefs.CHALLENGE_ACTIVE_KEY, ChallengePlayerPrefs.DEFAULT_CHALLENGE_ACTIVE_KEY)==0)
        {
            int tempOldBurned = PlayerPrefs.GetInt(ChallengePlayerPrefs.CURRENT_BURNED_CHALLENGE_KEY, 0);
            PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_BURNED_CHALLENGE_KEY, tempOldBurned + burned);
            PlayerPrefs.Save();
        }
    }
    

    public static void IncreaseCurrentMoneyChallenge(int money)
    {
        if (PlayerPrefs.GetInt(ChallengePlayerPrefs.CHALLENGE_ACTIVE_KEY, ChallengePlayerPrefs.DEFAULT_CHALLENGE_ACTIVE_KEY) == 1)
        {
            int tempOldMoney = PlayerPrefs.GetInt(ChallengePlayerPrefs.CURRENT_MONEY_CHALLENGE_KEY, 0);
            PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_MONEY_CHALLENGE_KEY, tempOldMoney + money);
            PlayerPrefs.Save();
        }
    }

    public static void ResetAllChallengePlayerPrefs()
    {
        PlayerPrefs.SetInt(ChallengePlayerPrefs.TARGET_BURNED_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_TARGET_BURNED_CHALLENGE_KEY);
        PlayerPrefs.SetInt(ChallengePlayerPrefs.TARGET_MONEY_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_TARGET_MONEY_CHALLENGE_KEY);
        PlayerPrefs.SetInt(ChallengePlayerPrefs.TARGET_HUNTER_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_TARGET_HUNTER_CHALLENGE_KEY);

        PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_BURNED_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_CURRENT_CHALLENGE_KEY);
        PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_MONEY_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_CURRENT_CHALLENGE_KEY);
        PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_HUNTER_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_CURRENT_CHALLENGE_KEY);
        
        PlayerPrefs.SetInt(ChallengePlayerPrefs.LEVEL_BURNED_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_LEVEL_CHALLENGE_KEY);
        PlayerPrefs.SetInt(ChallengePlayerPrefs.LEVEL_MONEY_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_LEVEL_CHALLENGE_KEY);
        PlayerPrefs.SetInt(ChallengePlayerPrefs.LEVEL_HUNTER_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_LEVEL_CHALLENGE_KEY);
        PlayerPrefs.Save();
    }

    public static void ResetCurrentChallengePlayerPrefs(string currentChallengeKey)
    {
        PlayerPrefs.SetInt(currentChallengeKey, 0);
        PlayerPrefs.Save();
    }

   



    public static int CalcAverageBurned()
    {
        int sumBurned = PlayerPrefs.GetInt(ActivityPlayerPrefs.SUM_BURNED_KEY, ActivityPlayerPrefs.DEFAULT_SUM_BURNED_KEY);
        int countDay = PlayerPrefs.GetInt(ActivityPlayerPrefs.COUNT_DAY_KEY,1);
        if (sumBurned == 0)
        {
            return (int)(DataCenterModel.instance.playerMng.BurnedRecord / countDay);
        }
        else
        {
            return (int)(sumBurned / countDay);
        }
    }

    public static void saveCompleteChallengeToJson(Challenge completeChallenge)
    {
        List<Challenge> tempChallenge = new List<Challenge>();

        tempChallenge = JsonConvert.DeserializeObject<List<Challenge>>(File.ReadAllText(ChallengeManager.jsonPath));

        tempChallenge.Add(completeChallenge);

        string jsonOutput = JsonConvert.SerializeObject(tempChallenge, Formatting.Indented);
        File.WriteAllText(jsonPath, jsonOutput);

    }


    public static void ResetJsonFile()
    {
        string initialJson = "[]";
        File.WriteAllText(jsonPath, initialJson);
    }

    public static int CalcScoreChallenge(int i, int target)
    {
        
        if (i == 0)
        {
            return 15 * target;
        }
        else if (i == 1)
        {
            return 8 * target;
        }
        else if (i == 2)
        {
            return 20 * target;
        }
        else
        {
            Debug.Log("Number of Challenge is invalid");
            return 0;
        }
    }

    public static void SaveToParse()
    {
        List<Challenge> tempChallenge = new List<Challenge>();
        tempChallenge = JsonConvert.DeserializeObject<List<Challenge>>(File.ReadAllText(jsonPath));

        //ตรวจสอบว่ามีเคยมีข้อมูลของ facebookId นี้อยู่บ้างไหมใน parse.com
        var query = ParseObject.GetQuery("ChallengeComplete")
           .WhereEqualTo("facebookId", facebookId);

        query.CountAsync().ContinueWith(t =>
        {
            int count = t.Result;
            if (count > 0)
            {
                Debug.Log("challenge c data exist");
                SendOnlyNewDataToParse(tempChallenge);
            }
            else
            {
                Debug.Log("challenge c data doesn't exist");
                SendAllDataToParse(tempChallenge);
            }
        });
    }

    private static void SendAllDataToParse(List<Challenge> tempChallenge)
    {
        foreach (Challenge challenge in tempChallenge)
        {
            ParseObject tmpParseObject = new ParseObject("ChallengeComplete");
            tmpParseObject["facebookId"] = challenge.facebookId;
            tmpParseObject["challengeName"] = challenge.challengeName;
            tmpParseObject["challengeLevel"] = challenge.challengeLevel;
            tmpParseObject["target"] = challenge.challengeTarget;
            tmpParseObject["scoreReward"] = challenge.scoreReward;
            tmpParseObject["saveDate"] = challenge.saveDate;
            tmpParseObject.SaveAsync();
            Debug.Log("Challenge Complete Saved");

        }
    }

    public static void SendOnlyNewDataToParse(List<Challenge> tempChallenge)
    {
        var query2 = ParseObject.GetQuery("ChallengeComplete").WhereEqualTo("facebookId", facebookId);
        query2.OrderByDescending("saveDate").FirstAsync().ContinueWith(t2 =>
        {

            ParseObject lastestChallengeFromParse = t2.Result;
            string dateTimeStyle = "MM/dd/yyyy hh:mm:ss tt";

            DateTime lastestDateFromParse = lastestChallengeFromParse.Get<DateTime>("saveDate");
            string lastDateParseNewStyle = lastestDateFromParse.ToString(dateTimeStyle);

            foreach (Challenge challenge in tempChallenge)
            {

                //send data to parse if local data latest
                if (DateTime.Compare(challenge.saveDate, lastestDateFromParse) > 0)
                {
                    string saveDateTemp = challenge.saveDate.ToString(dateTimeStyle);
                    if (!lastDateParseNewStyle.Equals(saveDateTemp))
                    {
                        ParseObject tmpParseObject = new ParseObject("ChallengeComplete");
                        tmpParseObject["facebookId"] = challenge.facebookId;
                        tmpParseObject["challengeName"] = challenge.challengeName;
                        tmpParseObject["challengeLevel"] = challenge.challengeLevel;
                        tmpParseObject["target"] = challenge.challengeTarget;
                        tmpParseObject["scoreReward"] = challenge.scoreReward;
                        tmpParseObject["saveDate"] = challenge.saveDate;
                        tmpParseObject.SaveAsync();
                        Debug.Log("Challenge Complete Saved");
                    }
                    else
                    {
                        Debug.Log("same date");
                    }

                }
            }

        });
    }
}
