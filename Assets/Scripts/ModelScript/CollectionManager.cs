﻿using System;
using Parse;
using UnityEngine;
using System.Collections.Generic;

public class CollectionManager
{
    public static string facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY);

    public static void IncreaseAnimalToCollection(string collectionKey, int amount)
    {
        PlayerPrefs.SetInt(collectionKey, PlayerPrefs.GetInt(collectionKey, 0) + amount);
        PlayerPrefs.Save();
    }

    

    public static void ResetCollection()
    {
        PlayerPrefs.SetInt(CollectionPlayerPrefs.BEAR_KEY, 0);
        PlayerPrefs.SetInt(CollectionPlayerPrefs.ELEPHANT_KEY, 0);
        PlayerPrefs.SetInt(CollectionPlayerPrefs.LION_KEY, 0);
        PlayerPrefs.SetInt(CollectionPlayerPrefs.MONKEY_KEY, 0);
        PlayerPrefs.SetInt(CollectionPlayerPrefs.RABBIT_KEY, 0);
        PlayerPrefs.SetInt(CollectionPlayerPrefs.RACCOON_KEY, 0);
        PlayerPrefs.Save();
    }

    public static void SaveToParse()
    {
        //initial playerprefs prevent error

        int bear = PlayerPrefs.GetInt(CollectionPlayerPrefs.BEAR_KEY, 0);
        int lion = PlayerPrefs.GetInt(CollectionPlayerPrefs.LION_KEY, 0);
        int monkey = PlayerPrefs.GetInt(CollectionPlayerPrefs.MONKEY_KEY, 0);
        int elephant = PlayerPrefs.GetInt(CollectionPlayerPrefs.ELEPHANT_KEY, 0);
        int rabbit = PlayerPrefs.GetInt(CollectionPlayerPrefs.RABBIT_KEY, 0);
        int raccoon = PlayerPrefs.GetInt(CollectionPlayerPrefs.RACCOON_KEY, 0);

        //find data exist
        var query = ParseObject.GetQuery("Collection")
          .WhereEqualTo("facebookId", facebookId);
        query.CountAsync().ContinueWith(t =>
        {
            int count = t.Result;
            if (count == 1)
            {
                Debug.Log("Collection data exist");
                //ถ้ามีข้อมูลอยู่ ให้ update
                var query2 = ParseObject.GetQuery("Collection").WhereEqualTo("facebookId", facebookId);
                query2.FirstAsync().ContinueWith(t2 =>
                {
                    ParseObject collectionFromParse = t2.Result;
                    collectionFromParse["bear"] = bear;
                    collectionFromParse["lion"] = lion;
                    collectionFromParse["monkey"] = monkey;
                    collectionFromParse["elephant"] = elephant;
                    collectionFromParse["rabbit"] = rabbit;
                    collectionFromParse["raccoon"] = raccoon;
                    collectionFromParse.SaveAsync();
                    Debug.Log("Collection Saved");
                });
            }
            else
            {
                Debug.Log("Collection data doesn't exist");

                ParseObject tmpToParse = new ParseObject("Collection");
                tmpToParse["facebookId"] = facebookId;
                tmpToParse["bear"] = bear;
                tmpToParse["lion"] = lion;
                tmpToParse["monkey"] = monkey;
                tmpToParse["elephant"] = elephant;
                tmpToParse["rabbit"] = rabbit;
                tmpToParse["raccoon"] = raccoon;
                tmpToParse.SaveAsync();
                Debug.Log("Collection Saved");
                
            }
        });

        
    }
    
   
}
