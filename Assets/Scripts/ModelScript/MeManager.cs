﻿using UnityEngine;
using System.Collections;
using Parse;

public class MeManager
{
    private static string facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY);

    public static void SaveToParse()
    {
  
        string FACEBOOK_NAME_KEY= PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_NAME_KEY, "");
        int score = PlayerPrefs.GetInt(MePlayerPrefs.SCORE_KEY, 0);
        int SCORE_RECORD_KEY = PlayerPrefs.GetInt(MePlayerPrefs.SCORE_RECORD_KEY, 0);
        int healthFull = PlayerPrefs.GetInt(MePlayerPrefs.HEALTH_MAX_KEY, 107);
        int energyFull = PlayerPrefs.GetInt(MePlayerPrefs.ENERGY_MAX_KEY, 100);
        int level = PlayerPrefs.GetInt(MePlayerPrefs.LEVEL_KEY, 0);
        int distance = PlayerPrefs.GetInt(MePlayerPrefs.DISTANCE_TRAVELLED_KEY, 0);
        int DISTANCE_RECORD_KEY = PlayerPrefs.GetInt(MePlayerPrefs.DISTANCE_RECORD_KEY, 0);
        int burn = PlayerPrefs.GetInt(MePlayerPrefs.BURNED_KEY, 0);
        int burnRecord = PlayerPrefs.GetInt(MePlayerPrefs.BURNED_RECORD_KEY, 0);
        int expCurrent = PlayerPrefs.GetInt(MePlayerPrefs.EXP_CURRENT_KEY, 0);
        int expMax = PlayerPrefs.GetInt(MePlayerPrefs.EXP_MAX_KEY, 11);
        Debug.Log("Me : initial finished");

        var query = ParseObject.GetQuery("Me")
       .WhereEqualTo("facebookId", facebookId);
        query.CountAsync().ContinueWith(t =>
        {
            t.Wait();
            int count = t.Result;
            if (count == 1)
            {
                Debug.Log("Me data exist");
                //ถ้ามีข้อมูลอยู่ ให้ update
                var query2 = ParseObject.GetQuery("Me").WhereEqualTo("facebookId", facebookId);
                query2.FirstAsync().ContinueWith(t2 =>
                {
                    t2.Wait();
                    ParseObject MeParse = t2.Result;
                    MeParse["FACEBOOK_NAME_KEY"] = FACEBOOK_NAME_KEY;
                    MeParse["score"] = score;
                    MeParse["SCORE_RECORD_KEY"] = SCORE_RECORD_KEY;
                    MeParse["healthFull"] = healthFull;
                    MeParse["energyFull"] = energyFull;
                    MeParse["level"] = level;
                    MeParse["distance"] = distance;
                    MeParse["DISTANCE_RECORD_KEY"] = DISTANCE_RECORD_KEY;
                    MeParse["burn"] = burn;
                    MeParse["burnRecord"] = burnRecord;
                    MeParse["expCurrent"] = expCurrent;
                    MeParse["expMax"] = expMax;

                    MeParse.SaveAsync();

                    Debug.Log("Me Saved");
                });
            }
            else
            {
                Debug.Log("Me data doesn't exist");
                
                ParseObject MeParse = new ParseObject("Me");
                MeParse["FACEBOOK_NAME_KEY"] = FACEBOOK_NAME_KEY;
                MeParse["score"] = score;
                MeParse["SCORE_RECORD_KEY"] = SCORE_RECORD_KEY;
                MeParse["healthFull"] = healthFull;
                MeParse["energyFull"] = energyFull;
                MeParse["level"] = level;
                MeParse["distance"] = distance;
                MeParse["DISTANCE_RECORD_KEY"] = DISTANCE_RECORD_KEY;
                MeParse["burn"] = burn;
                MeParse["burnRecord"] = burnRecord;
                MeParse["expCurrent"] = expCurrent;
                MeParse["expMax"] = expMax;
                MeParse["facebookId"] = facebookId;
                MeParse.SaveAsync();
                Debug.Log("Me Saved");
            }
        });
    }

    public static void SaveScoreToRankParse()
    {
        //initial
        
        int score = PlayerPrefs.GetInt(MePlayerPrefs.SCORE_KEY, 0);
        string FACEBOOK_NAME_KEY = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_NAME_KEY, "");


        Debug.Log("Me : score initial finished");

        var query = ParseObject.GetQuery("Rank")
       .WhereEqualTo("facebookId", facebookId);
        query.CountAsync().ContinueWith(t =>
        {
            t.Wait();
            int count = t.Result;
            if (count == 1)
            {
                Debug.Log("Rank data exist");
                //ถ้ามีข้อมูลอยู่ ให้ update
                var query2 = ParseObject.GetQuery("Rank").WhereEqualTo("facebookId", facebookId);
                query2.FirstAsync().ContinueWith(t2 =>
                {
                    t2.Wait();
                    ParseObject RankParse = t2.Result;
                   
                   RankParse["score"] = score;
                    RankParse.SaveAsync();
                    Debug.Log("Rank Saved");
                });
            }
            else
            {
                Debug.Log("Rank data doesn't exist");

                ParseObject RankParse = new ParseObject("Rank");
                RankParse["FACEBOOK_NAME_KEY"] = FACEBOOK_NAME_KEY;
                RankParse["score"] = score;
              
                RankParse["facebookId"] = facebookId;
                RankParse.SaveAsync();
                Debug.Log("Rank Saved");
            }
        });
    }


}
