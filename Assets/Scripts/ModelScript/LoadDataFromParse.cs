﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Parse;
using System;
using System.Threading.Tasks;

public class LoadDataFromParse
{

    public string facebookId;

    //collection part
    public int lion=0, elephant=0, monkey=0, raccoon=0, bear=0, rabbit=0;
    public bool isCollectionFinished;


    public bool isBackpackFinished;
    public int sleepingDart=15, healthPotion=2, energyPotion=2;

    public bool isMeFinished;
    public int MeBurn=0, MeBurnRecord=0, MeDistance=0, MeDISTANCE_RECORD_KEY=0, energyFull=0, expCurrent=0,
        expMax=11, healthFull=107, MeLevel=1, MeScore=0, MeSCORE_RECORD_KEY=0, money=300, currentHP = 107;
    public string FACEBOOK_NAME_KEY;

    public bool isChallengeCompleteFinished;
    public List<Challenge> challengeFromParse;

    public bool isActivityFinished;
    public List<Activity> activityFromParse;

    public bool isAllComplete;

    public LoadDataFromParse()
    {
        facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY, "1234567890");
        Debug.Log("facebookId :: " + facebookId);

        LoadActivityFromParse();
        LoadChallengeCompleteFromParse();
        LoadMeDataFromParse();
        LoadCollectionDataFromParse();
        LoadBackpackDataFromParse();

        isAllComplete = isActivityFinished && isChallengeCompleteFinished && isMeFinished && isBackpackFinished && isCollectionFinished;

    }

    public void LoadActivityFromParse()
    {
        var query = ParseObject.GetQuery("Activity")
         .WhereEqualTo("facebookId", facebookId);

        query.CountAsync().ContinueWith(t =>
        {
            t.Wait();
            int count = t.Result;
            if (count > 0)
            {
                Debug.Log("activity c data exist");

                var query2 = ParseObject.GetQuery("Activity")
    .WhereEqualTo("facebookId", facebookId);
                query2.FindAsync().ContinueWith(t2 =>
                {
                    t2.Wait();
                    IEnumerable<ParseObject> results = t2.Result;
                    foreach (ParseObject OKResults in results)
                    {
                        Activity aa = new Activity(facebookId,
                             OKResults.Get<int>("minute"),
                             OKResults.Get<int>("distance"),
                           OKResults.Get<int>("weight"),
                           OKResults.Get<int>("burned"),
                           OKResults.Get<DateTime>("saveDate"));

                        activityFromParse.Add(aa);
                    }

                    isActivityFinished = true;
                    isAllComplete = isActivityFinished && isChallengeCompleteFinished && isMeFinished && isBackpackFinished && isCollectionFinished;

                    Debug.Log("Activity Loaded");
                });
            }
            else
            {
                Debug.Log("activity data doesn't exist");

                isActivityFinished = true;
                isAllComplete = isActivityFinished && isChallengeCompleteFinished && isMeFinished && isBackpackFinished && isCollectionFinished;

                activityFromParse = new List<Activity>();
            }
        });
    }
    
    public void LoadChallengeCompleteFromParse()
    {
        var query = ParseObject.GetQuery("ChallengeComplete")
         .WhereEqualTo("facebookId", facebookId);

        query.CountAsync().ContinueWith(t =>
        {
            t.Wait();
            int count = t.Result;
            if (count > 0)
            {
                Debug.Log("challenge c data exist");


                var query2 = ParseObject.GetQuery("ChallengeComplete")
    .WhereEqualTo("facebookId", facebookId);
                query2.FindAsync().ContinueWith(t2 =>
                {
                    t2.Wait();
                    IEnumerable<ParseObject> results = t2.Result;
                    foreach (ParseObject OKResults in results)
                    {
                        Challenge c = new Challenge(facebookId,
                             OKResults.Get<string>("challengeName"),
                            OKResults.Get<int>("challengeLevel"),
                           OKResults.Get<int>("target"),
                           OKResults.Get<int>("scoreReward"),
                           OKResults.Get<DateTime>("saveDate"));

                        challengeFromParse.Add(c);
                    }

                    isChallengeCompleteFinished = true;
                    Debug.Log("Challenge Loaded");
                    isAllComplete = isActivityFinished && isChallengeCompleteFinished && isMeFinished && isBackpackFinished && isCollectionFinished;

                });

            }
            else
            {
                Debug.Log("challenge c data doesn't exist");
                isChallengeCompleteFinished = true;
                isAllComplete = isActivityFinished && isChallengeCompleteFinished && isMeFinished && isBackpackFinished && isCollectionFinished;
                challengeFromParse = new List<Challenge>();
            }
        });
    }

    public void LoadCollectionDataFromParse()
    {
        var query = ParseObject.GetQuery("Collection")
          .WhereEqualTo("facebookId", facebookId);

        query.CountAsync().ContinueWith(t =>
        {
            t.Wait();
            int count = t.Result;
            if (count > 0)
            {
                Debug.Log("collection data exist");

                var query2 = ParseObject.GetQuery("Collection")
    .WhereEqualTo("facebookId", facebookId);
                query2.FirstAsync().ContinueWith(t2 =>
                {
                    t2.Wait();
                    ParseObject results = t2.Result;
                    lion = results.Get<int>("lion");
                    elephant = results.Get<int>("elephant");
                    monkey = results.Get<int>("monkey");
                    raccoon = results.Get<int>("raccoon");
                    bear = results.Get<int>("bear");
                    rabbit = results.Get<int>("rabbit");

                    Debug.Log("Collection Loaded");

                    isCollectionFinished = true;
                    isAllComplete = isActivityFinished && isChallengeCompleteFinished && isMeFinished && isBackpackFinished && isCollectionFinished;
                });

            }
            else
            {
                Debug.Log("collection  data doesn't exist");

                isCollectionFinished = true;
                isAllComplete = isActivityFinished && isChallengeCompleteFinished && isMeFinished && isBackpackFinished && isCollectionFinished;

            }
        });
    }

    private void LoadBackpackDataFromParse()
    {
        var query = ParseObject.GetQuery("Backpack")
           .WhereEqualTo("facebookId", facebookId);

        query.CountAsync().ContinueWith(t =>
        {
            t.Wait();
            int count = t.Result;
            if (count > 0)
            {
                Debug.Log("backpack data exist");

                var query2 = ParseObject.GetQuery("Backpack")
    .WhereEqualTo("facebookId", facebookId);
                query2.FirstAsync().ContinueWith(t2 =>
                {
                    t2.Wait();
                    ParseObject results = t2.Result;
                    sleepingDart = results.Get<int>("sleepDart");
                    healthPotion = results.Get<int>("healthPotion");
                    energyPotion = results.Get<int>("energyPotion");
                    money = results.Get<int>("money");

                    isBackpackFinished = true;
                    isAllComplete = isActivityFinished && isChallengeCompleteFinished && isMeFinished && isBackpackFinished && isCollectionFinished;
                });
            }
            else
            {
                Debug.Log("backpack  data doesn't exist");
                isBackpackFinished = true;
                isAllComplete = isActivityFinished && isChallengeCompleteFinished && isMeFinished && isBackpackFinished && isCollectionFinished;
            }
        });
    }

    private void LoadMeDataFromParse()
    {
        var query = ParseObject.GetQuery("Me")
       .WhereEqualTo("facebookId", facebookId);
        query.CountAsync().ContinueWith(t =>
        {
            isMeFinished = false;
            t.Wait();
            int count = t.Result;
            if (count == 1)
            {
                Debug.Log("Me data exist");
                //ถ้ามีข้อมูลอยู่ ให้ update
                var query2 = ParseObject.GetQuery("Me").WhereEqualTo("facebookId", facebookId);
                query2.FirstAsync().ContinueWith(t2 =>
                {
                    t2.Wait();
                    ParseObject MeParse = t2.Result;

                    MeBurn = MeParse.Get<int>("burn");
                    MeBurnRecord = MeParse.Get<int>("burnRecord");
                    MeDistance = MeParse.Get<int>("distance");
                    MeDISTANCE_RECORD_KEY = MeParse.Get<int>("DISTANCE_RECORD_KEY");
                    energyFull = 100;
                    expCurrent = MeParse.Get<int>("expCurrent");
                    expMax = MeParse.Get<int>("expMax");
                    healthFull = MeParse.Get<int>("healthFull");
                    MeLevel = MeParse.Get<int>("level");
                    MeScore = MeParse.Get<int>("score");
                    MeSCORE_RECORD_KEY = MeParse.Get<int>("SCORE_RECORD_KEY");
                    currentHP = healthFull;
                    isMeFinished = true;
                    isAllComplete = isActivityFinished && isChallengeCompleteFinished && isMeFinished && isBackpackFinished && isCollectionFinished;
                    Debug.Log("Me Loaded");

                });
            }
            else
            {
                Debug.Log("Me data doesn't exist");
                isMeFinished = true;
                isAllComplete = isActivityFinished && isChallengeCompleteFinished && isMeFinished && isBackpackFinished && isCollectionFinished;
            }
                
        });
        

    }
}
