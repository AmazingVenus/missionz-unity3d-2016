﻿public class SettingsPlayerPref{
    public static string CAMERA_ON_CATCH_ANIMAL = "CAMERA_ON_CATCH_ANIMAL";
    public static string SOUND_EFFECT = "SOUND_EFFECT";
    public static string MUSIC = "MUSIC";

    public static bool DEFAULT_CAMERA_ON_CATCH_ANIMAL = true;
    public static bool DEFAULT_SOUND_EFFECT = true;
    public static bool DEFAULT_MUSIC = true;
}
