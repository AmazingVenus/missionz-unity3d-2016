﻿public class BackpackPlayerPrefs{
     public  static string HEALTH_POTION_KEY = "HEALTH_POTION_KEY";
     public  static string ENERGY_POTION_KEY = "ENERGY_POTION_KEY";
     public  static string SLEEP_DART_KEY = "SLEEP_DART_KEY";
     public  static string MONEY_KEY = "MONEY_KEY";

    public static int DEFAULT_HEALTH_POTION_KEY = 10;
    public static int DEFAULT_ENERGY_POTION_KEY = 10;
    public static int DEFAULT_SLEEP_DART_KEY = 50;
    public static int DEFAULT_MONEY_KEY = 1500;
}
