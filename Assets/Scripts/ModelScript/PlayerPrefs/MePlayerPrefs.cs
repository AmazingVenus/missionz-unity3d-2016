﻿public  class MePlayerPrefs
{


     public  static string FACEBOOK_ID_KEY = "FACEBOOK_ID_KEY";
     public  static string FACEBOOK_NAME_KEY = "FACEBOOK_NAME_KEY";

     public  static string HEIGHT_KEY = "HEIGHT_KEY";
     public  static string WEIGHT_KEY = "WEIGHT_KEY";
    
     public  static string SCORE_KEY = "SCORE_KEY";
     public  static string HEALTH_CURRENT_KEY = "HEALTH_CURRENT_KEY";
     public  static string ENERGY_CURRENT_KEY = "ENERGY_CURRENT_KEY";
     public  static string HEALTH_MAX_KEY = "HEALTH_MAX_KEY";
     public  static string ENERGY_MAX_KEY = "ENERGY_MAX_KEY";
     public  static string DISTANCE_TRAVELLED_KEY = "DISTANCE_TRAVELLED_KEY";
     public  static string LEVEL_KEY = "LEVEL_KEY";
     public  static string EXP_CURRENT_KEY = "EXP_CURRENT_KEY";
     public  static string EXP_MAX_KEY = "EXP_MAX_KEY";
     public  static string BURNED_KEY = "BURNED_KEY";

     public  static string DISTANCE_RECORD_KEY = "DISTANCE_RECORD_KEY";
     public  static string BURNED_RECORD_KEY = "BURNED_RECORD_KEY";
     public  static string SCORE_RECORD_KEY = "SCORE_RECORD_KEY";

     public  static string CURRENT_ANIMAL_ID = "CURRENT_ANIMAL_ID";


    public static string DEFAULT_FACEBOOK_ID_KEY = "1234567890";
    public static string DEFAULT_FACEBOOK_NAME_KEY = "FACEBOOK_NAME_KEY";

    public static int DEFAULT_HEIGHT_KEY = 160;
    public static int DEFAULT_WEIGHT_KEY = 55;

    public static int DEFAULT_SCORE_KEY = 0;
  
    public static int DEFAULT_HEALTH_MAX_KEY = 107;
    public static int DEFAULT_ENERGY_MAX_KEY = 100;

    public static int DEFAULT_HEALTH_CURRENT_KEY = DEFAULT_HEALTH_MAX_KEY;
    public static int DEFAULT_ENERGY_CURRENT_KEY = 100;

    public static int DEFAULT_DISTANCE_TRAVELLED_KEY = 0;
    public static int DEFAULT_LEVEL_KEY = 1;
    public static int DEFAULT_EXP_CURRENT_KEY = 0;
    public static int DEFAULT_EXP_MAX_KEY = 11;
    public static int DEFAULT_BURNED_KEY = 0;

    public static int DEFAULT_RECORD_KEY = 0;

    public static int DEFAULT_CURRENT_ANIMAL_ID = 0;
}
