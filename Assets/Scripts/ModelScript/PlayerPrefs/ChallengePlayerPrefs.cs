﻿public  class ChallengePlayerPrefs {
 //   public  static string CURRENT_DISTANCE_CHALLENGE_KEY = "CURRENT_DISTANCE_CHALLENGE_KEY";
    public  static string CURRENT_BURNED_CHALLENGE_KEY = "CURRENT_BURNED_CHALLENGE_KEY";
    public  static string CURRENT_MONEY_CHALLENGE_KEY = "CURRENT_MONEY_CHALLENGE_KEY";
    public  static string CURRENT_HUNTER_CHALLENGE_KEY = "CURRENT_HUNTER_CHALLENGE_KEY";

  //  public  static string TARGET_DISTANCE_CHALLENGE_KEY = "TARGET_DISTANCE_CHALLENGE_KEY";
    public  static string TARGET_BURNED_CHALLENGE_KEY = "TARGET_BURNED_CHALLENGE_KEY";
    public  static string TARGET_MONEY_CHALLENGE_KEY = "TARGET_MONEY_CHALLENGE_KEY";
    public  static string TARGET_HUNTER_CHALLENGE_KEY = "TARGET_HUNTER_CHALLENGE_KEY";

   // public  static string LEVEL_DISTANCE_CHALLENGE_KEY = "LEVEL_DISTANCE_CHALLENGE_KEY";
    public  static string LEVEL_BURNED_CHALLENGE_KEY = "LEVEL_BURNED_CHALLENGE_KEY";
    public  static string LEVEL_MONEY_CHALLENGE_KEY = "LEVEL_MONEY_CHALLENGE_KEY";
    public  static string LEVEL_HUNTER_CHALLENGE_KEY = "LEVEL_HUNTER_CHALLENGE_KEY";

    //new challenge
    public static string CHALLENGE_ACTIVE_KEY = "CHALLENGE_ACTIVE_KEY";
    
    public static int DEFAULT_CURRENT_CHALLENGE_KEY = 0;
    
    //public static int DEFAULT_TARGET_DISTANCE_CHALLENGE_KEY = 300;

    public static int DEFAULT_TARGET_BURNED_CHALLENGE_KEY = 250;
    public static int DEFAULT_TARGET_MONEY_CHALLENGE_KEY = 700;
    public static int DEFAULT_TARGET_HUNTER_CHALLENGE_KEY = 10;
    
    public static int DEFAULT_LEVEL_CHALLENGE_KEY = 1;

    public static int DEFAULT_CHALLENGE_ACTIVE_KEY = -1;
    //walking man (deprecated)
    //0 = burn calories
    //1 = rich man
    //2 = hunter

}
