﻿using System;

public  class ActivityPlayerPrefs{
     public  static string  SUM_BURNED_KEY = "SUM_BURNED_KEY";
     public  static string  SUM_DISTANCE_KEY = "SUM_DISTANCE_KEY";
     public  static string  COUNT_DAY_KEY = "COUNT_DAY_KEY";
     public  static string  DATE_LATEST_KEY = "DATE_LATEST_KEY";

    public static int DEFAULT_SUM_BURNED_KEY = 0;
    public static int DEFAULT_SUM_DISTANCE_KEY = 0;
    public static int DEFAULT_COUNT_DAY_KEY = 1;
    public static DateTime DEFAULT_DATE_LATEST_KEY = DateTime.Now;
}
