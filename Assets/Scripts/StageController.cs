﻿using UnityEngine;
using System.Collections;

public class StageController : MonoBehaviour {
	public AnimalInfoDialog AnimalHolder;

	public int PlayerLevel;
	public float PlayerMaxHP;
	public float PlayerCurrentHP;
	public float PlayerCurrentEnergy;
	public float PlayerDamage;
	public int sleepDart;

	public string AnimalName;
	public float AnimalHP;
	public int AnimalLevel;
    public int AnimalDamage;
    public GameObject WarningDialog;
	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void StartStage(){
        Debug.Log("StartStage "+PlayerPrefs.GetInt(MePlayerPrefs.HEALTH_CURRENT_KEY));
        PlayerMaxHP = DataCenterModel.instance.playerMng.MaxHP;
		PlayerCurrentHP = DataCenterModel.instance.playerMng.CurrentHP;
		PlayerCurrentEnergy = DataCenterModel.instance.playerMng.CurrentEnergy;
		PlayerLevel = DataCenterModel.instance.playerMng.Level;
		AnimalHP = AnimalHolder.animal.AnimalHP;
		AnimalLevel = AnimalHolder.animal.AnimalLevel;
		AnimalName = AnimalHolder.animal.AnimalName;
        AnimalDamage = AnimalHolder.animal.AnimalDamage;
        sleepDart = PlayerPrefs.GetInt(BackpackPlayerPrefs.SLEEP_DART_KEY, 0);


        if (DataCenterModel.instance.playerMng.CurrentHP < 10 || sleepDart <= 0)
        {
            //Show Dialog
            WarningDialog.GetComponent<AlertDialog>().Title = "Can't fight!";
            WarningDialog.GetComponent<AlertDialog>().message = "Your HP is too low OR out of sleep dart";
            WarningDialog.GetComponent<AlertDialog>().Show();
        }
        else {
            DataCenterModel.instance.playerMng.CurrentEnergy -= Random.Range(10, 15);
            PlayerCurrentEnergy = DataCenterModel.instance.playerMng.CurrentEnergy;
            DataCenterModel.instance.SaveAllData();
            Application.LoadLevel("GamePlayLoadingScreen");
        }
        
	}
}
