﻿using UnityEngine;
using System.Collections;

public class TransitionEffect : UIAnimation {

	//public Vector2 StartPosition;
	private Vector2 CurrentPosition;
	public Vector2 EndPosition;
	public float Speed;
	private Vector2 SpeedVector;
	// Use this for initialization


	void Start () {
		CurrentPosition = this.gameObject.transform.localPosition;
		//Debug.Log (CurrentPosition.ToString ());
		SpeedVector.x = (EndPosition.x - CurrentPosition.x);
		SpeedVector.y = (EndPosition.y - CurrentPosition.y);
	}
	
	// Update is called once per frame
	void Update () {


	}
	public void play(){
		SpeedVector.x = (EndPosition.x - CurrentPosition.x);
		SpeedVector.y = (EndPosition.y - CurrentPosition.y);
		StartCoroutine (Animate ());
		
	}

	public IEnumerator Animate(){

		while (Mathf.Abs(SpeedVector.x) >= 0.01f || Mathf.Abs(SpeedVector.y) >= 0.01f) {

			CurrentPosition.x += Time.deltaTime * SpeedVector.x;
			CurrentPosition.y += Time.deltaTime * SpeedVector.y;
			SpeedVector.x = (EndPosition.x - CurrentPosition.x) * Speed;
			SpeedVector.y = (EndPosition.y - CurrentPosition.y) * Speed;
		
			this.gameObject.transform.localPosition = CurrentPosition;
			yield return new WaitForSeconds(0.005f); // Refresh rate
		}
		isActive = false;
	}
}
