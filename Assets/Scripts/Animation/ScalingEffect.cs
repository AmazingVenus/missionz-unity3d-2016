﻿using UnityEngine;
using System.Collections;

public class ScalingEffect : UIAnimation {

	public Vector2 currentScale;
	public Vector2 EndScale;
	public float Speed;
	// Use this for initialization
	void Start () {
		currentScale = this.gameObject.transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void play(){
		try{
			StartCoroutine (Animate ());
		}catch{
			
		}
		
	}
	public IEnumerator Animate(){
		
		while (Vector2.Distance(currentScale,EndScale) >= 0.01) {
			
			currentScale = Vector2.Lerp(currentScale,EndScale,Speed*Time.deltaTime);
			this.gameObject.transform.localScale = currentScale;
			yield return new WaitForSeconds(0.005f); // Refresh rate
		}
		isActive = false;
	}
}
