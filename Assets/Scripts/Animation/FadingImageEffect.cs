﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadingImageEffect : MonoBehaviour
{

    public float Speed;
    public float CurrentSpeed;
    public float Opacity;
    public Color CurrentOpacity;
    // Use this for initialization
    void Start()
    {
        CurrentOpacity = this.GetComponent<Image>().color;
        CurrentSpeed = (Opacity - CurrentOpacity.a);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void play()
    {
        CurrentSpeed = (Opacity - CurrentOpacity.a);
        StartCoroutine(Animate());

    }

    public IEnumerator Animate()
    {
        while (Mathf.Abs(CurrentSpeed) >= 0.0001f)
        {

            CurrentOpacity.a += Time.deltaTime * CurrentSpeed;

            CurrentSpeed = (Opacity - CurrentOpacity.a) * Speed;

            this.GetComponent<Image>().color = CurrentOpacity;
            yield return new WaitForSeconds(0.005f); // Refresh rate
        }
    }
}
