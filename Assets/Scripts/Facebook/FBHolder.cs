﻿using UnityEngine;
using System.Collections;
using Facebook;
using UnityEngine.UI;
using System.Collections.Generic;
using Facebook.MiniJSON;
using System.IO;

public class FBHolder : MonoBehaviour {
	public string FACEBOOK_NAME_KEY;
	public Sprite ProfileImage;
    public string FacebookID;
    public GameObject UINotLoggedIn;
    public GameObject LoggedIn;
    public Text LoadingStatusText;
    public Dictionary<string, object> profile = null;

    public bool isLogged;
    void Awake()
    {
        
        DontDestroyOnLoad(gameObject);
        
        int isLoggedIn = PlayerPrefs.GetInt(MissionZPlayerPref.GAME_LOGIN_STATUS_KEY, 0);
        Debug.Log("Is Login : " + isLoggedIn);
        if (isLoggedIn == 0)
        {
            FB.Init(SetInit, onHideUnity);
        }
        else {
            Debug.Log("It's Logged in, don't connect to Facebook");
            UINotLoggedIn.SetActive(false);
            Texture2D tex = LoadPNG(Application.persistentDataPath + "/ProfileImage.png");
            ProfileImage = Sprite.Create(tex, new Rect(0, 0, 256, 256), new Vector2(0, 0));
            //FB.Init(SetInit, onHideUnity);
            //FB.API(Util.GetPictureURL("me", 256, 256), Facebook.HttpMethod.GET, DealWithProfilePicture);
            Application.LoadLevelAsync("main");
        }
    }
    private void SetInit()
    {
        Debug.Log("FB Init done.");
        if (FB.IsLoggedIn)
        {
            Debug.Log("FB Logged in.");
        }
        else
        {

        }
    }


    void Start(){
        
        LoadingStatusText.gameObject.SetActive(false);
        // LoadingStatusText.text = Application.persistentDataPath;
	}

    private void onHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;

        }
    }

    public void FBlogin()
    {
        //FB.Login("email,publish_actions", AuthCallback);
        FB.Login("", AuthCallback);
    }

    void AuthCallback(FBResult result)
    {
        LoadingStatusText.gameObject.SetActive(true);
        Debug.Log("AuthCallback");
        LoadingStatusText.text = "Start AuthCallback";
        if (FB.IsLoggedIn)
        {
            LoadingStatusText.text = "FB.IsLoggedIn";
            Debug.Log("Facebook Login worked!");
            //PlayerPrefs.SetInt(MissionZPlayerPref.GAME_LOGIN_STATUS_KEY, 1);
            DealWithFBMenus(true);
        }
        else
        {
            LoadingStatusText.text = "Facebook Login Fail!";
            LoadingStatusText.text = result.Text;
            Debug.Log("Facebook Login Fail!");
            //PlayerPrefs.SetInt(MissionZPlayerPref.GAME_LOGIN_STATUS_KEY, 0);
            DealWithFBMenus(false);
        }
    }

    void DealWithFBMenus(bool isLoggedIn)
    {
        UINotLoggedIn.SetActive(!isLoggedIn);
        if (isLoggedIn)
        {
            
            FB.API("/me?friends=id,name", Facebook.HttpMethod.GET, DealWithUsername);
            FB.API(Util.GetPictureURL("me", 256, 256), Facebook.HttpMethod.GET, DealWithProfilePicture);
        }
    }

    void DealWithProfilePicture(FBResult result)
    {
        LoadingStatusText.text = "Generating Animals Position";
        Debug.Log("Getting profile picture...");
        if (result.Error != null)
        {
            Debug.Log("Problem with getting profile picture");
            FB.API(Util.GetPictureURL("me", 256, 256), Facebook.HttpMethod.GET, DealWithProfilePicture);
            return;
        }

        //Write file to disk
        byte[] bytes = result.Texture.EncodeToPNG();
        File.WriteAllBytes(Application.persistentDataPath + "/ProfileImage.png", bytes);

        ProfileImage = Sprite.Create(result.Texture, new Rect(0, 0, 256, 256), new Vector2(0, 0));
        Debug.Log("Getting profile picture Done!");
        LoadingStatusText.text = "Applying Animals Position to Map ";
        StartCoroutine(WaitForImage());
    }
    void DealWithUsername(FBResult result)
    {
        LoadingStatusText.text = "Initializing Animal Finding System";
        if (result.Error != null)
        {
            Debug.Log("Problem with getting Username");
            FB.API("/me?friends=id,name", Facebook.HttpMethod.GET, DealWithUsername);
            return;
        }
        profile = Json.Deserialize(result.Text) as Dictionary<string, object>;
        Debug.Log("ID : " + profile["id"].ToString());
        FacebookID = profile["id"].ToString();
        FACEBOOK_NAME_KEY = profile["name"].ToString();
        PlayerPrefs.SetString(MePlayerPrefs.FACEBOOK_NAME_KEY, FACEBOOK_NAME_KEY);
        PlayerPrefs.SetString(MePlayerPrefs.FACEBOOK_ID_KEY, FacebookID);
        PlayerPrefs.Save();
        LoggedIn.SetActive(true);
        Debug.Log("Start Loading data from parse");
    }


    public void DeleteLoginData() {
        PlayerPrefs.DeleteKey(MissionZPlayerPref.GAME_LOGIN_STATUS_KEY);
    }

    public IEnumerator WaitForImage() {
        
        yield return new WaitForSeconds(3);
        LoadingStatusText.text = "Loading player data";
        //Application.LoadLevelAsync("main");
    }

    public static Texture2D LoadPNG(string filePath)
    {

        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        return tex;
    }
}
