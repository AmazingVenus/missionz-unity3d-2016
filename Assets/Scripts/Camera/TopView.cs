﻿using UnityEngine;
using System.Collections;

public class TopView : MonoBehaviour
{
    public Vector3 Target;
    public float smooth;
    //public AnimalButtonScript AnimalButton;
    private bool trigger = true;
    private float distance;

	public float currentSize;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = Vector3.Lerp(this.transform.position, Target, smooth * Time.deltaTime);
        //this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Target.transform.rotation, smooth * Time.deltaTime);

        distance = Vector3.Distance(transform.position, Target);
        if (distance <= 0.5f && trigger)
        {
            //AnimalButton.ShowButton();
            trigger = false;
        }

        if (distance > 0.5f)
        {
            trigger = true;
        }

		currentSize = 9.0f - GetComponent<Camera> ().orthographicSize;

		GetComponent<Camera> ().orthographicSize += 2f*currentSize * Time.deltaTime;
    }
}
