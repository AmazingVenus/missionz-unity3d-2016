﻿using UnityEngine;
using System.Collections;

public class FocusObject : MonoBehaviour
{

    public Transform target;
    public float smoothing = 5f;
	public float currentSize;
    void Start()
    {
        // Calculate the initial offset.
        //offset = targetCamPosition.transform.position - target.position;

    }

    void FixedUpdate()
    {

        transform.position = Vector3.Lerp(transform.position, target.position, smoothing * Time.deltaTime);
		currentSize = Mathf.Abs(2f - GetComponent<Camera> ().orthographicSize);
		GetComponent<Camera> ().orthographicSize -= 2f*currentSize * Time.deltaTime;


    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }
}
