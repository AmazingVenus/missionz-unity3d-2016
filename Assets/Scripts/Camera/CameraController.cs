﻿using UnityEngine;
using System.Collections;


public class CameraController : MonoBehaviour
{

    public int PositionIndex;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        SelectCameraOption(PositionIndex);
		LockZ ();
    }

    public void SelectCameraOption(int index)
    {

        switch (index)
        {
            case 1: //Top view.
                this.GetComponent<TopView>().enabled = true;
                this.GetComponent<FocusObject>().enabled = false;
                break;
            case 2: // Focus object
                this.GetComponent<TopView>().enabled = false;
                this.GetComponent<FocusObject>().enabled = true;
                break;

        }
    }

    public void setPositionIndex(int index)
    {
        PositionIndex = index;
    }

    public void setFocusObject(Transform target)
    {

    }

	public void LockZ(){
		Vector3 temp = transform.position;
		temp.z = 0;
		transform.position = temp;

	}
}
