﻿using UnityEngine;
using System.Collections;

public class MapControl : MonoBehaviour {
    
	public GameObject[] AnimalNodes;
	public GameObject PlayerPin;
	public int CurrentIndex;
	public GameObject CurrentAnimalNode;

	public bool isFindPath;
	public int click_ID;
    public GameObject DistanceAlert;
    public GameObject infoDialog;
    public StageController stageController;
    // Use this for initialization
    void Start () {
        CurrentIndex = PlayerPrefs.GetInt(MePlayerPrefs.CURRENT_ANIMAL_ID, 0);
        CurrentAnimalNode = AnimalNodes[CurrentIndex];
    }
	
	// Update is called once per frame
	void Update () {
        CurrentIndex = CurrentAnimalNode.GetComponent<Node> ().ID;
		if (isFindPath) {
			FindPath(click_ID);
			isFindPath = false;
		}

		PlayerPin.transform.position = Vector3.Lerp (PlayerPin.transform.position, CurrentAnimalNode.GetComponent<Node> ().StopPoint.transform.position, 5f * Time.deltaTime);
	}

	public void FindPath(int ID){
		StartCoroutine (Animate (ID));
	}
    public void GoToFightAnimal() {
        if (CurrentIndex != click_ID)
        {
            Debug.Log("Go Foght");
            FindPath(click_ID);
        }
        else {
            stageController.StartStage();
        }
        
    }

	public IEnumerator Animate(int ID){

        if (ID < CurrentIndex)
        {//when player go backward
            for (int i = CurrentIndex - 1; i >= ID; i--)
            {//Change animal util done

                if (DataCenterModel.instance.playerMng.CurrentDistance < CurrentAnimalNode.GetComponent<Node>().previousDistance)
                {
                    WhenPlayerNotEnoughDistance();
                    break;
                }
                else
                {
                    DataCenterModel.instance.playerMng.CurrentDistance -= CurrentAnimalNode.GetComponent<Node>().previousDistance;
                }

                CurrentAnimalNode = AnimalNodes[i];

                //Save current player position
                UpdatePlayerData();

                if (i == ID)
                {
                    yield return new WaitForSeconds(1f);
                    stageController.StartStage();
                    //Show Animal info dialog when player stay at animal
                    //infoDialog.GetComponent<AnimalInfoDialog>().animal = CurrentAnimalNode.GetComponent<Node>().Current;
                    //yield return new WaitForSeconds(1f);
                    //infoDialog.GetComponent<AnimalInfoDialog>().fadeIn();
                }
                yield return new WaitForSeconds(1f);//Delay 1sec when passed each animal

            }
        }
        else if (ID > CurrentIndex)
        { //when player go ahead
            for (int i = CurrentIndex + 1; i <= ID; i++)
            {

                if (DataCenterModel.instance.playerMng.CurrentDistance < CurrentAnimalNode.GetComponent<Node>().nextDistance)
                {
                    WhenPlayerNotEnoughDistance();
                    break;
                }
                else
                {
                    DataCenterModel.instance.playerMng.CurrentDistance -= CurrentAnimalNode.GetComponent<Node>().nextDistance;
                }
                Debug.Log("Distance : " + CurrentAnimalNode.GetComponent<Node>().nextDistance);

                CurrentAnimalNode = AnimalNodes[i];
                //Save current player position
                UpdatePlayerData();

                Debug.Log("ID : " + CurrentAnimalNode.GetComponent<Node>().ID);
                if (i == ID)
                {
                    yield return new WaitForSeconds(1f);
                    stageController.StartStage();
                    //Show Animal info dialog when player stay at animal
                    //infoDialog.GetComponent<AnimalInfoDialog>().animal = CurrentAnimalNode.GetComponent<Node>().Current;
                    //yield return new WaitForSeconds(1f);
                    //infoDialog.GetComponent<AnimalInfoDialog>().fadeIn();
                }
                yield return new WaitForSeconds(1f);//Delay when arrive at target


            }
        }
        else {
            //Show Animal info dialog when player stay at animal
            //infoDialog.GetComponent<AnimalInfoDialog>().animal = CurrentAnimalNode.GetComponent<Node>().Current;
            //infoDialog.GetComponent<AnimalInfoDialog>().fadeIn();

            yield return new WaitForSeconds(1f);
            stageController.StartStage();
        }

	}

    public void UpdatePlayerData() {
        PlayerPrefs.SetInt(MePlayerPrefs.CURRENT_ANIMAL_ID, CurrentAnimalNode.GetComponent<Node>().ID);
        //PlayerPrefs.SetFloat(MePlayerPrefs.DISTANCE_TRAVELLED_KEY, DataCenterModel.instance.playerMng.CurrentDistance);
    }

    public void WhenPlayerNotEnoughDistance()
    {
        DistanceAlert.GetComponent<AlertDialog>().Title = "Your distance not Enough!";
        DistanceAlert.GetComponent<AlertDialog>().message = "Please go to walk or run more";
        DistanceAlert.GetComponent<AlertDialog>().Show();
    }

}
