﻿using UnityEngine;
using System.Collections;

public class AnimalInfoData : MonoBehaviour {
	public Animal animal;
	public AnimalInfoDialog dialog;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		dialog.animal = this.animal;
	}
}
