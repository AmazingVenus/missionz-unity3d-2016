﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AnimalButtonScript : MonoBehaviour
{
    public Button[] buttons;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //if Top view is active

    }

    public void HideButton()
    {
        int n = buttons.Length;
        for (int i = 0; i < n; i++)
        {
            buttons[i].interactable = false;
            buttons[i].GetComponent<FadingImageEffect>().Opacity = 0;
            buttons[i].GetComponent<FadingImageEffect>().play();
        }
    }
    public void ShowButton()
    {
        int n = buttons.Length;
        for (int i = 0; i < n; i++)
        {
            buttons[i].GetComponent<FadingImageEffect>().Opacity = 1;
            buttons[i].GetComponent<FadingImageEffect>().play();
            buttons[i].interactable = true;
        }
    }

}
