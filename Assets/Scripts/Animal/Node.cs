﻿using UnityEngine;
using System.Collections;

public class Node : MonoBehaviour {
	public int ID;
    public Animal Current;
	public GameObject StopPoint;
	public Animal next;
	public int nextDistance;

	public Animal previous;
	public int previousDistance;

	public int status; // 0=lock 1=unlocked 2=cleared
	// Use this for initialization
	void Start () {
        Current = this.GetComponent<Animal>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
