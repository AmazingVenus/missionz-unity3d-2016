﻿using UnityEngine;
using System.Collections;

public class Animal : MonoBehaviour {
	public string AnimalName;
	public string AnimalDescription;
	public int AnimalLevel;
	public float AnimalHP;
	public int AnimalDamage;
	public Sprite AnimalImage;

	// Use this for initialization
	void Start () {
		AnimalDamage = AnimalDamageCalculator (AnimalLevel);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public int AnimalDamageCalculator(int level){
		return Mathf.CeilToInt(12.0f * Mathf.Pow(1.24f,0.24f*level) );
	}
}
