﻿using UnityEngine;
using System.Collections;

public class EyeBlink : MonoBehaviour {
	public Material CloseEyeMat;
	public Material OpenEyeMat;
	private bool isClosed;
	// Use this for initialization
	void Start () {
		StartCoroutine (Blink ());
	}
	
	// Update is called once per frame
	void Update () {

	}
	public IEnumerator Blink(){
		GetComponent<Renderer> ().material = OpenEyeMat;
		yield return new WaitForSeconds(Random.Range(1,3));
		GetComponent<Renderer> ().material = CloseEyeMat;
		yield return new WaitForSeconds(0.1f);
		StartCoroutine (Blink ());
	}

}
