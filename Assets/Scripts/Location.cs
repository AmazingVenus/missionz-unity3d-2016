﻿using UnityEngine;
using System.Collections;

public class Location {
    public double Latitude;
    public double Longitude;

    public Location() {
        Latitude = 0;
        Longitude = 0;
    }
    public Location(double latitude, double longitude)
    {
        Latitude = latitude;
        Longitude = longitude;
    }
}
