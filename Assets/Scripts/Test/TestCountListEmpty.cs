﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

public     class TestCountListEmpty : MonoBehaviour
    {
    List<Activity> tempActivities;
    void Start() {
        ActivityManager.ResetJsonFile();
        tempActivities = new List<Activity>();
        tempActivities = JsonConvert.DeserializeObject<List<Activity>>(File.ReadAllText(ActivityManager.jsonPath));
    }

    void Update() {

        Debug.Log(tempActivities.Count);
    }
    }

