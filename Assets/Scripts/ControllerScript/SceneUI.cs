﻿using UnityEngine;
using System.Collections;

public class SceneUI : GameUI {
	public bool isShowPanel;
    public bool isShowBackButton;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		SceneManagerModel.instance.panelBar.SetActive (isShowPanel);
        SceneManagerModel.instance.backButton.SetActive(isShowBackButton);
		SceneManagerModel.instance.panelBar.GetComponent<PanelUI>().title = this.title;
		SceneManagerModel.instance.panelBar.GetComponent<PanelUI>().panelColor = this.panelColor;

	}
}
