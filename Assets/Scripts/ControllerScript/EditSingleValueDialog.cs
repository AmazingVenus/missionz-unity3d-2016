﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EditSingleValueDialog : GameDialog {

	public InputField ValueHolder;
	public Button SaveButton;
	public Text UnitValueText;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void setUnit(string unit){
		UnitValueText.text = unit;
	}

	

}
