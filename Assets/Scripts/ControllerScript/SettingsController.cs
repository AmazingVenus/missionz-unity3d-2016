﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using System.Threading;

public class SettingsController : MonoBehaviour {
    public GameObject dialogWarningNoConnection;

    public GameObject dialogBackupProcessing;
    public bool isBackupSucces;

    public GameObject dialogWeightSetting;
    public Text weightValue;
    public Button upWeight;
    public Button downWeight;
    //ป้องกันการกดน้ำหนักน้อยเกินจริง

    public GameObject dialogHeightSetting;
    public Text heightValue;
    public Button upHeight;
    public Button donwHeight;

    public Text heightTextShow;
    public Text weightTextShow;

    public int weight;
    public int height;

    //ป้องกันการกดความสูงน้อยเกินจริง

    public Text FacebookNameText;
    public Image ProfileImage;


    void Start() {
        weight = (int)DataCenterModel.instance.playerMng.Weight;//PlayerPrefs.GetInt(MePlayerPrefs.WEIGHT_KEY, 55);
        height = (int)DataCenterModel.instance.playerMng.Height;//PlayerPrefs.GetInt(MePlayerPrefs.HEIGHT_KEY, 155);

        SetTextHeight();
        SetTextWeight();
    }


    void Update() {
        FacebookNameText.text = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_NAME_KEY);
        ProfileImage.sprite = GameObject.FindGameObjectWithTag("Facebook").GetComponent<FBHolder>().ProfileImage;
    }



    void SetTextHeight() {
        heightTextShow.text = height+" cm";
    }

    void SetTextWeight() {
        weightTextShow.text = weight+" kg";
    }

    public void OnClickIncreaseNumberWeight() {
        weight++;
        SetWeightDialogText();
    }

    public void OnClickDecreaseNumberWeight()
    {
        if (weight > 10)
        {
            weight--;
            SetWeightDialogText();
        }
    }

    public void OnClickIncreaseNumberHeight()
    {
        height++;
        SetHeightDialogText();
    }

    public void OnClickDecreaseNumberHeight()
    {
        if (height > 30)
        {
            height--;
            SetHeightDialogText();
        }
    }

    public void OnClickSubmitWeightChanged() {

        DataCenterModel.instance.playerMng.Weight = weight;
        PlayerPrefs.SetInt(MePlayerPrefs.WEIGHT_KEY, weight);
        PlayerPrefs.Save();

        SetTextWeight();
        OnClickCloseDialogWeightSetting();

    }

    public void OnClickSubmitHeightChanged() {

        DataCenterModel.instance.playerMng.Height = height;
        PlayerPrefs.SetInt(MePlayerPrefs.HEIGHT_KEY, height);
        PlayerPrefs.Save();

        SetTextHeight();

        OnClickCloseDialogHeightSetting();
    }

    void SetWeightDialogText() {
        weightValue.text = "" + weight;
    }

    void SetHeightDialogText() {
        heightValue.text = "" + height;
    }


     public static IEnumerator checkInternetConnection(Action<bool> action)
    {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null)
        {
            action(false);
        }
        else
        {
            action(true);
        }
    }

    public void OnClickBackupListener() {
        StartCoroutine(checkInternetConnection((isConnected) => {
            if (isConnected == true)
            {
                Debug.Log("Internet");
                OpenDialogBackupWaiting();
                
            }
            else
            {
                Debug.Log("No Internet");
                OpenDialogConnectionWarning();
            }
        }));

    }

  

    

    public void OpenDialogConnectionWarning()
    {
        dialogWarningNoConnection.SetActive(true);
    }

    public void OpenDialogBackupWaiting()
    {

        dialogBackupProcessing.SetActive(true);

        StartCoroutine(CloseDialogBackupWaitting());
    }

    public void OnClickOpenDialogHeightSetting() {
        SetHeightDialogText();
        dialogHeightSetting.SetActive(true);
    }

    public void OnClickOpenDialogWeightSetting() {
        SetWeightDialogText();
        dialogWeightSetting.SetActive(true);
    }

    public void OnClickCloseDialogWeightSetting() {
        dialogWeightSetting.SetActive(false);
    }

    public void OnClickCloseDialogHeightSetting() {
        dialogHeightSetting.SetActive(false);
    }

    public IEnumerator CloseDialogBackupWaitting() {

        yield return new WaitForSeconds(10);
        dialogBackupProcessing.SetActive(false);

        
    }

    public void OnClickCloseDialogConnectionWarning() {
        dialogWarningNoConnection.SetActive(false);
    }

}
