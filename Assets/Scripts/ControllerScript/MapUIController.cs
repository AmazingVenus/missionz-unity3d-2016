﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MapUIController : MonoBehaviour {

	private MapUIView view;
	public bool isShowMenu;
	// Use this for initialization
	void Start () {
        Debug.Log("xxxxxxxxxxxxxxxxxxxxxxxxx");
		view = GetComponent<MapUIView> ();

	}
	
	// Update is called once per frame
	void Update () {
        view.HealthBar.maxValue = 100;//DataCenterModel.instance.playerMng.MaxHP;
		view.EnergyBar.maxValue = 100;
		view.HealthBar.value = 100 * DataCenterModel.instance.playerMng.CurrentHP / DataCenterModel.instance.playerMng.MaxHP;
		view.HealthBar.transform.FindChild ("value").GetComponent<Text> ().text = view.HealthBar.value.ToString("0.#")+" %";

        view.EnergyBar.value = DataCenterModel.instance.playerMng.CurrentEnergy;// DataCenterModel.instance.playerMng.CurrentEnergy;
        view.EnergyBar.transform.FindChild("value").GetComponent<Text>().text = view.EnergyBar.value.ToString() + " %";

        view.ExpBar.maxValue = DataCenterModel.instance.playerMng.MaxExp;
		view.ExpBar.value = DataCenterModel.instance.playerMng.CurrentExp;

		view.LevelText.text = "Level "+DataCenterModel.instance.playerMng.Level.ToString ();
        view.ScoreText.text = DataCenterModel.instance.playerMng.Score.ToString();
        
		if (isShowMenu) {
			ShowMenu ();
		} else {
			HideMenu();
		}

        
	}

	public void ToggleMenu(){
		isShowMenu = !isShowMenu;

	}

	public void ShowMenu(){
		view.CollectionBtn.GetComponent<TransitionEffect> ().EndPosition = new Vector2 (97.5f, 81.4f);
		view.CollectionBtn.GetComponent<TransitionEffect> ().play ();
		view.CollectionBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().Opacity = 1;
		view.CollectionBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().play ();

		view.BackpackBtn.GetComponent<TransitionEffect> ().EndPosition = new Vector2 (97.5f, -88.1f);
		view.BackpackBtn.GetComponent<TransitionEffect> ().play ();
		view.BackpackBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().Opacity = 1;
		view.BackpackBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().play ();

		view.ShopBtn.GetComponent<TransitionEffect> ().EndPosition = new Vector2 (97.5f, -30.6f);
		view.ShopBtn.GetComponent<TransitionEffect> ().play ();
		view.ShopBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().Opacity = 1;
		view.ShopBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().play ();

		view.RankBtn.GetComponent<TransitionEffect> ().EndPosition = new Vector2 (97.5f, 24f);
		view.RankBtn.GetComponent<TransitionEffect> ().play ();
		view.RankBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().Opacity = 1;
		view.RankBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().play ();

		view.ChallengeBtn.GetComponent<TransitionEffect> ().EndPosition = new Vector2 (97.5f, 132.3f);
		view.ChallengeBtn.GetComponent<TransitionEffect> ().play ();
		view.ChallengeBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().Opacity = 1;
		view.ChallengeBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().play ();

        view.SettingBtn.GetComponent<TransitionEffect>().EndPosition = new Vector2(97.5f, -139.7f);
        view.SettingBtn.GetComponent<TransitionEffect>().play();
        view.SettingBtn.transform.FindChild("Button Layer").GetComponent<FadingImageEffect>().Opacity = 1;
        view.SettingBtn.transform.FindChild("Button Layer").GetComponent<FadingImageEffect>().play();

    }

	public void HideMenu(){
		//Debug.Log ("Hide");
		view.BackpackBtn.GetComponent<TransitionEffect> ().EndPosition = new Vector2 (97.5f, -205f);
		view.BackpackBtn.GetComponent<TransitionEffect> ().play ();
		view.BackpackBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().Opacity = 0;
		view.BackpackBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().play ();

		view.ShopBtn.GetComponent<TransitionEffect> ().EndPosition = new Vector2 (97.5f, -205f);
		view.ShopBtn.GetComponent<TransitionEffect> ().play ();
		view.ShopBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().Opacity = 0;
		view.ShopBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().play ();
		
		view.RankBtn.GetComponent<TransitionEffect> ().EndPosition = new Vector2 (97.5f, -205f);
		view.RankBtn.GetComponent<TransitionEffect> ().play ();
		view.RankBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().Opacity = 0;
		view.RankBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().play ();
		
		view.CollectionBtn.GetComponent<TransitionEffect> ().EndPosition = new Vector2 (97.5f, -205f);
		view.CollectionBtn.GetComponent<TransitionEffect> ().play ();
		view.CollectionBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().Opacity = 0;
		view.CollectionBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().play ();

		view.ChallengeBtn.GetComponent<TransitionEffect> ().EndPosition = new Vector2 (97.5f, -205f);
		view.ChallengeBtn.GetComponent<TransitionEffect> ().play ();
		view.ChallengeBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().Opacity = 0;
		view.ChallengeBtn.transform.FindChild ("Button Layer").GetComponent<FadingImageEffect> ().play ();

        view.SettingBtn.GetComponent<TransitionEffect>().EndPosition = new Vector2(97.5f, -205f);
        view.SettingBtn.GetComponent<TransitionEffect>().play();
        view.SettingBtn.transform.FindChild("Button Layer").GetComponent<FadingImageEffect>().Opacity = 0;
        view.SettingBtn.transform.FindChild("Button Layer").GetComponent<FadingImageEffect>().play();
    }
}
