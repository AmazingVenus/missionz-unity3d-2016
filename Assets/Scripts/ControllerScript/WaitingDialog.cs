﻿using UnityEngine;
using UnityEngine.UI;

public class WaitingDialog : MonoBehaviour {
    public Text ProgressText;
    public Slider ProgressBar;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        int percent = Mathf.CeilToInt((ProgressBar.value / ProgressBar.maxValue) * 100);
        ProgressText.text = percent.ToString() + "%";
	}
}
