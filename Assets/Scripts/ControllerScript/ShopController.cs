﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ShopController : MonoBehaviour {
    public Button[] buyButton = new Button[3];
    public Button confirmBuyButton;
    public GameObject buyDialog;
    public Text amountText;
    public Sprite[] spriteItemIcon= new Sprite[3];
    public Image imageItemInDialog;
    public Text moneyText;

    string itemSelected="";
    int amountItem=1;
    int totalPrice = 0;
    int itemPrice = 0;
    int[] price = { 100, 200, 10 }; //healthPotion, energyPotion, sleeDartPrice

    public Color inactiveColor;
    public Color activeColor;



	void Start () {
        SetEnableBuyButton();
        SetTextMoney();
       
	}
	

	void Update () {
        SetEnableBuyButton();
        SetTextMoney();
    }

    void  SetEnableBuyButton() {
        int money = PlayerPrefs.GetInt(BackpackPlayerPrefs.MONEY_KEY, 0);
        for (int i = 0; i < price.Length; i++)
        {
            if (money < price[i])
            {
                
                buyButton[i].enabled = false;
                buyButton[i].GetComponent<Image>().color = inactiveColor;
            }
            else
            {
                buyButton[i].enabled = true;
                buyButton[i].GetComponent<Image>().color = activeColor;
            }
        }
        
    }

    void SetTextMoney() {
        moneyText.text = ""+PlayerPrefs.GetInt(BackpackPlayerPrefs.MONEY_KEY, 300);
     
    }


    public void OnClickBuyItem(int i) {
        amountItem = 1;

      
        if (i == 0) { //click buy HP
          totalPrice  = amountItem*price[0];
            itemPrice = price[0];
            itemSelected = BackpackPlayerPrefs.HEALTH_POTION_KEY;
        } else if (i==1) { //click buy EP
            totalPrice = amountItem*price[1];
            itemPrice = price[1];
            itemSelected = BackpackPlayerPrefs.ENERGY_POTION_KEY;

        } else if (i==2) { //click buy SD
            totalPrice = amountItem*price[2];
            itemPrice = price[2];
            itemSelected = BackpackPlayerPrefs.SLEEP_DART_KEY;
        }

        SetAmountText();
        SetTextConfirmBuyButton();
        SetImageItemIcon(i);
        OpenBuyDialog();
    }

    public void OnClickConfirmBuyButton() {
        int money = PlayerPrefs.GetInt(BackpackPlayerPrefs.MONEY_KEY, 0);
        PlayerPrefs.SetInt(BackpackPlayerPrefs.MONEY_KEY, money - totalPrice);
        PlayerPrefs.SetInt(itemSelected, PlayerPrefs.GetInt(itemSelected, 0)+amountItem);
        PlayerPrefs.Save();
        Debug.Log("money = " + PlayerPrefs.GetInt(BackpackPlayerPrefs.MONEY_KEY, 0));
        closeBuyDialog();
        SetTextMoney();
    
    }
    
    public void OnClickIncreaseAmount() { //จะรู้ได้ไง
        int money = PlayerPrefs.GetInt(BackpackPlayerPrefs.MONEY_KEY, 0);
   
        if (itemPrice * (amountItem + 1) <= money)
        {
            amountItem++;
            totalPrice = itemPrice * amountItem;
            SetAmountText();
            SetTextConfirmBuyButton();
        }
        
    }

    public void OnClickDecreaseAmount() {

        
        if (amountItem >1)
        {
            amountItem--;
            totalPrice = itemPrice * amountItem;
            SetAmountText();
            SetTextConfirmBuyButton();
        }
        
    }

    void SetAmountText() {
        amountText.text = "x " + amountItem;
    }
    void SetImageItemIcon(int i) {
        imageItemInDialog.sprite = spriteItemIcon[i];
    }

   public  void OpenBuyDialog() {
        buyDialog.SetActive(true);
    }
    public void closeBuyDialog() {
      
        buyDialog.SetActive(false);
        amountItem = 1;
       totalPrice = 0;
        itemPrice = 0;
        itemSelected = "";
    }

    
    void SetTextConfirmBuyButton() {
        confirmBuyButton.transform.Find("Text").GetComponent<Text>().text = "" + totalPrice;
    }

    //  int amountTemp = Int32.Parse(amountText.text);
}
