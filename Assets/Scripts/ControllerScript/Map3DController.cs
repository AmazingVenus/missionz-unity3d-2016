﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Map3DController : MonoBehaviour {

    private Map3DView view;
    // Use this for initialization
    public int CurrentAnimal;
    //public Vector3 pos;
    void Awake() {
        view = GetComponent<Map3DView>();
    }
    void Start () {
        
        
    }
	
	// Update is called once per frame
	void Update () {
        view.PlayerPin.transform.position = Vector3.Lerp(view.PlayerPin.transform.position, view.animals[CurrentAnimal].FrontPosition.transform.position, 5f*Time.deltaTime);
        //pos = view.animals[CurrentAnimal].FrontPosition.transform.position;
    }
}
