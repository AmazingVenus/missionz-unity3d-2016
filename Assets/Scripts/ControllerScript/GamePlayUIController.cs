﻿using UnityEngine;
using System.Collections;

public class GamePlayUIController : MonoBehaviour {
	private GameplayUIView view;
	// Use this for initialization
	void Start () {
		view = this.GetComponent<GameplayUIView> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (view.AnimalHPBar.value <= 0) {
			view.CompleteDialog.SetActive(true);
		}
	}

	public void hitAnimal(float damage){
		view.AnimalHPBar.value -= damage;
	}
}
