﻿using UnityEngine;
using UnityEngine.UI;

public class CollectionController : MonoBehaviour
{
    public Text[] animalCount = new Text[6];

    public Image[] animalImage = new Image[6];

    public Sprite[] animalCatched = new Sprite[6];

    public Sprite emptySprite;

    void Start()
    {
        
        SetCountText();
    }


    void Update()
    {
        SetCountText();
    }

    void SetCountText()
    {

        int[] count = new int[] {
            PlayerPrefs.GetInt(CollectionPlayerPrefs.RACCOON_KEY, 0),
            PlayerPrefs.GetInt(CollectionPlayerPrefs.RABBIT_KEY, 0),
            PlayerPrefs.GetInt(CollectionPlayerPrefs.BEAR_KEY, 0),
            PlayerPrefs.GetInt(CollectionPlayerPrefs.ELEPHANT_KEY, 0),
            PlayerPrefs.GetInt(CollectionPlayerPrefs.MONKEY_KEY, 0),
            PlayerPrefs.GetInt(CollectionPlayerPrefs.LION_KEY, 0)

        };

        for (int i = 0; i < 6; i++)
        {
            if (count[i] == 0)
            {
                animalCount[i].text = "Never Catch";
                animalImage[i].sprite = emptySprite;
            }
            else
            {
                animalCount[i].text = "x " + count[i];
                animalImage[i].sprite = animalCatched[i];
            }
        }


    }

}
