﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PanelUI : GameUI {
	public Text titleLable;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		titleLable.text = this.title;
		GetComponent<Image> ().color = this.panelColor;
	}
}
