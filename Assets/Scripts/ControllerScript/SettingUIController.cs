﻿using UnityEngine;
using System.Collections;

public class SettingUIController : MonoBehaviour {
    SettingUIView view;
	// Use this for initialization
	void Start () {
        view = GetComponent<SettingUIView>();
	}
	
	// Update is called once per frame
	void Update () {
        view.PlayerName.text = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_NAME_KEY);
        //view.PlayerImage.sprite = GameObject.FindGameObjectWithTag("Facebook").GetComponent<FBHolder>().ProfileImage;
    }
	public void SaveWeight(EditSingleValueDialog dialog){
		DataCenterModel.instance.playerMng.Weight = int.Parse(dialog.ValueHolder.text);
	}
	
	public void SaveHeight(EditSingleValueDialog dialog){
		DataCenterModel.instance.playerMng.Height = int.Parse(dialog.ValueHolder.text);
	}

    public void clearAllData() {
        PlayerPrefs.DeleteAll();
    }
}
