﻿using UnityEngine;
using System.Collections;

public class InputWHController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		this.GetComponent<InputWHView>().WeightLable.text = ((int)DataCenterModel.instance.playerMng.Weight).ToString();
		this.GetComponent<InputWHView>().HeightLable.text = ((int)DataCenterModel.instance.playerMng.Height).ToString();
	}

	public void increaseHeight(){
		DataCenterModel.instance.playerMng.Height += 1;
	}

	public void decreaseHeight(){
		DataCenterModel.instance.playerMng.Height -= 1;
	}

	public void increaseWeight(){
		DataCenterModel.instance.playerMng.Weight += 1;
	}
	public void decreaseWeight(){
		DataCenterModel.instance.playerMng.Weight -= 1;
	}

	public void saveData(){
		DataCenterModel.instance.SaveDataToPlayerPref (MePlayerPrefs.WEIGHT_KEY, DataCenterModel.instance.playerMng.Weight);
		DataCenterModel.instance.SaveDataToPlayerPref (MePlayerPrefs.HEIGHT_KEY, DataCenterModel.instance.playerMng.Height);
	}
}
