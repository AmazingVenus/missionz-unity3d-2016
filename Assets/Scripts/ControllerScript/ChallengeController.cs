﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ChallengeController : MonoBehaviour
{
    string facebookId;

    public Text[] challengeProgress = new Text[3];
    public Text[] lvChallenge = new Text[3];
    public Button[] acceptButton = new Button[3];
    public GameObject rewardDialog;
    public Text scoreText;

    public Color inactiveColor;
    public Color activeColor;

    public Sprite[] challengeIcon = new Sprite[3];

    public GameObject CompleteTabPanel;
    public GameObject ChallengeEntry;
    private GameObject newCloneChallenge;

    int currentDistance;
    int currentBurned;
    int currentMoney;
    int currentHunter;

    int targetDistance;
    int targetBurned;
    int targetMoney;
    int targetHunter;

    int lvDistance;
    int lvBurned;
    int lvMoney;
    int lvHunter;

    int challengeActive;

    public Button[] completeButton = new Button[3];
    public Button[] abortChallengeButton = new Button[3];
    public GameObject acceptChallengeDialog;
    public GameObject abortChallengeDialog;
    int numberChallengeAcceptClicked = -1;
    int numberChallengeAbortClicked = -1;

    public Text descTextAccept;
    public Text titleTextAccept;
    public Text titleTextAbort;


    void Start()
    {

        facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY);
        InitialPlayerPrefs();
        SetProgressTextChallenge();
        SetLvTextChallenge();
        SetActiveButton();

        ShowCompleteChallenge();


       
    }

    void Update()
    {
        InitialPlayerPrefs();
        SetProgressTextChallenge();
        SetLvTextChallenge();
        SetActiveButton();
    }

    public string AddSuffixInProgressText(string challengeName)
    {
        switch (challengeName)
        {
            case "Burn Calories": return "kcal";
            case "Rich Man": return "coin";
            case "Hunter": return "animal";
            default: return "";
        }
    }

    public void ShowCompleteChallenge()
    {
        if (GameObject.FindGameObjectsWithTag("ChallengeEntry") != null)
        { 
              GameObject[] oldEntry = GameObject.FindGameObjectsWithTag("ChallengeEntry");
              foreach (GameObject entry in oldEntry)
              {
                    Destroy(entry);
              }

        }
        //get complete challenge from json
        List<Challenge> tempChallenge = new List<Challenge>();
        tempChallenge = JsonConvert.DeserializeObject<List<Challenge>>(File.ReadAllText(ChallengeManager.jsonPath));
        for (int i = 0; i < tempChallenge.Count; i++)
        {
            
            newCloneChallenge = Instantiate(ChallengeEntry) as GameObject; //copy
            newCloneChallenge.transform.SetParent(CompleteTabPanel.transform); //move to panel
            newCloneChallenge.transform.localScale = new Vector3(1, 1, 1);
            newCloneChallenge.transform.Find("ChallengeNameText").GetComponent<Text>().text = tempChallenge[i].challengeName + "Lv." + tempChallenge[i].challengeLevel; 
            newCloneChallenge.transform.Find("ChallengeProgressText").GetComponent<Text>().text = tempChallenge[i].challengeTarget + " " + AddSuffixInProgressText(tempChallenge[i].challengeName);

            newCloneChallenge.transform.Find("ImageChallenge").GetComponent<Image>().sprite = getIconChallenge(tempChallenge[i].challengeName);

        }

    }

    private Sprite getIconChallenge(string challengeName)
    {
        //ต้องคงสภาพไว้เพราะใช้ชื่อ Challenge ตัดสิน

        switch (challengeName)
        {
            case "Burn Calories": return challengeIcon[0];
            case "Rich Man": return challengeIcon[1];
            case "Hunter": return challengeIcon[2];
            default: return challengeIcon[0];
        }

    }

    public void SetTextTabInactive(Text textClickedButton)
    {
        textClickedButton.fontStyle = FontStyle.Normal;
        textClickedButton.color = inactiveColor;
    }
    public void SetTextTabActive(Text textClickedButton)
    {
        textClickedButton.fontStyle = FontStyle.Bold;
        textClickedButton.color = activeColor;
    }




    public void SetActiveButton()
    {

        if (challengeActive == -1)
        {
            //open all button
            acceptButton[0].interactable = true;
            acceptButton[0].GetComponent<Image>().color = activeColor;
            acceptButton[1].interactable = true;
            acceptButton[1].GetComponent<Image>().color = activeColor;
            acceptButton[2].interactable = true;
            acceptButton[2].GetComponent<Image>().color = activeColor;

            acceptButton[0].gameObject.SetActive(true);
            acceptButton[1].gameObject.SetActive(true);
            acceptButton[2].gameObject.SetActive(true);

            SetActiveCompleteButton(-1);
        }
        else if (challengeActive == 0)
        {
            acceptButton[1].interactable = false;
            acceptButton[1].GetComponent<Image>().color = inactiveColor;
            acceptButton[2].interactable = false;
            acceptButton[2].GetComponent<Image>().color = inactiveColor;

            acceptButton[0].gameObject.SetActive(false);
            acceptButton[1].gameObject.SetActive(true);
            acceptButton[2].gameObject.SetActive(true);

            SetActiveCompleteButton(0);
        }
        else if (challengeActive == 1)
        {
            acceptButton[0].interactable = false;
            acceptButton[0].GetComponent<Image>().color = inactiveColor;
            acceptButton[2].interactable = false;
            acceptButton[2].GetComponent<Image>().color = inactiveColor;

            acceptButton[0].gameObject.SetActive(true);
            acceptButton[1].gameObject.SetActive(false);
            acceptButton[2].gameObject.SetActive(true);

            SetActiveCompleteButton(1);
        }
        else if (challengeActive == 2) {
            acceptButton[0].interactable = false;
            acceptButton[0].GetComponent<Image>().color = inactiveColor;
            acceptButton[1].interactable = false;
            acceptButton[1].GetComponent<Image>().color = inactiveColor;

            acceptButton[0].gameObject.SetActive(true);
            acceptButton[1].gameObject.SetActive(true);
            acceptButton[2].gameObject.SetActive(false);

            SetActiveCompleteButton(2);
        }

    }


    public void SetActiveCompleteButton(int challengeActive) 
    {
        if (challengeActive == -1)
        {
            completeButton[0].gameObject.SetActive(false);
            completeButton[1].gameObject.SetActive(false);
            completeButton[2].gameObject.SetActive(false);

            abortChallengeButton[0].gameObject.SetActive(false);
            abortChallengeButton[1].gameObject.SetActive(false);
            abortChallengeButton[2].gameObject.SetActive(false);
        }
        else if (challengeActive == 0)
        {
            if (currentBurned >= targetBurned)
            {
                completeButton[0].interactable = true;
                completeButton[0].GetComponent<Image>().color = activeColor;
            }
            else {
                completeButton[0].interactable = false;
                completeButton[0].GetComponent<Image>().color = inactiveColor;
            }
            completeButton[0].gameObject.SetActive(true);
            completeButton[1].gameObject.SetActive(false);
            completeButton[2].gameObject.SetActive(false);

            abortChallengeButton[0].gameObject.SetActive(true);
            abortChallengeButton[1].gameObject.SetActive(false);
            abortChallengeButton[2].gameObject.SetActive(false);
        }
        else if (challengeActive == 1)
        {
            if (currentMoney >= targetMoney)
            {
                completeButton[1].interactable = true;
                completeButton[1].GetComponent<Image>().color = activeColor;
            }
            else
            {
                completeButton[1].interactable = false;
                completeButton[1].GetComponent<Image>().color = inactiveColor;
            }

            completeButton[0].gameObject.SetActive(false);
            completeButton[1].gameObject.SetActive(true);
            completeButton[2].gameObject.SetActive(false);

            abortChallengeButton[0].gameObject.SetActive(false);
            abortChallengeButton[1].gameObject.SetActive(true);
            abortChallengeButton[2].gameObject.SetActive(false);
        }
        else if (challengeActive == 2) {
            if (currentHunter >= targetHunter)
            {
                completeButton[2].interactable = true;
                completeButton[2].GetComponent<Image>().color = activeColor;
            }
            else
            {
                completeButton[2].interactable = false;
                completeButton[2].GetComponent<Image>().color = inactiveColor;
            }

            completeButton[0].gameObject.SetActive(false);
            completeButton[1].gameObject.SetActive(false);
            completeButton[2].gameObject.SetActive(true);

            abortChallengeButton[0].gameObject.SetActive(false);
            abortChallengeButton[1].gameObject.SetActive(false);
            abortChallengeButton[2].gameObject.SetActive(true);
        }
    }


    public void OpenRewardDialog()
    {
        rewardDialog.SetActive(true);
    }

    public void OnClickCloseRewardDialog()
    {
        rewardDialog.SetActive(false);
    }

    public void OnClickAcceptChallenge() {
        if (numberChallengeAcceptClicked == 0)
        {
            challengeActive = 0;
        }
        else if (numberChallengeAcceptClicked == 1)
        {
            challengeActive = 1;
        }
        else if (numberChallengeAcceptClicked == 2) {
            challengeActive = 2;
        }
        PlayerPrefs.SetInt(ChallengePlayerPrefs.CHALLENGE_ACTIVE_KEY, challengeActive);
        PlayerPrefs.Save();

        SetActiveButton();
        numberChallengeAcceptClicked = -1; //reset
    }

    public void OpenAcceptChallengeDialog(int challengeNumber) {
        numberChallengeAcceptClicked = challengeNumber;
        if (challengeNumber == 0) {
            descTextAccept.text = "-Target: "+targetBurned+" kcal\n- Due Time: 23:59";
            titleTextAccept.text = "Burned Calories";
        }
        else if (challengeNumber == 1)
        {
            descTextAccept.text = "-Target: "+targetMoney+" coin\n- Due Time: 23:59";
            titleTextAccept.text = "Rich Man";
        }
        else if (challengeNumber == 2)
        {
            descTextAccept.text = "-Target: " + targetHunter + " animal\n- Due Time: 23:59";
            titleTextAccept.text = "Hunter";
        }

        acceptChallengeDialog.SetActive(true);
    }

    public void OpenAbortChallengeDialog(int button) {
        if (button == 0)
        {
            numberChallengeAbortClicked = 0;
            titleTextAbort.text = "Burn Calories";
        }
        else if (button == 1)
        {
            numberChallengeAbortClicked = 1;
            titleTextAbort.text = "Rich Man";
        }
        else if (button == 2)
        {
            numberChallengeAbortClicked = 2;
            titleTextAbort.text = "Hunter";
        }

        abortChallengeDialog.SetActive(true);

    }

    public void OnClickAbortChallenge() {
        if (numberChallengeAbortClicked == 0)
        {
            PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_BURNED_CHALLENGE_KEY, 0);
        }
        else if (numberChallengeAbortClicked == 1)
        {
            PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_MONEY_CHALLENGE_KEY, 0);
        }
        else if (numberChallengeAbortClicked == 2) {
            PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_HUNTER_CHALLENGE_KEY, 0);
        }

        PlayerPrefs.SetInt(ChallengePlayerPrefs.CHALLENGE_ACTIVE_KEY, -1);
        PlayerPrefs.Save();
        InitialPlayerPrefs();
        SetActiveButton();

    }

 

    public void OnClickCompleteChallenge(int identifyButton)
    {
        string challengeName="";
        int currentScore = DataCenterModel.instance.playerMng.Score;
        int upScore = 0;

        Challenge completeChallenge;

        if (identifyButton == 0)
        {
            upScore = ChallengeManager.CalcScoreChallenge(0, targetBurned);

            challengeName = ChallengeManager.ChallengeBurnName;

            PlayerPrefs.SetInt(ChallengePlayerPrefs.LEVEL_BURNED_CHALLENGE_KEY, lvBurned + 1);
            PlayerPrefs.SetInt(ChallengePlayerPrefs.TARGET_BURNED_CHALLENGE_KEY, (int)1.5 * ChallengeManager.CalcAverageBurned());
            PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_BURNED_CHALLENGE_KEY, 0);
           completeChallenge = new Challenge(facebookId, challengeName, lvBurned, targetBurned, upScore, DateTime.Now.ToLocalTime());
            ChallengeManager.saveCompleteChallengeToJson(completeChallenge);
        }
        else if (identifyButton == 1)
        {
            
            upScore = ChallengeManager.CalcScoreChallenge(1, targetMoney);

            challengeName = ChallengeManager.ChallengeMoneyName;

            PlayerPrefs.SetInt(ChallengePlayerPrefs.LEVEL_MONEY_CHALLENGE_KEY, lvMoney + 1);
            PlayerPrefs.SetInt(ChallengePlayerPrefs.TARGET_MONEY_CHALLENGE_KEY, 2 * targetMoney);
            PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_MONEY_CHALLENGE_KEY, 0);
            completeChallenge = new Challenge(facebookId, challengeName, lvMoney, targetMoney, upScore, DateTime.Now.ToLocalTime());
            ChallengeManager.saveCompleteChallengeToJson(completeChallenge);
        }
        else if (identifyButton == 2)
        {

            upScore = ChallengeManager.CalcScoreChallenge(2, targetHunter);

            challengeName = ChallengeManager.ChallengeHunterName;

            PlayerPrefs.SetInt(ChallengePlayerPrefs.LEVEL_HUNTER_CHALLENGE_KEY,lvHunter + 1);
            PlayerPrefs.SetInt(ChallengePlayerPrefs.TARGET_HUNTER_CHALLENGE_KEY, 2 * targetHunter);
            PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_HUNTER_CHALLENGE_KEY, 0);
            completeChallenge = new Challenge(facebookId, challengeName, lvHunter, targetHunter, upScore, DateTime.Now.ToLocalTime());
            ChallengeManager.saveCompleteChallengeToJson(completeChallenge);
        }

        DataCenterModel.instance.playerMng.ScoreRecord += upScore;
        PlayerPrefs.SetInt(ChallengePlayerPrefs.CHALLENGE_ACTIVE_KEY, -1);
        PlayerPrefs.Save();

        DataCenterModel.instance.playerMng.Score = currentScore+upScore;




        SetActiveButton();
        SetProgressTextChallenge();
        
        ShowCompleteChallenge();

        //show dialog reward
        scoreText.text = "+ " + upScore;
        OpenRewardDialog();

        InitialPlayerPrefs();
        SetLvTextChallenge();

    }


    void InitialPlayerPrefs()
    {
     
        lvBurned = PlayerPrefs.GetInt(ChallengePlayerPrefs.LEVEL_BURNED_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_LEVEL_CHALLENGE_KEY);
        lvMoney = PlayerPrefs.GetInt(ChallengePlayerPrefs.LEVEL_MONEY_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_LEVEL_CHALLENGE_KEY);
        lvHunter = PlayerPrefs.GetInt(ChallengePlayerPrefs.LEVEL_HUNTER_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_LEVEL_CHALLENGE_KEY);

     
        currentBurned = PlayerPrefs.GetInt(ChallengePlayerPrefs.CURRENT_BURNED_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_CURRENT_CHALLENGE_KEY);
        currentMoney = PlayerPrefs.GetInt(ChallengePlayerPrefs.CURRENT_MONEY_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_CURRENT_CHALLENGE_KEY);
        currentHunter = PlayerPrefs.GetInt(ChallengePlayerPrefs.CURRENT_HUNTER_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_CURRENT_CHALLENGE_KEY);
        
        targetBurned = PlayerPrefs.GetInt(ChallengePlayerPrefs.TARGET_BURNED_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_TARGET_BURNED_CHALLENGE_KEY);
        targetMoney = PlayerPrefs.GetInt(ChallengePlayerPrefs.TARGET_MONEY_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_TARGET_MONEY_CHALLENGE_KEY);
        targetHunter = PlayerPrefs.GetInt(ChallengePlayerPrefs.TARGET_HUNTER_CHALLENGE_KEY, ChallengePlayerPrefs.DEFAULT_TARGET_HUNTER_CHALLENGE_KEY);

        challengeActive = PlayerPrefs.GetInt(ChallengePlayerPrefs.CHALLENGE_ACTIVE_KEY, ChallengePlayerPrefs.DEFAULT_CHALLENGE_ACTIVE_KEY);
    }

    void SetProgressTextChallenge()
    {
        challengeProgress[0].text = currentBurned + " / " + targetBurned + " " + AddSuffixInProgressText("Burn Calories");
        challengeProgress[1].text = currentMoney + " / " + targetMoney + " " + AddSuffixInProgressText("Rich Man");
        challengeProgress[2].text = currentHunter + " / " + targetHunter + " " + AddSuffixInProgressText("Hunter");
    }

    void SetLvTextChallenge()
    {
        lvChallenge[0].text = "" + lvBurned;
        lvChallenge[1].text = "" + lvMoney;
        lvChallenge[2].text = "" + lvHunter;
    }
    
}
