﻿using UnityEngine;
using System.Collections;

public class GameUI : MonoBehaviour {
	public string title;
	public Color panelColor;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void setActiveScene(GameObject scenes){
		scenes.SetActive (true);
		this.gameObject.SetActive (false);
		SceneManagerModel.instance.scenes.Add (scenes);
	}
	public void backScene(){
		SceneManagerModel.instance.currentScene.SetActive (false);
		SceneManagerModel.instance.previousScene.SetActive (true);
		SceneManagerModel.instance.scenes.RemoveAt (SceneManagerModel.instance.count-1);
	}

	public void ShowDialog(GameDialog dialog){
		dialog.Show ();
	}
}
