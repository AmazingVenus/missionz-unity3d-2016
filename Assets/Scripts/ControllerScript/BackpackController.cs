﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BackpackController : MonoBehaviour
{
    public Text[] amountText = new Text[3];
    public Text moneyText;
    public Button[] useButton = new Button[2];
   

    public Color inactiveColor;
    public Color activeColor;

    public GameObject showDialog;

    void Start()
    {
        SetMoneyText();
        SetEnableUseButton();
        SetTextAmountItem();
    }

    void Update()
    {
        SetTextAmountItem();
        SetMoneyText();
        SetEnableUseButton();
    }


    void SetEnableUseButton()
    {
        int amountHealthPotion = PlayerPrefs.GetInt(BackpackPlayerPrefs.HEALTH_POTION_KEY, BackpackPlayerPrefs.DEFAULT_HEALTH_POTION_KEY);
        int amountEnergyPotion = PlayerPrefs.GetInt(BackpackPlayerPrefs.ENERGY_POTION_KEY, BackpackPlayerPrefs.DEFAULT_ENERGY_POTION_KEY);

        //int money = PlayerPrefs.GetInt(BackpackPlayerPrefs.moneyKey, 300);

        if (amountHealthPotion < 1)
        {

            useButton[0].interactable = false;
            useButton[0].GetComponent<Image>().color = inactiveColor;
        }
        else
        {
            useButton[0].interactable = true;
            useButton[0].GetComponent<Image>().color = activeColor;
        }

        if (amountEnergyPotion < 1)
        {
            useButton[1].interactable = false;
            useButton[1].GetComponent<Image>().color = inactiveColor;
        }
        else
        {
            useButton[1].interactable = true;
            useButton[1].GetComponent<Image>().color = activeColor;
        }


    }

    void SetTextAmountItem()
    {
        amountText[0].text = "x " + PlayerPrefs.GetInt(BackpackPlayerPrefs.HEALTH_POTION_KEY, BackpackPlayerPrefs.DEFAULT_HEALTH_POTION_KEY);
        amountText[1].text = "x " + PlayerPrefs.GetInt(BackpackPlayerPrefs.ENERGY_POTION_KEY, BackpackPlayerPrefs.DEFAULT_ENERGY_POTION_KEY);
        amountText[2].text = "x " + PlayerPrefs.GetInt(BackpackPlayerPrefs.SLEEP_DART_KEY, BackpackPlayerPrefs.DEFAULT_SLEEP_DART_KEY);
    }

    void SetMoneyText()
    {
        moneyText.text = "" + PlayerPrefs.GetInt(BackpackPlayerPrefs.MONEY_KEY, BackpackPlayerPrefs.DEFAULT_MONEY_KEY);
    }

    public void OnClickUseButton(int i)
    {
        if (i == 0)
        {
            IncreaseHealthPower();
        }
        else if (i == 1)
        {
            IncreaseEnergyPower();
        }
        SetEnableUseButton();
        //CloseBackpackPanel();
        //must close backpack panel

    }

    void IncreaseHealthPower()
    {

        //ฟื้นฟูพลัง
        int currentHP = (int)DataCenterModel.instance.playerMng.CurrentHP;
        int fullHP = (int)DataCenterModel.instance.playerMng.MaxHP;

        showDialog.SetActive(true);
        showDialog.GetComponent<ShowConsumeResultScript>().title = "HP";
        showDialog.GetComponent<ShowConsumeResultScript>().sliderColor = new Color(0.914f, 0.118f, 0.388f);
        showDialog.GetComponent<ShowConsumeResultScript>().iconIndex = 0;
        showDialog.GetComponent<ShowConsumeResultScript>().currentProgress.x = currentHP;
        showDialog.GetComponent<ShowConsumeResultScript>().afterValue = (int)(currentHP + fullHP * 0.3);
        showDialog.GetComponent<ShowConsumeResultScript>().slider.maxValue = fullHP;
        showDialog.GetComponent<ShowConsumeResultScript>().slider.minValue = 0;
        showDialog.GetComponent<ShowConsumeResultScript>().isShow = true;

        DataCenterModel.instance.playerMng.CurrentHP = Mathf.CeilToInt(currentHP + fullHP * 0.3f); //30% restore

        //ลดจำนวนของลง
        int currentAmountItem = PlayerPrefs.GetInt(BackpackPlayerPrefs.HEALTH_POTION_KEY, BackpackPlayerPrefs.DEFAULT_HEALTH_POTION_KEY) - 1;
        PlayerPrefs.SetInt(BackpackPlayerPrefs.HEALTH_POTION_KEY, currentAmountItem);
        PlayerPrefs.Save();



    }

    void IncreaseEnergyPower()
    {

        //ฟื้นฟูพลัง
        int currentEnergy = (int)DataCenterModel.instance.playerMng.CurrentEnergy;
        int newCurrentEnergy = currentEnergy + 30; //30% restore

        showDialog.SetActive(true);
        showDialog.GetComponent<ShowConsumeResultScript>().title = "Energy";
        showDialog.GetComponent<ShowConsumeResultScript>().sliderColor = new Color(1f, 0.596f, 0f);
        showDialog.GetComponent<ShowConsumeResultScript>().iconIndex = 1;
        showDialog.GetComponent<ShowConsumeResultScript>().currentProgress.x = currentEnergy;
        showDialog.GetComponent<ShowConsumeResultScript>().afterValue = newCurrentEnergy;
        showDialog.GetComponent<ShowConsumeResultScript>().slider.maxValue = 100;
        showDialog.GetComponent<ShowConsumeResultScript>().slider.minValue = 0;
        showDialog.GetComponent<ShowConsumeResultScript>().isShow = true;

        DataCenterModel.instance.playerMng.CurrentEnergy = newCurrentEnergy;

        //ลดจำนวนของลง
        int currentAmountItem = PlayerPrefs.GetInt(BackpackPlayerPrefs.ENERGY_POTION_KEY, BackpackPlayerPrefs.DEFAULT_ENERGY_POTION_KEY) - 1;
        PlayerPrefs.SetInt(BackpackPlayerPrefs.ENERGY_POTION_KEY, currentAmountItem);
        PlayerPrefs.Save();


        
    }

  

    void SetTextMoney()
    {
        moneyText.text = "" + DataCenterModel.instance.playerMng.Money;
    }
}
