﻿using UnityEngine;
using System.Collections;
using Parse;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using UnityEngine.UI;

public class LoadDataFromParseController : MonoBehaviour
{
    public LoadActivityData activityData;
    public LoadBackpackData backpackData;
    public LoadChallengData challengData;
    public LoadCollectionData collectionData;
    public LoadMeDataFromParse meData;
    public LoadChallengeData2 challengeData2;

    public bool isActivityFinishRestore;
    public bool isChallengeCompleteFinishRestore;
    public bool isChallengeRestoreFinish;
    public bool isCollectionFinishRestore;
    public bool isMeFinishRestore;
    public bool isBackpackFinishRestore;

    public int progress;
    public Text statusText;

    private bool isLoaded;
    void Start()
    {

    }

    void Update()
    {
        /*
        isActivityFinishRestore = activityData.isActivityFinished;
        isChallengeCompleteFinishRestore = challengData.isChallengeCompleteFinished;
        isCollectionFinishRestore = collectionData.isCollectionFinished;
        isMeFinishRestore = meData.isMeFinished;
        isBackpackFinishRestore = backpackData.isBackpackFinished;
        */

        if (!isActivityFinishRestore && activityData.isDone) {
            //progress++;
            RestoreActivity();
            
        }
        if (!isChallengeCompleteFinishRestore && challengData.isDone) {
            //progress++;
            RestoreChallengeComplete();
            
        }
        if (!isCollectionFinishRestore && collectionData.isDone) {
            //progress++;
            RestoreCollection();
        }
        if (!isMeFinishRestore && meData.isDone) {
            //progress++;
            RestoreMe();
        }
        if (!isBackpackFinishRestore && backpackData.isDone) {
            ///progress++;
            RestoreBackpack();
        }
        if (!isChallengeRestoreFinish && challengeData2.isDone)
        {
            ///progress++;
            RestoreChallenge();
        }


        if (progress == 6 && !isLoaded)
        {
            statusText.text = "Load Complete " + progress + "/6";
            Debug.Log("Start Load sceneeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
            isLoaded = true;
            Application.LoadLevel(2);


        }
        else {
            statusText.text = "Loading player data..." + progress + "/6";
        }

        
    }

    public IEnumerator delay(int second) {
        yield return new WaitForSeconds(second);
        Application.LoadLevel("main");
    }

    private void RestoreActivity()
    {
            string output;
            if (activityData.activityFromParse.Count == 0)
            {
                output = "[]";
            }
            else
            {
               output = JsonConvert.SerializeObject(activityData.activityFromParse);
            }
            File.WriteAllText(ActivityManager.jsonPath, output);
            isActivityFinishRestore = true;
        progress++;
        Debug.Log("activity Restore Finish");
    }

    private void RestoreChallengeComplete()
    {
            string output;
            if (challengData.challengeFromParse.Count == 0) {
                output = "[]";
            }
            else {
                output = JsonConvert.SerializeObject(challengData.challengeFromParse);
               
            }

            File.WriteAllText(ChallengeManager.jsonPath, output);
        progress++;
        Debug.Log("Challenge c Restore Finish");
            isChallengeCompleteFinishRestore = true;
    }

    private void RestoreMe()
    {
   
            Debug.Log("a.MeDistance " + meData.MeDistance);
            PlayerPrefs.SetInt(MePlayerPrefs.HEALTH_CURRENT_KEY, meData.currentHP);
            PlayerPrefs.SetInt(MePlayerPrefs.BURNED_RECORD_KEY, meData.MeBurnRecord);
            PlayerPrefs.SetInt(MePlayerPrefs.BURNED_KEY, meData.MeBurn);
            PlayerPrefs.SetInt(MePlayerPrefs.DISTANCE_RECORD_KEY, meData.MeDISTANCE_RECORD_KEY);
            PlayerPrefs.SetInt(MePlayerPrefs.DISTANCE_TRAVELLED_KEY, meData.MeDistance);
            PlayerPrefs.SetInt(MePlayerPrefs.ENERGY_MAX_KEY, 100);
            PlayerPrefs.SetInt(MePlayerPrefs.EXP_CURRENT_KEY, meData.expCurrent);
            PlayerPrefs.SetInt(MePlayerPrefs.EXP_MAX_KEY, meData.expMax);
            PlayerPrefs.SetInt(MePlayerPrefs.HEALTH_MAX_KEY, meData.healthFull);
            PlayerPrefs.SetInt(MePlayerPrefs.LEVEL_KEY, meData.MeLevel);
            PlayerPrefs.SetInt(MePlayerPrefs.SCORE_KEY, meData.MeScore);
            PlayerPrefs.SetInt(MePlayerPrefs.SCORE_RECORD_KEY, meData.MeScore);
            PlayerPrefs.Save();

            isMeFinishRestore = true;
        progress++;
        Debug.Log("Me Restore Finish");
    }

    private void RestoreCollection()
    {

            PlayerPrefs.SetInt(CollectionPlayerPrefs.BEAR_KEY, collectionData.bear);
            PlayerPrefs.SetInt(CollectionPlayerPrefs.LION_KEY, collectionData.lion);
            PlayerPrefs.SetInt(CollectionPlayerPrefs.RACCOON_KEY, collectionData.raccoon);
            PlayerPrefs.SetInt(CollectionPlayerPrefs.RABBIT_KEY, collectionData.rabbit);
            PlayerPrefs.SetInt(CollectionPlayerPrefs.MONKEY_KEY, collectionData.monkey);
            PlayerPrefs.SetInt(CollectionPlayerPrefs.ELEPHANT_KEY, collectionData.elephant);
            PlayerPrefs.Save();

            Debug.Log("Collection Restore Finish");
        progress++;
        isCollectionFinishRestore = true;
    }

    private void RestoreBackpack()
    {
            PlayerPrefs.SetInt(BackpackPlayerPrefs.HEALTH_POTION_KEY, backpackData.healthPotion);
            PlayerPrefs.SetInt(BackpackPlayerPrefs.ENERGY_POTION_KEY, backpackData.energyPotion);
            PlayerPrefs.SetInt(BackpackPlayerPrefs.SLEEP_DART_KEY, backpackData.sleepingDart);
            PlayerPrefs.SetInt(BackpackPlayerPrefs.MONEY_KEY, backpackData.money);
            PlayerPrefs.Save();

            Debug.Log("Backpack Restore Finish");
        progress++;
        isBackpackFinishRestore = true;
    }

    private void RestoreChallenge()
    {
       
        PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_BURNED_CHALLENGE_KEY, challengeData2.currentBurned);
        PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_MONEY_CHALLENGE_KEY, challengeData2.currentMoney);
        PlayerPrefs.SetInt(ChallengePlayerPrefs.CURRENT_HUNTER_CHALLENGE_KEY, challengeData2.currentHunter);

        Debug.Log("challengeData2.targetMoney " + challengeData2.targetMoney);
        PlayerPrefs.SetInt(ChallengePlayerPrefs.TARGET_BURNED_CHALLENGE_KEY, challengeData2.targetBurned);
        PlayerPrefs.SetInt(ChallengePlayerPrefs.TARGET_MONEY_CHALLENGE_KEY, challengeData2.targetMoney);
        PlayerPrefs.SetInt(ChallengePlayerPrefs.TARGET_HUNTER_CHALLENGE_KEY, challengeData2.targetHunter);

        
        PlayerPrefs.SetInt(ChallengePlayerPrefs.LEVEL_BURNED_CHALLENGE_KEY, challengeData2.levelBurned);
        PlayerPrefs.SetInt(ChallengePlayerPrefs.LEVEL_MONEY_CHALLENGE_KEY, challengeData2.levelMoney);
        PlayerPrefs.SetInt(ChallengePlayerPrefs.LEVEL_HUNTER_CHALLENGE_KEY, challengeData2.levelHunter);
        PlayerPrefs.Save();

        Debug.Log("Challenge2 Restore Finish");
        progress++;
        isChallengeRestoreFinish = true;
    }

}



