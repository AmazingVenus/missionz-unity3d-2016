﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameDialog : MonoBehaviour {
	public string Title;
	public Text TitleHolder;
	public Button CloseButton;
	public GameObject DismissArea;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		TitleHolder.text = Title;
	}

	public void Close(){
		this.gameObject.SetActive(false);
	}

	public void Show(){
		this.gameObject.SetActive(true);
	}


}
