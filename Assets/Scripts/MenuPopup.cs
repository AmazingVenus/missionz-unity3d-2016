﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuPopup : MonoBehaviour {
	public GameObject clicked_animal;
	public GameObject dialog;
	public bool isClicked;
	public GameObject Map;
    public Button CatchButton;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Map.GetComponent<MapControl>().click_ID == Map.GetComponent<MapControl>().CurrentIndex){

        } else {

        }
    }

	public void showInfo(){
		isClicked = true;
		dialog.GetComponent<AnimalInfoDialog> ().animal = clicked_animal.GetComponent<Animal> ();
		dialog.GetComponent<AnimalInfoDialog> ().fadeIn ();
		GetComponent<ScalingEffect> ().Speed = 20;
		GetComponent<ScalingEffect> ().EndScale = Vector2.zero;
		GetComponent<ScalingEffect> ().play ();
		play ();
	}

	public void goThere(){

		Map.GetComponent<MapControl> ().click_ID = clicked_animal.GetComponent<Node> ().ID;
		Map.GetComponent<MapControl> ().isFindPath = true;
	}

	public void play(){
		StartCoroutine (Animate ());
	}
	
	public IEnumerator Animate(){
		yield return new WaitForSeconds(0.2f);
		isClicked = false;
	}
}
