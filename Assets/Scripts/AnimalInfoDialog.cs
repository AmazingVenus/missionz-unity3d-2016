﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AnimalInfoDialog : MonoBehaviour {
	public Animal animal;
    public Node node;
    public GameObject Map;
    public Text AnimalNameText;
	public Text AnimalLevelText;
	public Text AnimalDescText;
	public Text AnimalHPText;
	public Text AnimalDamageText;
	public Image AnimalImage;
    public bool isShow;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (animal != null) {
			AnimalNameText.text = animal.AnimalName;
			AnimalLevelText.text = "Level : "+animal.AnimalLevel.ToString ();
			AnimalDescText.text = animal.AnimalDescription;
			AnimalHPText.text = "Strength : "+animal.AnimalHP.ToString ();
			AnimalDamageText.text = "Damage : "+animal.AnimalDamage.ToString ();
			AnimalImage.sprite = animal.AnimalImage;
		} else {
			
		}
	}

	public void fadeOut(){
		this.GetComponent<TransitionEffect> ().EndPosition = new Vector2 (-277, 0);
		this.GetComponent<TransitionEffect> ().play ();
        isShow = false;
	}

	public void fadeIn(){
		this.GetComponent<TransitionEffect> ().EndPosition = new Vector2 (0, 0);
		this.GetComponent<TransitionEffect> ().play ();
        isShow = true;
	}

    public void goThere()
    {

        Map.GetComponent<MapControl>().click_ID = node.ID;
        Map.GetComponent<MapControl>().isFindPath = true;
    }
}
