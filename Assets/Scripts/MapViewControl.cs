﻿using UnityEngine;
using System.Collections;

public class MapViewControl : MonoBehaviour {

	public GameObject menu;
    

	public float mapX = 100.0f;
	public float mapY = 100.0f;
	public float minX;
	public float maxX;
	public float minY;
	public float maxY;

	public float fResistanceFactor = 0.98f;
	public float fStopThreashold = 0.01f;
		
	Plane planeHit;
	Vector3 v3StartPos;
	Vector3 v3LastPos;
	Vector3 v3Delta;
	float fStartTime;
		
	bool bTranslating = false;
		
	void OnMouseDown() {
		play();
			bTranslating = false;
			v3StartPos = GetHitPoint ();
			v3LastPos = v3StartPos;
			fStartTime = Time.time;
	}
		
	void OnMouseDrag() {
			Vector3 v3T = GetHitPoint ();
			transform.Translate(v3T - v3LastPos);
			v3LastPos = v3T;
	}
		
	void OnMouseUp() {

			v3Delta = GetHitPoint();
			v3Delta = (v3Delta - v3StartPos) / (Time.time - fStartTime) * Time.deltaTime;
			bTranslating = true;
	//	Debug.Log("Drag ended!");
	}
		
	Vector3 GetHitPoint() {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			float fDist;
			if (planeHit.Raycast (ray, out fDist))
				return ray.GetPoint (fDist);
			else 
				return Vector3.zero;
	}
		
	void Start() {
			planeHit =  new Plane(Vector3.forward, transform.position);
		float vertExtent = Camera.main.orthographicSize;    
		float horzExtent = vertExtent * Screen.width / Screen.height;
		
		// Calculations assume map is position at the origin
		minX = horzExtent - mapX / 2.0f;
		maxX = mapX / 2.0f - horzExtent;
		minY = vertExtent - mapY / 2.0f;
		maxY = mapY / 2.0f - vertExtent;


	}
		
	void Update () {
			if (bTranslating) {
				transform.position += v3Delta;
				v3Delta = v3Delta * fResistanceFactor;
				if (v3Delta.magnitude < fStopThreashold)
					bTranslating = false;
			}
	}
	void LateUpdate() {
		Vector3 v3 = transform.position;
		v3.x = Mathf.Clamp(v3.x, minX, maxX);
		v3.y = Mathf.Clamp(v3.y, minY, maxY);
		transform.position = v3;

	}

	void hideMenu(){
		menu.GetComponent<ScalingEffect> ().Speed = 20;
		menu.GetComponent<ScalingEffect> ().EndScale = Vector2.zero;
		menu.GetComponent<ScalingEffect> ().play ();
	}

	public void play(){
		StartCoroutine (Animate ());
	}
	
	public IEnumerator Animate(){
			yield return new WaitForSeconds(0.20f);
			hideMenu ();
	}
}
