﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ExpSlideBar : MonoBehaviour {
	public GameObject Fill1;
	public GameObject Fill2;

	private float valueFill1;
	private float valueFill2;

	private Slider slider;
	// Use this for initialization
	void Start () {
		slider = this.GetComponent<Slider> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void AddValue1(float number){
		slider.fillRect = Fill1.GetComponent<RectTransform>();
		valueFill1 += number;
		slider.value = valueFill1;
	}

	public void AddValue2(float number){
		slider.fillRect = Fill2.GetComponent<RectTransform>();
		valueFill2 += number;
		slider.value = valueFill2;
	}

	public void AddValue(){
		//StartCoroutine (ApplyValue ());
	}

/*	public IEnumerator ApplyValue(float number){
		yield return new WaitForSeconds(0.5f); 
		while (valueFill1 != valueFill2) {
			
		}
	}
	*/
}
