﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CrossfireScript : MonoBehaviour {
	public Vector3 AccelerometerVelue;
	public Vector3 CenterPosition;
	public Vector3 CurrentPosition;
	// Use this for initialization
	

	public Text AccText;
	public Text PosText;
	public Text CurrPosText;

	public bool onTarget;

	public GameObject animalTransform;
	public Vector3 animalPosition;
	public float distance;

	void Start () {
		CenterPosition = this.transform.position;
		StartCoroutine (FindAnimal ());
		

	}
	
	// Update is called once per frame
	void Update () {
        AccelerometerVelue = Input.acceleration;
		if (animalTransform != null) {
			animalPosition = animalTransform.transform.position;
			distance = Vector3.Distance (animalPosition, transform.position);
		}

		onTarget = checkOnTarget ();

		CurrentPosition.x = AccelerometerVelue.x*2.54f;
		CurrentPosition.y = AccelerometerVelue.z*5f;
		CurrentPosition.z = -1;




        this.transform.position = Vector3.Lerp (this.transform.position, CurrentPosition, 5f * Time.deltaTime);


		//AccText.text = "Acc Value "+AccelerometerVelue.ToString ();
		//PosText.text = "Current Position " + CenterPosition.ToString ();
		//CurrPosText.text = "Output Position " + this.transform.position.ToString ();
	}

	bool checkOnTarget(){
		if (distance <= 1.3f) {
			Debug.Log ("OnTarget");
			return true;
		} else {
			return false;
		}
	}

	public IEnumerator FindAnimal(){
		yield return new WaitForSeconds(0.1f);
		animalTransform = GameObject.FindGameObjectWithTag("Animal");
	}
}
