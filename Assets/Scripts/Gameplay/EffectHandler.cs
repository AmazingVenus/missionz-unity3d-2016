﻿using UnityEngine;
using System.Collections;

public class EffectHandler : MonoBehaviour {
    public ParticleSystem PlayerHeartFallParticle;
    // Use this for initialization
    void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    public void PlayerGetDamageEffect() {
        PlayerHeartFallParticle.Play();
    }
}
