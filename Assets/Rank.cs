﻿using UnityEngine;
using System.Collections;

public class Rank {
    
    public string name;
   public  int score;

    public Rank(string name, int score) {
        this.name = name;
        this.score = score;
    }

}
