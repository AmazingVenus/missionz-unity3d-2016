﻿using UnityEngine;
using System.Collections;

public static class CalorieBurnTracker {
	// Use this for initialization

    public static float CalculateCalorie(float speed) {
        float MET;
        float weight = PlayerPrefs.GetInt(MePlayerPrefs.WEIGHT_KEY); //DataCenterModel.instance.playerMng.Weight;
        float burned;
        MET = getMETValue(speed);
        burned = (MET * weight)/855;//(((MET * weight)/57)/60)*4;
        return burned;
    }

    public static float getMETValue(float speed) {
        if (speed >= 3f) {
            return 10.5f;
        }
        else if (speed >= 2.68f)
        {
            return 9.8f;
        }
        else if (speed >= 2.32f)
        {
            return 9;
        }
        else if (speed >= 2.24f)
        {
            return 8.3f;
        }
        else if (speed >= 1.78f)
        {
            return 6f;
        }
        else if (speed >= 1.56f)
        {
            return 4.8f;
        }
        else if (speed >= 1.25f)
        {
            return 3.5f;
        }
        else if (speed >= 1.12f)
        {
            return 3f;
        }
        else if (speed >= 0.89f)
        {
            return 2.8f;
        }
        else if (speed >= 0.66f)
        {
            return 2f;
        }
        else {
            return 0;
        }

    }
}
