﻿using UnityEngine;
using System.Collections;

public class HightButtonSlider : MonoBehaviour {
	public float spaceWidth;
	private Vector3 currentPosition;
	public float maxSpeed;
	public bool isIncrease;

	private Vector3 startPosition;
	private Vector3 endPosition;
	private bool isRelease;
	private float currentSpeed;

	// Use this for initialization
	void Start () {
		startPosition = this.transform.position;
		endPosition = startPosition + new Vector3 (spaceWidth, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {
		if (isRelease) {

			this.transform.position = Vector3.Lerp (this.transform.position, startPosition, 5f * Time.deltaTime);
		} else {

			//if(isIncrease){
				DataCenterModel.instance.playerMng.Height += Time.deltaTime * maxSpeed * currentSpeed;
		//	}else{
		//		DataCenterModel.instance.playerMng.Weight -= Time.deltaTime * maxSpeed * currentSpeed;

		//	}
		}
	}

	public void MouseDrag(){
		isRelease = false;
		//Debug.Log ("Down");
		if (isIncrease) {
			if (Input.mousePosition.x > endPosition.x) {
				currentPosition.x = endPosition.x;
			} else if (Input.mousePosition.x < startPosition.x) {
				currentPosition.x = startPosition.x;
			} else {
				currentPosition.x = Input.mousePosition.x;
			}
		} else {
			if (Input.mousePosition.x < endPosition.x) {
				currentPosition.x = endPosition.x;
			} else if (Input.mousePosition.x > startPosition.x) {
				currentPosition.x = startPosition.x;
			} else {
				currentPosition.x = Input.mousePosition.x;
			}
		}



		currentSpeed = Vector3.Distance (currentPosition, startPosition)/spaceWidth;
		Debug.Log (currentSpeed);
		currentPosition.y = this.transform.position.y;
		this.transform.position = currentPosition;
	}

	public void MouseRelease(){
		isRelease = true;
	}
}
