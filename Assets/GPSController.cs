﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GPSController : MonoBehaviour {

    private GPSPlugin gpsPlugin;

    private double[] PositionInfo;

    public Location L1 = new Location();
    public Location L2 = new Location();

    //private double D;
    private double d;
    private double Speed;
    //public Text speedText;
    public Text DistanceText;
    public Text DistanceRecordText;
    public Text speedText;

    
    void Awake() {
        
    }
    // Use this for initialization
    void Start()
    {
        //D = DataCenterModel.instance.playerMng.CurrentDistance;
        
       

        gpsPlugin = GPSPlugin.GetInstance();
        gpsPlugin.SetDebug(0);

        //long updateInterval = 1000 * 60 * 1; // 1 minute
        //long updateInterval = 1000 * 5; // 5 seconds
        long updateInterval = 5000; // 200 millsecs 

        //long minimumMeterChangeForUpdate = 10 //10 meters;
        long minimumMeterChangeForUpdate = 0; //0 meters reason it's a hack because LocationManager in androind is buggy and unreliable


        gpsPlugin.Init(updateInterval, minimumMeterChangeForUpdate);
        gpsPlugin.SetLocationChangeListener(
                        OnLocationChange
                        , OnEnableGPS
                        , OnGetLocationComplete
                        , OnGetLocationFail
                        , onLocationChangeInformation
                        , OnGetLocationCompleteInformation
            );

        L1.Latitude = gpsPlugin.GetLatitude();
        L1.Longitude = gpsPlugin.GetLongitude();
        // PositionInfo = new double[3];
        // PositionInfo = GetLocation();
        // L1.Latitude = PositionInfo[0];
        // L1.Longitude = PositionInfo[1];

    }
    void Update() {
        DistanceText.text = (int)DataCenterModel.instance.playerMng.CurrentDistance + "m";
        //DistanceRecordText.text = (int)DataCenterModel.instance.playerMng.DistanceRecord + "m";
        //DataCenterModel.instance.playerMng.CurrentDistance = (float)D;
    }
    public double[] GetLocation()
    {
        double[] positionInfo = new double[3];
        string locationSet = gpsPlugin.GetLocation();
        string[] locations = locationSet.Split(',');


        Debug.Log(locationSet);
        double latitude = System.Double.Parse(locations.GetValue(0).ToString());
        double longitude = System.Double.Parse(locations.GetValue(1).ToString());
        double speed = System.Double.Parse(locations.GetValue(2).ToString());


        Debug.Log("latitude: " + latitude);
        Debug.Log("longitude: " + longitude);
        Debug.Log("speed: " + speed);

        positionInfo[0] = latitude;
        positionInfo[1] = longitude;
        positionInfo[2] = speed;

        return positionInfo;

    }

    private void OnLocationChange(double latitude, double longitude)
    {
        
        Debug.Log("[GPSDemo] OnLocationChange latitude: " + latitude + " longitude: " + longitude);
        L2.Latitude = latitude;
        L2.Longitude = longitude;





        d = DistanceCalculate.GreatCircleDistance(L1, L2) * 1000;
        if (Speed > 20) {
            d = 0;
        }

        if (d >= 5)
        {
            if (d > 18) {
                d = d / 10;
            }
            DataCenterModel.instance.playerMng.CurrentDistance += (float)d;
            DataCenterModel.instance.playerMng.DistanceRecord += (float)d;
        }
        //PlayerPrefs.SetFloat(MePlayerPrefs.DISTANCE_TRAVELLED_KEY, (float)D);
        DistanceText.text = (int)DataCenterModel.instance.playerMng.CurrentDistance + "m"; //((int)PlayerPrefs.GetFloat(MePlayerPrefs.DISTANCE_TRAVELLED_KEY, 0)).ToString()+"m";
        L1.Latitude = L2.Latitude;
        L1.Longitude = L2.Longitude;
    }

    private void onLocationChangeInformation(string information)
    {
        Debug.Log("[GPSDemo] onLocationChangeInformation " + information);

        string[] informations = information.Split(',');

        //note: here's the orders of information
        //latidude,longitude,speed,altitude,bearing
        Speed = System.Double.Parse(informations.GetValue(2).ToString());
        speedText.text = Speed.ToString();
        if (Speed > 20)
        {
            Speed = 0.5;
        }
        DataCenterModel.instance.playerMng.Burned += CalorieBurnTracker.CalculateCalorie((float)Speed);
        DataCenterModel.instance.playerMng.BurnedRecord += CalorieBurnTracker.CalculateCalorie((float)Speed);
        ChallengeManager.IncreaseCurrentBurnedChallenge((int)CalorieBurnTracker.CalculateCalorie((float)Speed));
        foreach (string info in informations)
        {
            Debug.Log("info " + info);
        }
    }

    private void OnEnableGPS(string status)
    {
      
    }

    private void OnGetLocationComplete(double latitude, double longitude)
    {
        Debug.Log("[GPSDemo] OnGetLocationComplete latitude: " + latitude + " longitude: " + longitude);

    }

    private void OnGetLocationCompleteInformation(string information)
    {
        Debug.Log("[GPSDemo] OnGetLocationCompleteInformation " + information);
        string[] informations = information.Split(',');

        //note: here's the orders of information
        //latidude,longitude,speed,altitude,bearing

        foreach (string info in informations)
        {
            Debug.Log("info " + info);
        }
    }

    private void OnGetLocationFail()
    {
        Debug.Log("[GPSDemo] OnGetLocationFail");
    }
}
