﻿using UnityEngine;
using System.Collections;

public class Dart : MonoBehaviour {
	public Quaternion r;
	public Vector3 R;
	private Transform target;
	// Use this for initialization
	public float angle;
	public float x;
	public float y;

	Vector3 FirstPosition;
	void Start () {
		target = GameObject.Find ("Crosshair").transform;
		FirstPosition = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		r = this.transform.rotation;
		R = r.eulerAngles;


		x = Mathf.Abs(target.position.x - FirstPosition.x);
		y = target.position.y - FirstPosition.y;
		//float z = Mathf.Sqrt (Mathf.Pow (x, 2) + Mathf.Pow (y, 2));
		
		float z_angle = Mathf.Atan(y/x);
		angle = z_angle*57.29578f;

		if (target.position.x - FirstPosition.x < 0) {
			angle = 180-angle;

		}
		R.z = angle;
		r.eulerAngles = R;
		transform.rotation = r;
		this.transform.position = Vector3.MoveTowards (transform.position, target.position, 0.5f);
		if(Vector3.Distance(transform.position,target.position) <= 0.01f){
			DestroyObject(this.gameObject);
		}

	}
}
