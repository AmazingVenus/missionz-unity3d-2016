﻿using UnityEngine;
using System.Collections;

public class DartSpwnerController : MonoBehaviour {
	public GameObject dart;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ShootDart(GameObject target){
		if (!GameplayController.instance.isLoading) {
			Instantiate (dart, this.transform.position, Quaternion.identity);
		}
	}
}
