﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ApplicationStatus : MonoBehaviour {

    //public int timeInSecond;
    Activity activity;
    string facebookId;
    int minute;
    int distance1;
    int distance2;
    int weight;
    int burned1;
    int burned2;
    DateTime saveDate;
    public int savecount;
    // Use this for initialization

    float time;
    float delay;
	void Start () {
        distance1 = (int)DataCenterModel.instance.playerMng.DistanceRecord;
        burned1 = (int)DataCenterModel.instance.playerMng.BurnedRecord;
    }
	
	// Update is called once per frame
	void Update () {
        
        distance2 = (int)DataCenterModel.instance.playerMng.DistanceRecord;
        burned2 = (int)DataCenterModel.instance.playerMng.BurnedRecord;
        facebookId = PlayerPrefs.GetString(MePlayerPrefs.FACEBOOK_ID_KEY);
        


        //timeInSecond = Mathf.FloorToInt(Time.time);
        if (Input.GetKey(KeyCode.Escape)) {
            Application.Quit();
        }

        time += Time.deltaTime;
        if (time >= 300)
        {
            time = 0;
            minute = Mathf.CeilToInt(time / 60f);
            weight = (int)DataCenterModel.instance.playerMng.Weight;
            activity = new Activity(facebookId, minute, distance2 - distance1, weight, burned2 - burned1, DateTime.Now.ToLocalTime());
            ActivityManager.SaveActivity(activity);

            distance1 = (int)DataCenterModel.instance.playerMng.DistanceRecord;
            burned1 = (int)DataCenterModel.instance.playerMng.BurnedRecord;

            savecount++;
        }


    }

    void OnApplicationQuit() {
        //Debug.Log((distance2 - distance1) + " m");
        //Debug.Log("datetime now"+DateTime.Now);
        Debug.Log("Path : "+Application.persistentDataPath);
        minute = Mathf.CeilToInt(time / 60f);
        weight = (int)DataCenterModel.instance.playerMng.Weight;

        Debug.Log("minute "+ minute);
        Debug.Log("distance "+(distance2 - distance1));
        Debug.Log("weight "+weight);
        Debug.Log("burned "+(burned2 - burned1));
        Debug.Log(DateTime.Now.ToLocalTime());

        activity = new Activity(facebookId, minute, distance2-distance1, weight, burned2-burned1, DateTime.Now.ToLocalTime());
        ActivityManager.SaveActivityToJson(activity);

        DataCenterModel.instance.SaveAllData();
        Debug.Log("Quit");
    }
}
