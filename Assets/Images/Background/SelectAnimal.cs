﻿using UnityEngine;
using System.Collections;

public class SelectAnimal : MonoBehaviour {
    public GameObject SelectedAnimal;
    public Vector3 SpawnAnimalPoint;
    public GameObject[] AnimalPool;
    // Use this for initialization
    void Start () {

         SelectAnimals(GameObject.Find("StageController").GetComponent<StageController>().AnimalName);
         Instantiate(SelectedAnimal.gameObject, SpawnAnimalPoint, Quaternion.identity);
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SelectAnimals(string AnimalName)
    {
        if (AnimalName.Equals("Lion"))
        {
            SelectedAnimal = AnimalPool[0];

        }
        else if (AnimalName.Equals("Rabbit"))
        {
            SelectedAnimal = AnimalPool[1];

        }
        else if (AnimalName.Equals("Elephen"))
        {
            SelectedAnimal = AnimalPool[2];
        }

    }
}
