﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AlertDialog : MonoBehaviour {
    public string Title;
    public string message;

    public Text titleText;
    public Text messageText;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        titleText.text = Title;
        messageText.text = message;
	}

    public void Show() {
        this.GetComponent<TransitionEffect>().EndPosition = new Vector2(0, 0);
        this.GetComponent<TransitionEffect>().play();

    }

    public void hide() {
        this.GetComponent<TransitionEffect>().EndPosition = new Vector2(277, 0);
        this.GetComponent<TransitionEffect>().play();
    }
}
