﻿using UnityEngine;
using System.Collections;
using Parse;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine.UI;

public class rankController : MonoBehaviour {

    public GameObject rankPanel;
    string facebookId;
    public GameObject rankEntry;
    private GameObject newCloneRank;
    private List<Rank> rankList = new List<Rank>();
    public GameObject loadingText;
    
    public bool isFinish;
    public int progress;
    // Use this for initialization
    void Start() {
        //LoadRankFromParse();
    }

    // Update is called once per frame
    void Update() {
        if (isFinish) {
            showRank();
            isFinish = false;
            loadingText.SetActive(false);
        }

    }


    private void showRank()
    {
            
            for (int i = 0; i < rankList.Count; i++)
            {
                newCloneRank = Instantiate(rankEntry) as GameObject; //copy
                newCloneRank.transform.SetParent(rankPanel.transform); //move to panel
                newCloneRank.transform.localScale = new Vector3(1, 1, 1);
                newCloneRank.transform.Find("name").GetComponent<Text>().text = rankList[i].name;
                newCloneRank.transform.Find("score").GetComponent<Text>().text = "" + rankList[i].score;
                newCloneRank.transform.Find("Text").GetComponent<Text>().text = "" + (i + 1);
            }
    }

    public void LoadRankFromParse()
    {
        loadingText.SetActive(true);
        if (GameObject.FindGameObjectsWithTag("RankEntry") != null)
        {
            GameObject[] oldEntry = GameObject.FindGameObjectsWithTag("RankEntry");
            foreach (GameObject entry in oldEntry)
            {
                Destroy(entry);
            }
        }
        rankList.Clear();
        var query = ParseObject.GetQuery("Rank");

        query.CountAsync().ContinueWith(t =>
        {
            t.Wait();
            int count = t.Result;
            if (count > 0)
            {
                Debug.Log("rank data exist");

                var query2 = ParseObject.GetQuery("Rank").OrderByDescending("score");
                query2.FindAsync().ContinueWith(t2 =>
                {
                    t2.Wait();
                    IEnumerable<ParseObject> results = t2.Result;
                    foreach (ParseObject OKResults in results)
                    {
                        Rank a = new Rank(OKResults.Get<string>("facebookName"), OKResults.Get<int>("score"));
                        rankList.Add(a);
                        //progress = Mathf.FloorToInt(rankList.Count*100 / (float)count);
                    }

                    isFinish = true;


                    Debug.Log("Rank Loaded");
                });
            }
            else
            {
                Debug.Log("rank data doesn't exist");
                isFinish = true;
            }
        });
    }

}